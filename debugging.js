function installChromeExtensions() {
  const devtron = require('devtron');
  const log = require('electron-log');

  const { info } = log;

  const {
    default: installExtension,
    REACT_DEVELOPER_TOOLS,
    REDUX_DEVTOOLS,
  } = require('electron-devtools-installer');

  devtron.install();

  [REACT_DEVELOPER_TOOLS, REDUX_DEVTOOLS].forEach((ext) => {
    installExtension(ext)
      .then((name) => info(`Added Extension: ${name}`))
      .catch((err) => info('An error occurred: ', err));
  });
}

module.exports = {
  installChromeExtensions,
};
