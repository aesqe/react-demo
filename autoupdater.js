const { ipcMain } = require('electron');
const { autoUpdater } = require('electron-updater');
const shellEnv = require('shell-env');
const log = require('electron-log');
const Config = require('electron-store');
const fetch = require('node-fetch');
const packageJson = require('./package.json');

const userEnv = shellEnv.sync();
const config = new Config();

let mainWindow;
let githubTokenIsValid = false;

autoUpdater.autoDownload = false;

autoUpdater.logger = log;
autoUpdater.logger.transports.file.level = 'info';

function sendMessageToWindow(...args) {
  mainWindow.webContents.send('autoupdater', ...args);
}

function validateGithubToken(token) {
  if (token === 'current') {
    token = config.get('githubToken');
  }

  const { author, repoName } = packageJson;
  const repoApiUrl = `https://api.github.com/repos/${author}/${repoName}`;

  return fetch(repoApiUrl, {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Token ${token}`,
    },
  }).then((res) => {
    const status = res.headers.get('status');

    if (status !== '200 OK') {
      sendMessageToWindow('invalid-github-token');
      githubTokenIsValid = false;
      return false;
    }

    sendMessageToWindow('valid-github-token');
    githubTokenIsValid = true;
    return true;
  });
}

async function setupAutoUpdater(win) {
  mainWindow = win;

  const githubToken = config.get('githubToken');
  const { GH_TOKEN } = userEnv;
  const updateToken = githubToken || GH_TOKEN;

  if (!updateToken) {
    sendMessageToWindow('Either the GH_TOKEN env variable, or the githubToken config prop needs to be set for auto updates to work.');
    return autoUpdater;
  }

  await validateGithubToken(updateToken);

  if (!githubTokenIsValid) {
    return autoUpdater;
  }

  autoUpdater.setFeedURL({
    provider: 'github',
    owner: 'aesqe',
    repo: 'documentor',
    token: updateToken,
  });

  return autoUpdater;
}

autoUpdater.on('checking-for-update', () => {
  sendMessageToWindow('checking-for-update');
});

autoUpdater.on('update-available', (info) => {
  sendMessageToWindow('update-available', info);
});

autoUpdater.on('update-not-available', (info) => {
  sendMessageToWindow('update-not-available', info);
});

autoUpdater.on('error', (err) => {
  sendMessageToWindow('error', err);
});

autoUpdater.on('download-progress', (progressObj) => {
  sendMessageToWindow('download-progress', progressObj);
});

autoUpdater.on('update-downloaded', (info) => {
  sendMessageToWindow('update-downloaded', info);
});

module.exports = {
  sendMessageToWindow,
  setupAutoUpdater,
};

async function validateAndUpdateGithubToken(token) {
  await validateGithubToken(token);

  autoUpdater.setFeedURL({
    provider: 'github',
    owner: 'aesqe',
    repo: 'documentor',
    token,
  });
}

ipcMain.on('autoupdater', (event, message, ...args) => {
  log.info(message);

  switch (message) {
    case 'validate-github-token':
      validateGithubToken(args[0]);
      break;
    case 'update-github-token':
      validateAndUpdateGithubToken(args[0]);
      break;
    case 'check-for-updates':
      if (githubTokenIsValid) {
        autoUpdater.checkForUpdates();
      }
      break;
    case 'download-update':
      autoUpdater.downloadUpdate();
      break;
    case 'install-update':
      autoUpdater.quitAndInstall();
      break;
  }
});
