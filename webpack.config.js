const path = require('path');
const webpack = require('webpack');
const WebpackMd5Hash = require('webpack-md5-hash');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const { devServerPort } = require('./src/const.js');

const baseFolder = process.env.NODE_ENV === 'production' ? './app' : './src';

module.exports = {
  target: 'electron-renderer',
  devtool: 'eval-source-map',
  output: {
    publicPath: '/',
    path: path.resolve(__dirname, baseFolder),
    filename: 'bundle.js',
  },
  node: {
    __dirname: false,
    __filename: false,
  },
  devServer: {
    headers: { 'Access-Control-Allow-Origin': '*' },
    port: devServerPort,
    historyApiFallback: true,
    disableHostCheck: true,
    https: false,
    hot: true,
  },
  resolve: {
    extensions: ['.js', '.jsx', '.json'],
    alias: {
      root: __dirname,
      src: path.resolve(__dirname, 'src/'),
      data: path.resolve(__dirname, 'data/'),
      lib: path.resolve(__dirname, 'src/js/lib/'),
      modules: path.resolve(__dirname, 'src/js/modules/'),
      reducers: path.resolve(__dirname, 'src/js/reducers/'),
      components: path.resolve(__dirname, 'src/js/components/'),
    },
  },
  module: {
    rules: [
      {
        test: /\.html$/,
        use: [{ loader: 'html-loader' }],
      },
      {
        test: /\.js$/,
        exclude: [
          /node_modules/,
          path.resolve(__dirname, 'data/'),
          path.resolve(__dirname, 'dist/'),
        ],
        use: [
          'babel-loader',
          'react-hot-loader/webpack',
        ],
      },
      {
        test: /\.(css|scss)$/,
        use: [
          // MiniCssExtractPlugin.loader,
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
              modules: true,
              localIdentName: '[local]',
            },
          },
          'sass-loader',
        ],
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          'file-loader',
        ],
      },
    ],
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new CleanWebpackPlugin(['dist', 'app']),
    new CopyWebpackPlugin([{ from: 'src/assets', to: 'assets' }]),
    new HtmlWebPackPlugin({ template: './src/index.html', filename: 'index.html' }),
    new MiniCssExtractPlugin({ filename: 'style.css' }),
    new WebpackMd5Hash(),
  ],
};
