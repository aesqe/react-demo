
const { app, Tray, Menu } = require('electron');
const { productName } = require('./package.json');

function createTrayMenuForWindow(win, iconPath) {
  const appTray = new Tray(iconPath);
  const appTrayMenu = Menu.buildFromTemplate([{
    label: 'Show',
    click() {
      win.show();
    },
  }, {
    label: 'Quit',
    click() {
      win.close();
      win = null;
      app.isQuiting = true;
      app.quit();
      appTray.destroy();
    },
  }]);

  appTray.setContextMenu(appTrayMenu);
  appTray.setToolTip(productName);

  appTray.on('click', () => {
    if (win !== null) {
      win.show();
    }
  });

  /*
  win.on('close', (event) => {
    event.preventDefault();

    if (win !== null) {
      win.hide();
    }
  });
  */

  win.on('show', () => {
    appTray.setHighlightMode('always');
  });

  app.on('before-quit', () => {
    app.isQuiting = true;
  });

  return appTray;
}

module.exports = {
  createTrayMenuForWindow,
};
