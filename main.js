const { app, BrowserWindow, ipcMain  } = require('electron');
const windowStateKeeper = require('electron-window-state');
const contextMenu = require('electron-context-menu');
const electronIsDev = require('electron-is-dev');
const log = require('electron-log');
const path = require('path');

const { staticServer } = require('./src/js/modules/static-server.js');
const { serverPort, devServerPort, IS_OSX } = require('./src/const.js');
const { setDefaultApplicationMenu } = require('./menus.js');
const { setupAutoUpdater } = require('./autoupdater.js');

const { info: infoLog, error: errorLog } = log;

const iconExtension = IS_OSX ? 'png' : 'ico';
const appIcon = path.resolve(__dirname, `build/icon.${iconExtension}`);

const IS_DEVELOPMENT = electronIsDev && process.env.NODE_ENV !== 'production';

const localServerDir = IS_DEVELOPMENT ? 'src' : 'app';
const localServerPort = IS_DEVELOPMENT ? devServerPort : serverPort;
const localServerUrl = `http://localhost:${localServerPort}`;

const localServer = staticServer({
  default: path.join(__dirname, localServerDir),
}, serverPort, true);

let mainWindow = null;
let appTray = null;

process.env.ELECTRON_DISABLE_SECURITY_WARNINGS = true;

function closeEverything() {
  localServer.shutdown(() => {
    if (appTray) {
      appTray.destroy();
    }
    app.quit();
    mainWindow = null;
  });
}

function createLoadingWindow() {
  const loadingWindow = new BrowserWindow({
    width: 240,
    height: 240,
    show: true,
    frame: false,
    icon: appIcon,
    transparent: true,
    titleBarStyle: 'customButtonsOnHover', // OSX only
    backgroundColor: '#FFFFFF',
  });

  const loadingpage = path.resolve(__dirname, 'loading.html');

  loadingWindow.loadURL(`file:///${loadingpage}`);

  return loadingWindow;
}

function createMainWindow() {
  const loadingWindow = createLoadingWindow();

  const mainWindowState = windowStateKeeper({
    defaultWidth: 1024,
    defaultHeight: 768,
  });

  const win = new BrowserWindow({
    x: mainWindowState.x,
    y: mainWindowState.y,
    width: mainWindowState.width,
    height: mainWindowState.height,
    show: false,
    frame: IS_OSX,
    icon: appIcon,
    titleBarStyle: 'hiddenInset', // OSX only
    backgroundColor: '#FFFFFF',
    webPreferences: {
      nodeIntegration: true,
    },
  });

  // Let us register listeners on the window, so we can update the state
  // automatically (the listeners will be removed when the window is closed)
  // and restore the maximized or full screen state
  mainWindowState.manage(win);

  ipcMain.on('save-window-state', () => {
    mainWindowState.saveState();
  });

  win.once('ready-to-show', () => {
    if(loadingWindow) {
      loadingWindow.close();
    }

    win.show();

    if (IS_DEVELOPMENT) {
      win.openDevTools();
    }
  });

  win.on('closed', () => {
    closeEverything();
  });

  win.webContents.on('crashed', (err) => {
    errorLog('crashed', err);
    closeEverything();
  });

  win.on('unresponsive', (err) => {
    errorLog('unresponsive', err);
    closeEverything();
  });

  return win;
}

infoLog('App initializing...');

contextMenu();

app.once('ready', () => {
  infoLog('App ready');

  setDefaultApplicationMenu();

  mainWindow = createMainWindow();

  setupAutoUpdater(mainWindow);

  mainWindow.loadURL(localServerUrl);
});

app.on('window-all-closed', () => {
  if (process.platform != 'darwin') {
    closeEverything();
  }
});

process.on('uncaughtException', (err) => {
  errorLog('uncaughtException', err);
  closeEverything();
});
