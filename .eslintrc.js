module.exports = {
	"env": {
		"browser": true,
		"es6": true,
		"node": true
	},
	"extends": "eslint:recommended",
	"parser": "babel-eslint",
	"parserOptions": {
		"ecmaFeatures": {
			"jsx": true
		},
		"experimentalDecorators": true,
		"ecmaVersion": 2018,
		"sourceType": "module",
		"target": "es6"
	},
	"plugins": [
		"react",
		"import"
	],
	"settings": {
		"import/resolver": "webpack"
	},
	"rules": {
		"array-bracket-spacing": ["error", "never"],
		"brace-style": ["error", "1tbs", {
			"allowSingleLine": true
		}],
		"comma-dangle": ["error", {
			"arrays": "always-multiline",
			"objects": "always-multiline",
			"imports": "always-multiline",
			"exports": "always-multiline",
			"functions": "ignore"
		}],
		"comma-spacing": ["error", {
			"before": false,
			"after": true
		}],
		"consistent-return": ["error"],
		"eol-last": ["error", "always"],
		"import/named": 2,
		"import/no-unresolved": ["error"],
		"indent": ["error", 2, { "SwitchCase": 1 }],
		"jsx-quotes": ["error", "prefer-double"],
		"linebreak-style": ["warn", "unix"],
		"max-len": ["warn", {
			"code": 100,
			"ignoreStrings": true
		}],
		"no-console": ["warn"],
		"no-const-assign": ["error"],
		"no-dupe-args": ["error"],
		"no-dupe-keys": ["error"],
		"no-duplicate-case": ["error"],
		"no-duplicate-imports": ["error"],
		"no-shadow": ["error"],
		"no-trailing-spaces": ["error"],
		"no-undef": ["error"],
		"no-unused-vars": ["warn"],
		"no-use-before-define": ["error"],
		"no-var": ["error"],
		"object-shorthand": ["error"],
		"prefer-arrow-callback": ["error"],
		"prefer-const": ["error"],
		"prefer-destructuring": ["error"],
		"prefer-rest-params": ["error"],
		"prefer-spread": ["error"],
		"prefer-template": ["error"],
		"quotes": ["error", "single"],
		"react/jsx-uses-vars": ["warn"],
		"semi": ["error", "always"],
		"template-curly-spacing": ["error", "never"],
    "react/jsx-uses-react": ["warn"]
	}
};
