const serverPort = 59400;
const fileServerPort = 59401;
const devServerPort = 8080;

const platform = (() => {
  const { platform: p } = process;

  switch (p) {
    case 'darwin':
      return 'osx';
    case 'win32':
      return 'windows';
    default:
      return p;
  }
})();

const IS_OSX = process.platform === 'darwin';

module.exports = {
  platform,
  serverPort,
  fileServerPort,
  devServerPort,
  IS_OSX,
};
