import React from 'react';
import { Link } from 'react-router-dom';

export default function MainNavigation() {
  return (
    <ul className="navigation-main">
      <li className="nav-streetview"><Link to="/streetview">Map</Link></li>
      <li className="nav-heatmap"><Link to="/heatmap">HMAP</Link></li>
      <li className="nav-streetview-list"><Link to="/streetview-list">Img</Link></li>
      <li className="nav-terms"><Link to="/terms">Tag</Link></li>
      <li className="nav-collections"><Link to="/collections">Cols</Link></li>
      <li className="nav-output"><Link to="/output">Out</Link></li>
    </ul>
  );
}
