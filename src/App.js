import React, { PureComponent } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { hot } from 'react-hot-loader/root';
import SplitPane from 'react-split-pane';
import { connect } from 'react-redux';

import AppToolbar from 'components/app-toolbar';
import AppStatusbar from 'components/app-statusbar';

import Routes from './routes';
import MainNavigation from './main-navigation';
import ErrorBoundary from 'components/ErrorBoundary';

class App extends PureComponent {
  render() {
    const { appUpdate } = this.props;

    return (
      <ErrorBoundary>
        <BrowserRouter>
          <AppToolbar />
          <main className="app-main">
            <SplitPane
              className="splitpane-main-horizontal" split="horizontal" primary="second"
              minSize={20} defaultSize={20} maxSize={20}
            >
              <SplitPane
                className="splitpane-main-vertical" split="vertical"
                minSize={50} defaultSize={50} maxSize={100}
              >
                <MainNavigation />
                <Routes />
              </SplitPane>
              <AppStatusbar {...appUpdate} />
            </SplitPane>
          </main>
        </BrowserRouter>
      </ErrorBoundary>
    );
  }
}

const mapStateToProps = (state) => ({
  appUpdate: state.appUpdate,
});

export default hot(connect(mapStateToProps)(App));
