import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import {
  getTermGroupsForTerm,
  addTermToTermGroupThunk,
  addTermToNewTermGroupThunk,
  removeTermFromTermGroupThunk,
} from 'modules/taxonomies';
import Terms from 'components/terms';
import './style.scss';

class TermGroupsTagger extends PureComponent {
  constructor(props) {
    super(props);

    this.sortByIndex = this.sortByIndex.bind(this);
    this.termNotInTermGroup = this.termNotInTermGroup.bind(this);
    this.handleTermGroupAdd = this.handleTermGroupAdd.bind(this);
    this.handleTermGroupRemove = this.handleTermGroupRemove.bind(this);
    this.handleTermGroupSelect = this.handleTermGroupSelect.bind(this);
    this.handleTermGroupInput = this.handleTermGroupInput.bind(this);
    this.calculateMatches = this.calculateMatches.bind(this);

    this.state = {
      typedTextRx: false,
    };
  }

  termNotInTermGroup(termGroup) {
    const { term } = this.props;

    return term && !termGroup.termIds.includes(term.$loki);
  }

  sortByIndex(a, b) {
    return a[1] - b[1];
  }

  calculateMatches(termGroup) {
    const { typedTextRx } = this.state;
    let idx = 0;

    if (typedTextRx) {
      const matches = termGroup.name.match(typedTextRx);

      if (!matches) {
        return false;
      }

      idx = matches.index;
    }

    return [termGroup, idx];
  }

  getSuggestions() {
    const { termGroups } = this.props;
    const { typedTextRx } = this.state;

    if (!typedTextRx) {
      return termGroups
        .filter(this.termNotInTermGroup)
        .slice(0, 100);
    }

    return termGroups
      .filter(this.termNotInTermGroup)
      .map(this.calculateMatches)
      .filter(Boolean)
      .sort(this.sortByIndex)
      .map(a => a[0])
      .slice(0, 100);
  }

  handleTermGroupInput(typedText) {
    const trimmed = typedText.trim();
    const typedTextRx = trimmed ? new RegExp(trimmed, 'i') : false;

    this.setState({
      typedTextRx,
    });
  }

  handleTermGroupAdd(termGroupName) {
    const { term } = this.props;

    this.props.addTermToNewTermGroup(termGroupName, term);

    this.setState({
      typedTextRx: '',
    });
  }

  handleTermGroupSelect(termGroup) {
    const { term } = this.props;

    this.props.addTermToTermGroup(term.$loki, termGroup.$loki);
  }

  handleTermGroupRemove(termGroup) {
    const { term } = this.props;

    this.props.removeTermFromTermGroup(term.$loki, termGroup.$loki);
  }

  render() {
    const { term } = this.props;

    const termGroupsForTerm = getTermGroupsForTerm(term.$loki);
    const suggestions = this.getSuggestions();

    return (
      <Terms
        terms={termGroupsForTerm}
        suggestions={suggestions}
        onInput={this.handleTermGroupInput}
        onTermAdd={this.handleTermGroupAdd}
        onTermSelect={this.handleTermGroupSelect}
        onTermRemove={this.handleTermGroupRemove}
        onUndo={() => {}}
      />
    );
  }
}

TermGroupsTagger.propTypes = {
  term: PropTypes.object.isRequired,
  termGroups: PropTypes.array.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    addTermToTermGroup: (termId, termGroupId) => {
      dispatch(addTermToTermGroupThunk(termId, termGroupId));
    },
    addTermToNewTermGroup: (termGroupName, term) => {
      dispatch(addTermToNewTermGroupThunk(termGroupName, term));
    },
    removeTermFromTermGroup: (termId, termGroupId) => {
      dispatch(removeTermFromTermGroupThunk(termId, termGroupId));
    },
  };
}

export default connect(null, mapDispatchToProps)(TermGroupsTagger);
