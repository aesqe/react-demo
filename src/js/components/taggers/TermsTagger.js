import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import {
  addTermToItemThunk,
  addNewTermToItemThunk,
  removeTermFromItemThunk,
} from 'modules/taxonomies';
import Terms from 'components/terms';
import './style.scss';

class TermsTagger extends PureComponent {
  constructor(props) {
    super(props);

    this.handleTermInput = this.handleTermInput.bind(this);

    this.undo = this.undo.bind(this);
    this.sortByIndex = this.sortByIndex.bind(this);
    this.sortByNameProp = this.sortByNameProp.bind(this);
    this.termNotInItem = this.termNotInItem.bind(this);
    this.handleTermAdd = this.handleTermAdd.bind(this);
    this.handleTermRemove = this.handleTermRemove.bind(this);
    this.handleTermSelect = this.handleTermSelect.bind(this);
    this.calculateMatches = this.calculateMatches.bind(this);
    this.emitChange = this.emitChange.bind(this);

    this.state = {
      typedTextRx: false,
    };

    this.undoStack = [];
  }

  undo() {
    if (!this.undoStack.length) {
      return;
    }

    const deletedTerm = this.undoStack.pop();

    this.handleTermSelect(deletedTerm);
  }

  termNotInItem(term) {
    const { item: { termIds } } = this.props;

    return termIds && !termIds.includes(term.$loki);
  }

  sortByIndex(a, b) {
    return a[1] - b[1];
  }

  sortByNameProp(a, b) {
    const an = a.name;
    const bn = b.name;

    if (an > bn) {
      return 1;
    }

    if (an < bn) {
      return -1;
    }

    return 0;
  }

  calculateMatches(term) {
    const { typedTextRx } = this.state;
    let idx = 0;

    if (typedTextRx) {
      const matches = term.name.match(typedTextRx);

      if (!matches) {
        return false;
      }

      idx = matches.index;
    }

    return [term, idx];
  }

  getSuggestions() {
    const { terms } = this.props;
    const { typedTextRx } = this.state;

    let res = [];

    if (!typedTextRx) {
      res = terms
        .slice()
        .filter(this.termNotInItem)
        .sort(this.sortByNameProp)
        .slice(0, 100);
    } else {
      res = terms
        .slice()
        .filter(this.termNotInItem)
        .map(this.calculateMatches)
        .filter(Boolean)
        .sort(this.sortByIndex)
        .map(a => a[0])
        .slice(0, 100);
    }

    return res;
  }

  handleTermInput(typedText) {
    const trimmed = typedText.trim();
    const typedTextRx = trimmed ? new RegExp(trimmed, 'i') : false;

    this.setState({
      typedTextRx,
    });
  }

  handleTermAdd(termName) {
    const { taxonomyId, item } = this.props;

    this.props.addNewTermToItem(termName, taxonomyId, item)
      // eslint-disable-next-line no-unused-vars
      .then(([updatedItem, term]) => {
        this.emitChange({
          action: 'termAdd',
          term,
        });

        this.setState({
          typedTextRx: false,
        });
      });
  }

  handleTermSelect(term) {
    const { item } = this.props;

    this.props.addTermToItem(term, item)
      .then(() => this.emitChange({
        action: 'termAdd',
        term,
      }));
  }

  handleTermRemove(term) {
    const { item } = this.props;

    this.undoStack.push(term);
    this.props.removeTermFromItem(term.$loki, item)
      .then(() => this.emitChange({
        action: 'termRemove',
        term,
      }));
  }

  emitChange(data) {
    const { onChange} = this.props;

    if(typeof onChange === 'function') {
      onChange(data);
    }
  }

  render() {
    const { terms, item: { termIds } } = this.props;

    const itemTerms = terms.filter((term) => termIds.includes(term.$loki));
    const suggestions = this.getSuggestions();

    return (
      <Terms
        terms={itemTerms}
        suggestions={suggestions}
        onInput={this.handleTermInput}
        onTermAdd={this.handleTermAdd}
        onTermSelect={this.handleTermSelect}
        onTermRemove={this.handleTermRemove}
        onUndo={this.undo}
      />
    );
  }
}

TermsTagger.propTypes = {
  terms: PropTypes.array.isRequired,
  item: PropTypes.object.isRequired,
  taxonomyId: PropTypes.number.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    addTermToItem: (term, item) => dispatch(
      addTermToItemThunk(term, item)
    ),
    addNewTermToItem: (termName, taxonomyId, item) => dispatch(
      addNewTermToItemThunk(termName, taxonomyId, item)
    ),
    removeTermFromItem: (termId, item) => dispatch(
      removeTermFromItemThunk(termId, item)
    ),
  };
}

export default connect(null, mapDispatchToProps)(TermsTagger);
