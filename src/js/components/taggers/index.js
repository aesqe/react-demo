import TermsTagger from './TermsTagger';
import TermGroupsTagger from './TermGroupsTagger';

export {
  TermsTagger,
  TermGroupsTagger,
};

