// https://github.com/igorprado/react-notification-system/
// issues/72#issuecomment-305491648
import React, { PureComponent } from 'react';
import { PropTypes } from 'prop-types';

import NotificationSystem from 'react-notification-system';

class NotificationProvider extends PureComponent {
  getChildContext() {
    return {
      getNotificationSystem: this.getNotificationSystem.bind(this),
    };
  }

  getNotificationSystem() {
    return this.notification;
  }

  render() {
    return (
      <React.Fragment>
        {this.props.children}
        <NotificationSystem ref={(ref) => { this.notification = ref; }} />
      </React.Fragment>
    );
  }
}

NotificationProvider.propTypes = {
  children: PropTypes.node,
};

NotificationProvider.defaultProps = {
  children: '',
};

NotificationProvider.childContextTypes = {
  getNotificationSystem: PropTypes.func,
};

export default NotificationProvider;
