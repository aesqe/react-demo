import React, { PureComponent } from 'react';
import { MdSettings } from 'react-icons/md';
import { Link } from 'react-router-dom';

import './style.scss';

export default class ProjectSettingsMenu extends PureComponent {
  render() {
    return (
      <span className="project-settings-menu">
        <Link to="/">
          <MdSettings />
        </Link>
      </span>
    );
  }
}
