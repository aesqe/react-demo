import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import classnames from 'classnames';
import _ from 'lodash';

import { reloadCollectionAction, updateCollectionReducers } from 'reducers';
import { db, defaultCollectionNames, getCollection } from 'lib/database';
import { addClassToAppMain, removeClassFromAppMain } from 'lib/util';
import { getTermsByIds, getTaxonomyById } from 'modules/taxonomies';

import './style.scss';

export class CollectionList extends PureComponent {
  constructor(props) {
    super(props);

    this.selectCollection = this.selectCollection.bind(this);
    this.deleteCollection = this.deleteCollection.bind(this);
    this.emptyCollection = this.emptyCollection.bind(this);
    this.renderCollectionItem = this.renderCollectionItem.bind(this);
    this.renderDataTableRow = this.renderDataTableRow.bind(this);
    this.renderTerm = this.renderTerm.bind(this);

    this.state = {
      selectedCollectionName: '',
      selectedCollectionIsDefault: false,
    };
  }

  componentDidMount() {
    addClassToAppMain('collection-list');
  }

  componentWillUnmount() {
    removeClassFromAppMain('collection-list');
  }

  selectCollection(event) {
    const selectedCollectionName = event.target.id;
    const isDefault = defaultCollectionNames.includes(selectedCollectionName);

    this.setState({
      selectedCollectionName,
      selectedCollectionIsDefault: isDefault,
    });
  }

  deleteCollection() {
    const { selectedCollectionName } = this.state;
    const collection = getCollection(selectedCollectionName);
    const warning = 'This will delete all the data in this collection and remove it from the database - are you sure?';

    if (confirm(warning)) {
      collection.clear();

      db.removeCollection(selectedCollectionName);

      this.props.reloadCollection(selectedCollectionName);

      updateCollectionReducers();

      this.setState({
        selectedCollectionName: '',
        selectedCollectionIsDefault: false,
      });
    }
  }

  emptyCollection() {
    const { selectedCollectionName } = this.state;
    const collection = getCollection(selectedCollectionName);

    if (confirm('This will delete all the data in this collection - are you sure?')) {
      collection.clear();
      this.props.reloadCollection(selectedCollectionName);
    }
  }

  renderCollectionItem(collectionName) {
    const { selectedCollectionName } = this.state;

    const className = classnames('nolink', {
      active: selectedCollectionName === collectionName,
    });

    return (
      <li
        className={className}
        key={collectionName}
        id={collectionName}
        onClick={this.selectCollection}
      >
        {collectionName}
      </li>
    );
  }

  renderCollectionList() {
    const { collectionNames } = this.props;

    return (
      <ul className="list collection-list">
        {collectionNames.map(this.renderCollectionItem)}
      </ul>
    );
  }

  renderTerms(termIds) {
    const terms = getTermsByIds(termIds);

    return terms.map(this.renderTerm).join('\n');
  }

  getColumnNames(row) {
    return Object.keys(_.omit(row, ['meta', 'metadata', '$loki']));
  }

  renderTerm(term) {
    const { name, taxonomyId } = term;

    const { name: taxonomyName } = getTaxonomyById(taxonomyId);

    return `${taxonomyName}: ${name}`;
  }

  renderDataTableRow(row, rowIndex) {
    const columnNames = this.getColumnNames(row);

    return (
      <tr key={`tr-${rowIndex}`}>
        {columnNames.map((colName) => {
          let val = row[colName];

          if (colName === 'termIds') {
            val = this.renderTerms(val);
          } else if (typeof val !== 'string') {
            val = JSON.stringify(val);
          }

          return (
            <td key={`td-${rowIndex}-${colName}`}>
              {val}
            </td>
          );
        })}
      </tr>
    );
  }

  renderDataTable(rows) {
    const { selectedCollectionIsDefault } = this.state;
    const columnNames = this.getColumnNames(rows[0]);

    return (
      <div className="collection-data">
        <div className="collection-options">
          <p>
            <strong>
              There is no undo for operations presented here - please be careful.
            </strong>
          </p>
          <div className="buttons">
            {!selectedCollectionIsDefault && (
              <button className="delete-collection" onClick={this.deleteCollection}>
                Delete this collection
              </button>
            )}
            <button className="empty-collection" onClick={this.emptyCollection}>
              Empty this collection
            </button>
          </div>
        </div>
        <table>
          <thead>
            <tr>
              {columnNames.map((colName) => (
                <th key={`th-${colName}`}>
                  {colName}
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {rows.map(this.renderDataTableRow)}
          </tbody>
        </table>
      </div>
    );
  }

  renderCollectionContents() {
    const { collections } = this.props;
    const { selectedCollectionName } = this.state;

    if (!selectedCollectionName) {
      return null;
    }

    const data = collections[selectedCollectionName] || [];
    const rows = data.slice(0, 100);

    return this.renderDataTable(rows);
  }

  render() {
    const { database, projectReady } = this.props;

    if (!projectReady || !database.loaded) {
      return (
        <Redirect to="/" />
      );
    }

    return (
      <section id="view-collections">
        {this.renderCollectionList()}
        {this.renderCollectionContents()}
      </section>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    database: state.database,
    collections: state.collections,
    collectionNames: Object.keys(state.collections || {}),
    projectReady: !!state.project.databaseFilePath,
  };
};

const mapDispatchToProps = (dispatch) => ({
  reloadCollection: (collectionName) => dispatch(reloadCollectionAction(collectionName)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CollectionList);
