import React, { PureComponent } from 'react';

import ProjectOptions from 'components/project-options';

import './style.scss';

export default class Home extends PureComponent {
  render() {
    return (
      <section id="home">
        <ProjectOptions />
      </section>
    );
  }
}
