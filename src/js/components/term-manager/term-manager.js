import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import SplitPane from 'react-split-pane';
import { createSelector } from 'reselect';
import { Redirect } from 'react-router-dom';
import classNames from 'classnames';
import slugg from 'slugg';
import _ from 'lodash';

import {
  deleteTerms,
  addNewTermThunk,
  addNewTermWithTermGroupsThunk,
  mergeTermsThunk,
} from 'modules/taxonomies';
import { TAGGABLE_COLLECTION_NAMES } from 'modules/collections';
import { db } from 'lib/database';
import { Confirm } from 'components/dialog';

import TermOptions from './term-options';
import TermManagerTerms from './term-manager-terms';
import { TermGroupsTagger } from 'components/taggers';
import { sorters, calculateTermsUsage } from './services';
import {
  termsSelector,
  termGroupsSelector,
  projectSelector,
  taxonomyFromNameSelector,
} from './selectors';
import './style.scss';

export class TermManager extends PureComponent {
  constructor(props) {
    super(props);

    this.handleTermClick = this.handleTermClick.bind(this);
    this.handleAddNewTerm = this.handleAddNewTerm.bind(this);
    this.handleTermUpdate = this.handleTermUpdate.bind(this);
    this.handleTermDelete = this.handleTermDelete.bind(this);
    this.handleAddToSelectedGroupsDeny = this.handleAddToSelectedGroupsDeny.bind(this);
    this.handleAddToSelectedGroupsConfirm = this.handleAddToSelectedGroupsConfirm.bind(this);

    this.addNewTerm = this.addNewTerm.bind(this);
    this.setBusy = this.setBusy.bind(this);
    this.mergeSelected = this.mergeSelected.bind(this);
    this.deselectTerms = this.deselectTerms.bind(this);
    this.selectAllDisplayed = this.selectAllDisplayed.bind(this);

    this.sortDirAsc = this.sortDirAsc.bind(this);
    this.sortDirDesc = this.sortDirDesc.bind(this);
    this.sortByAlpha = this.sortByAlpha.bind(this);
    this.sortByGroup = this.sortByGroup.bind(this);
    this.sortByUsage = this.sortByUsage.bind(this);

    this.searchTerms = this.searchTerms.bind(this);
    this.handleInputSearchText = this.handleInputSearchText.bind(this);
    this.searchTermsDebounced = _.debounce(this.searchTerms.bind(this), 150);

    this.flashingTimer = -1;
    this.addNewTermInput = React.createRef();
    this.mergeTermsInput = React.createRef();

    this.state = {
      selectedTerms: [],
      selectedTermGroups: [],
      termGroupsToAddTo: [],
      singleSelectedTerm: null,
      showAddToSelectedGroupsConfirm: false,
      showOptions: false,
      busy: false,
      sortBy: 'alpha',
      sortDir: 'Asc',
      flashing: -1,
      filterVal: '',
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { terms } = nextProps;
    const { selectedTerms } = prevState;

    if (!terms.length) {
      return {
        selectedTerms: [],
        singleSelectedTerm: null,
      };
    }

    const newSelectedTerms = terms.filter((term) => {
      const finder = (t) => (t.$loki === term.$loki && t.slug === term.slug);
      return !!selectedTerms.find(finder);
    });
    const singleSelectedTerm = newSelectedTerms.length === 1 ? newSelectedTerms[0] : null;

    return {
      selectedTerms: newSelectedTerms,
      singleSelectedTerm,
    };
  }

  componentWillUnmount() {
    clearTimeout(this.flashingTimer);
  }

  flashTerm(termId) {
    this.setState({
      flashing: termId,
    });

    this.flashingTimer = setTimeout(() => {
      this.setState({
        flashing: -1,
      });
    }, 1500);
  }

  getFilteredTerms(){
    const { terms } = this.props;

    if (terms.length === 0) {
      return terms;
    }

    const { filterVal, sortBy, sortDir } = this.state;

    const sortFuncName = `${sortBy}${sortDir}`;
    const sort = sorters[sortFuncName];

    if (!sort) {
      return terms;
    }

    let filterFunc;

    if (filterVal === '') {
      filterFunc = Boolean;
    } else {
      const rxName = new RegExp(filterVal, 'i');
      const rxSlug = new RegExp(slugg(filterVal), 'i');

      filterFunc = (term) => {
        const { name, slug } = term;
        return (name.match(rxName) || slug.match(rxSlug));
      };
    }

    return terms
      .filter(filterFunc)
      .sort(sort);
  }

  sortBy(propName) {
    const { sortBy } = this.state;

    if (sortBy === propName) {
      return;
    }

    this.deselectTerms();
    this.setState({
      sortBy: propName,
    });
  }

  sortDir(dir) {
    const { sortDir } = this.state;

    if (sortDir === dir) {
      return;
    }

    this.deselectTerms();
    this.setState({
      sortDir: dir,
    });
  }

  sortByAlpha() {
    this.sortBy('alpha');
  }

  sortByGroup() {
    this.sortBy('alphaGroup');
  }

  sortByUsage() {
    this.sortBy('usage');
  }

  sortDirAsc() {
    this.sortDir('Asc');
  }

  sortDirDesc() {
    this.sortDir('Desc');
  }

  searchTerms(event) {
    const filterVal = event.target.value;

    this.setState({
      filterVal,
    });
  }

  handleInputSearchText(event) {
    event.persist();

    return this.searchTermsDebounced(event);
  }

  deselectTerms() {
    this.setState({
      selectedTerms: [],
      singleSelectedTerm: null,
    });
  }

  handleSelectEmptyTerm() {
    this.deselectTerms();
  }

  getSelectedTermsIndex(termId) {
    const { selectedTerms } = this.state;
    return selectedTerms.findIndex(term => (term.$loki === termId));
  }

  handleTermClick( event, term ) {
    this.props.onTermClick(term);

    if (event === 'empty') {
      this.handleSelectEmptyTerm();
      return;
    }

    const { selectedTerms } = this.state;
    const { metaKey, ctrlKey } = event;

    const termId = term.$loki;
    const index = this.getSelectedTermsIndex(termId);
    const isSelected = index > -1;

    let newSelectedTerms;

    if (isSelected) {
      if (metaKey || ctrlKey) {
        newSelectedTerms = [
          ...selectedTerms.slice(0, index),
          ...selectedTerms.slice(index+1),
        ];
      } else {
        newSelectedTerms = [term];
      }
    } else if (metaKey || ctrlKey) {
      newSelectedTerms = [
        ...selectedTerms,
        term,
      ];
    } else {
      newSelectedTerms = [term];
    }

    const singleSelectedTerm = (newSelectedTerms.length === 1) ? newSelectedTerms[0] : null;
    const selectedTermsUsage = calculateTermsUsage(newSelectedTerms);

    this.setState({
      selectedTerms: newSelectedTerms,
      singleSelectedTerm,
      selectedTermsUsage,
    });
  }

  handleTermDelete() {
    this.deselectTerms();
    this.setBusy(false);
  }

  handleTermUpdate(termId) {
    const { terms } = this.props;

    const term = terms.find(t => t.$loki === termId);

    this.setBusy(false);

    this.setState({
      selectedTerms: [term],
      singleSelectedTerm: term,
    });
  }

  mergeSelected(event) {
    if (event.keyCode !== 13) {
      return;
    }

    const { selectedTerms } = this.state;

    const mergeNameEl = this.mergeTermsInput.current;
    const { value: mergedName } = mergeNameEl;

    const termIds = selectedTerms.map(term => term.$loki);

    this.props.mergeTerms(mergedName, termIds)
      .then((mergedTerm) => {
        mergeNameEl.value = '';

        this.setState({
          selectedTerms: [mergedTerm],
          singleSelectedTerm: mergedTerm,
          scrollTo: mergedTerm.$loki,
          flashing: mergedTerm.$loki,
        });
      });
  }

  handleAddToSelectedGroupsDeny() {
    this.setState({
      termGroupsToAddTo: [],
      showAddToSelectedGroupsConfirm: false,
    }, this.addNewTerm);
  }

  handleAddToSelectedGroupsConfirm() {
    const { selectedTermGroups } = this.state;
    const termGroupsToAddTo = selectedTermGroups.slice();

    this.setState({
      termGroupsToAddTo,
      showAddToSelectedGroupsConfirm: false,
    }, this.addNewTerm);
  }

  handleAddNewTerm(event) {
    if (event.keyCode !== 13) {
      return;
    }

    const { selectedTermGroups } = this.state;

    if (selectedTermGroups.length) {
      this.setState({
        showAddToSelectedGroupsConfirm: true,
      });
    } else {
      this.setState({
        showAddToSelectedGroupsConfirm: false,
      });

      this.addNewTerm();
    }
  }

  addNewTerm() {
    const { taxonomy: { $loki: taxonomyId } } = this.props;
    const { termGroupsToAddTo } = this.state;

    const inputEl = this.addNewTermInput.current;
    const { value: termName } = inputEl;

    this.props.addNewTerm(termName, taxonomyId, termGroupsToAddTo)
      .then((newTerm) => {
        inputEl.value = '';

        this.setState({
          selectedTerms: [newTerm],
          singleSelectedTerm: newTerm,
          scrollTo: newTerm.$loki,
          flashing: newTerm.$loki,
          showAddToSelectedGroupsConfirm: false,
        });
      });
  }

  deleteSelectedTerms() {
    if (confirm('Delete ALL selected terms: are you sure?')) {
      return;
    }

    const { selectedTerms } = this.state;

    deleteTerms(selectedTerms, TAGGABLE_COLLECTION_NAMES);

    this.setState({
      selectedTerms: [],
      singleSelectedTerm: null,
      flashing: -1,
    });
  }

  setBusy(val) {
    this.setState({
      busy: val,
    });
  }

  renderMultiTermsOptions() {
    const { merging, selectedTerms, selectedTermsUsage } = this.state;

    const selectedTermsNames = selectedTerms.map(term => term.name).join(', ');

    return (
      <div className="multi-term-options">
        <div className="merge-terms options-block">
          <label htmlFor="merge-terms">Merge as</label>
          <input
            name="merge-terms"
            type="text"
            onKeyUp={this.mergeSelected}
            ref={this.mergeTermsInput}
          />
        </div>

        <div className="delete-terms options-block fullwidth">
          <button onClick={this.deleteSelectedTerms}>Delete Selected Terms</button>
        </div>

        <div className="deselect-terms options-block fullwidth">
          <button onClick={this.deselectTerms}>Deselect all Terms</button>
        </div>

        <div className="stats">
          {merging && ( <p>Merging <strong>{selectedTermsNames}</strong></p> )}
          {!merging && ( <p>Terms <strong>{selectedTermsNames}</strong> can be merged</p> )}
          <p>
            {selectedTermsUsage.items} items containing the term,
            found in {selectedTermsUsage.groups} term groups, combined.
          </p>
        </div>
      </div>
    );
  }

  renderTermsSidebar() {
    const { termGroups, sidebarRenderer } = this.props;

    if (typeof sidebarRenderer === 'function') {
      return sidebarRenderer();
    }

    const {
      busy,
      sortBy,
      sortDir,
      selectedTerms,
      singleSelectedTerm,
      showAddToSelectedGroupsConfirm,
    } = this.state;

    const sortClasses = {
      sortDirAsc: sortDir === 'Asc' ? 'active' : '',
      sortDirDesc: sortDir === 'Desc' ? 'active' : '',
      sortByAlpha: sortBy === 'alpha' ? 'active' : '',
      sortByUsage: sortBy === 'usage' ? 'active' : '',
      sortByGroup: sortBy === 'alphaGroup' ? 'active' : '',
    };

    return (
      <div className="options options-terms">
        <p>
          <button onClick={this.selectAllDisplayed}>
            Select all
          </button>
        </p>
        <div className="sorting options-section">
          <h3 className="item-label">View / sort</h3>

          <div className="search-terms options-block">
            <label htmlFor="search-terms">Search</label>
            <input
              type="text"
              id="search-terms"
              onKeyUp={this.handleInputSearchText}
            />
          </div>

          <div className="sort-terms options-block">
            <button onClick={this.sortDirAsc} className={sortClasses.sortDirAsc}>ASC</button>
            <button onClick={this.sortDirDesc} className={sortClasses.sortDirDesc}>DESC</button>
            <button onClick={this.sortByAlpha} className={sortClasses.sortByAlpha}>ABC</button>
            <button onClick={this.sortByUsage} className={sortClasses.sortByUsage}>123</button>
          </div>
        </div>

        <div className="actions options-section">
          <h3 className="item-label">Action</h3>

          <div className="new-term options-block">
            <label htmlFor="new-term">New term</label>
            <input
              id="new-term"
              type="text"
              onKeyUp={this.handleAddNewTerm}
              ref={this.addNewTermInput}
            />
          </div>

          {showAddToSelectedGroupsConfirm && (
            <Confirm
              description="Add the new term to all selected term groups?"
              confirmLabel="Yes"
              denyLabel="No"
              onConfirm={this.handleAddToSelectedGroupsConfirm}
              onDeny={this.handleAddToSelectedGroupsDeny}
            />
          )}
          {!!singleSelectedTerm && (
            <React.Fragment>
              <TermOptions
                term={singleSelectedTerm}
                busy={busy}
                busySetter={this.setBusy}
                onTermDelete={this.handleTermDelete}
                onTermUpdate={this.handleTermUpdate}
              />
              <div className="term-groups-tagger options-block">
                <h3>Term groups</h3>
                <TermGroupsTagger
                  term={singleSelectedTerm}
                  termGroups={termGroups}
                />
              </div>
            </React.Fragment>
          )}

          {selectedTerms.length > 1 && this.renderMultiTermsOptions()}
        </div>

      </div>
    );
  }

  selectAllDisplayed() {
    const selectedTerms = this.getFilteredTerms();
    const singleSelectedTerm = (selectedTerms.length === 1) ? selectedTerms[0] : null;
    const selectedTermsUsage = calculateTermsUsage(selectedTerms);

    this.setState({
      selectedTerms,
      singleSelectedTerm,
      selectedTermsUsage,
    });
  }

  render() {
    const { projectReady } = this.props;
    const { busy, flashing, selectedTerms } = this.state;

    if (!projectReady || !db.loaded) {
      return (
        <Redirect to="/" />
      );
    }

    const filteredTerms = this.getFilteredTerms();
    const managerClasses = classNames('term-manager', {
      busy,
    });

    return(
      <div className={managerClasses}>
        <SplitPane split="vertical" minSize={300} defaultSize={300} maxSize={400} primary="second">
          <TermManagerTerms
            terms={filteredTerms}
            selectedTerms={selectedTerms}
            flashing={flashing}
            onTermClick={this.handleTermClick}
          />
          {this.renderTermsSidebar()}
        </SplitPane>
      </div>
    );
  }
}

const stateToPropsSelector = (project, allTerms, allTermGroups, taxonomy) => {
  const projectReady = !!project.databaseFilePath;

  if (!taxonomy) {
    return {
      terms: [],
      projectReady,
    };
  }

  const taxonomyId = taxonomy.$loki;
  const termsInTaxonomy = allTerms.filter(t => t.taxonomyId === taxonomyId);
  const termGroups = allTermGroups.filter(t => t.taxonomyId === taxonomyId);

  return {
    termsInTaxonomy,
    termGroups,
    taxonomy,
    projectReady,
  };
};

const getPropsFromState = createSelector(
  projectSelector,
  termsSelector,
  termGroupsSelector,
  taxonomyFromNameSelector,
  stateToPropsSelector
);

const mapStateToProps = (state, ownProps) => {
  return getPropsFromState(state, ownProps);
};

const mapDispatchToProps = (dispatch) => ({
  addNewTerm: (termName, taxonomyId, termGroupIds) => {
    const action = (termGroupIds.length) ?
      addNewTermWithTermGroupsThunk(termName, taxonomyId, termGroupIds) :
      addNewTermThunk(termName, taxonomyId);

    return dispatch(action);
  },
  mergeTerms: (termIds, mergedName) =>
    dispatch(mergeTermsThunk(termIds, mergedName, ['streetviewimages', 'items', 'images'])),
});

TermManager.defaultProps = {
  terms: [],
  taxonomyName: 'tags',
};

export default connect(mapStateToProps, mapDispatchToProps)(TermManager);
