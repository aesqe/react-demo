import React, { PureComponent } from 'react';
import classNames from 'classnames';

import TermManagerTermGroup from './term-manager-term-group';

/**
 * Displays a list of termGroups in TermManager
 */
export default class TermManagerTermGroups extends PureComponent {
  constructor(props) {
    super(props);

    this.renderTermGroup = this.renderTermGroup.bind(this);
    this.handleSelectEmptyTermGroup = this.handleSelectEmptyTermGroup.bind(this);
  }

  handleSelectEmptyTermGroup() {
    this.props.onTermGroupClick('empty');
  }

  isTermGroupSelected(termGroupId) {
    const { selectedTermGroups } = this.props;

    const index = selectedTermGroups.findIndex(t => (t.$loki === termGroupId));

    return index > -1;
  }

  renderTermGroup(termGroup) {
    const { onTermGroupClick, flashing } = this.props;
    const { $loki: termGroupId } = termGroup;

    const key = `term-manager-term-group-${termGroupId}`;

    const isSelected = this.isTermGroupSelected(termGroupId);
    const isFlashing = flashing === termGroupId;

    return (
      <TermManagerTermGroup
        key={key}
        termGroup={termGroup}
        flashing={isFlashing}
        selected={isSelected}
        onClick={onTermGroupClick}
      />
    );
  }

  render() {
    const { termGroups, selectedTermGroups } = this.props;

    const classes = classNames('term-group', {
      selected: (selectedTermGroups.length === 0),
    });

    return(
      <section className="term-manager-term-groups">
        <ul className="term-groups">
          <li id="term-group-empty" className={classes} onClick={this.handleSelectEmptyTermGroup}>
            <span className="term-group-slug">
              -- no term group --
            </span>
          </li>
          {termGroups.map(this.renderTermGroup)}
        </ul>
      </section>
    );
  }
}
