import React, { PureComponent} from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';

/**
 * Displays a single termGroup in TermManager
 */
export default class TermManagerTermGroup extends PureComponent {
  constructor(props) {
    super(props);

    this.termGroupNodeRef = React.createRef();
  }

  componentDidMount() {
    this.scrollToDomRef();
  }

  componentDidUpdate() {
    this.scrollToDomRef();
  }

  scrollToDomRef() {
    const { scrollTo } = this.props;

    if (scrollTo) {
      const node = ReactDOM.findDOMNode(this.termGroupNodeRef.current);
      node.scrollIntoView({
        behavior: 'smooth',
      });
    }
  }

  render() {
    const { termGroup, flashing, selected, onClick } = this.props;
    const { $loki: termGroupId, slug, name, visible, color } = termGroup;

    const id = `termGroup-${termGroupId}`;
    const visibility = visible ? '-' : '+';

    const classes = classNames('term-group', {
      flashing: flashing === termGroupId,
      selected,
    });

    function handleClick(event) {
      onClick(event, termGroup);
    }

    return (
      <li id={id} className={classes} onClick={handleClick} ref={this.termGroupNodeRef}>
        <span className="term-group-slug" title={slug} data-termgroupid={termGroupId}>
          {name}
        </span>
        <span className="term-group-visibility">
          {visibility}
        </span>
        <span className="term-group-color" style={{backgroundColor: color}}></span>
      </li>
    );
  }
}
