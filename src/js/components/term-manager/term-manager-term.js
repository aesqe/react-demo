import React, { PureComponent} from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';
import { boundMethod } from 'autobind-decorator';

import {
  isTermUsed,
  getTermGroupsForTerm,
} from 'modules/taxonomies';

/**
 * Displays a single term in TermManager
 */
export default class TermManagerTerm extends PureComponent {
  constructor(props) {
    super(props);

    this.termNodeRef = React.createRef();
  }

  componentDidMount() {
    this.scrollToDomRef();
  }

  componentDidUpdate() {
    this.scrollToDomRef();
  }

  scrollToDomRef() {
    const { scrollTo } = this.props;

    if (scrollTo) {
      const node = ReactDOM.findDOMNode(this.termNodeRef.current);
      node.scrollIntoView({
        behavior: 'smooth',
      });
    }
  }

  @boundMethod
  handleTermClick(event) {
    const { term, onClick } = this.props;

    onClick(event, term);
  }

  render() {
    const { term, flashing, selected } = this.props;
    const {
      $loki: termId,
      slug,
      name,
      visible,
      color,
      streetviewimageCount,
      termGroupsCount = 0,
    } = term;

    const id = `term-${termId}`;
    const isUsed = isTermUsed(termId, 'items');
    const visibility = isUsed ? (visible ? '+' : '-') : '';

    const termGroups = getTermGroupsForTerm(termId);
    const termGroupsElements = termGroups.map((termGroup) => (
      <span className="term-termgroup" key={`t-tg-${termGroup.$loki}`}>
        {termGroup.name}
      </span>
    ));

    const termGroupsNames = (termGroupsElements.length && termGroupsElements) || '-';

    const classes = classNames('term', {
      flashing: flashing === termId,
      selected,
    });

    return (
      <li id={id} className={classes} onClick={this.handleTermClick} ref={this.termNodeRef}>
        <span className="term-color" style={{backgroundColor: color}}></span>
        <span className="term-visibility">
          {visibility}
        </span>
        <span className="term-streetviewimage-count" title="streetviewimageCount">
          {streetviewimageCount}
        </span>
        <span className="term-termgroup-count" title="termGroupsCount">
          {termGroupsCount}
        </span>
        <span className="term-slug" title={slug} data-termid={termId}>
          {name}
        </span>
        <span className="term-groups">
          {termGroupsNames}
        </span>
      </li>
    );
  }
}
