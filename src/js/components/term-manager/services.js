import {
  alphaAsc,
  alphaDesc,
  numericAsc,
  numericDesc,
} from 'lib/sorting';

import {
  getTermGroupsForTerm,
} from 'modules/taxonomies';

export const sorters = {
  alphaAsc: (a, b) => alphaAsc(a, b, 'name'),
  alphaDesc: (a, b) => alphaDesc(a, b, 'name'),
  usageAsc: (a, b) => numericAsc(a, b, 'streetviewimageCount'),
  usageDesc: (a, b) => numericDesc(a, b, 'streetviewimageCount'),

  alphaGroupAsc(a, b) {
    const termGroupA = getTermGroupsForTerm(a.$loki);
    const termGroupB = getTermGroupsForTerm(b.$loki);

    if (!termGroupA || !termGroupB) {
      return 0;
    }

    return alphaAsc(termGroupA.shift(), termGroupB.shift(), 'name');
  },

  alphaGroupDesc(a, b) {
    const termGroupA = getTermGroupsForTerm(a.$loki);
    const termGroupB = getTermGroupsForTerm(b.$loki);

    if (!termGroupA || !termGroupB) {
      return 0;
    }

    return alphaDesc(termGroupA.shift(), termGroupB.shift(), 'name');
  },
};

export function calculateTermsUsage(terms) {
  return terms.reduce((acc, term) => {
    const { items, groups } = acc;

    return {
      items: items + term.itemCount,
      streetviewimages: items + term.streetviewimageCount,
      groups: groups + term.termGroupCount,
    };
  }, {items: 0, groups: 0});
}
