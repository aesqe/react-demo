import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { createSelector } from 'reselect';
import SplitPane from 'react-split-pane';
import classNames from 'classnames';

import TermGroupManager from './term-group-manager';

import { getElData } from 'lib/util';
import { db } from 'lib/database';
import {
  addNewTaxonomyThunk,
  renameTaxonomyThunk,
  deleteTaxonomyThunk,
} from 'modules/taxonomies';

class TaxonomyManager extends PureComponent {
  constructor(props) {
    super(props);

    this.renderTaxonomy = this.renderTaxonomy.bind(this);
    this.renderTaxonomies = this.renderTaxonomies.bind(this);
    this.handleTaxonomySelect = this.handleTaxonomySelect.bind(this);
    this.handleSelectEmptyTaxonomy = this.handleSelectEmptyTaxonomy.bind(this);

    this.state = {
      selectedTaxonomies: [],
    };
  }

  handleTaxonomySelect(event) {
    const el = event.target;
    const value = getElData(el, 'taxonomyid', 'int');

    this.setState({
      selectedTaxonomies: [value],
    });
  }

  handleSelectEmptyTaxonomy() {
    this.setState({
      selectedTaxonomies: [],
    });
  }

  renderTaxonomy(taxonomy) {
    const { selectedTaxonomies } = this.state;
    const { $loki: taxonomyId, name } = taxonomy;

    const selected = selectedTaxonomies[0] === taxonomyId;

    const key = `taxonomy-list-${taxonomyId}`;
    const classes = classNames('taxonomy', {
      selected,
    });

    return (
      <li
        key={key}
        className={classes}
        data-taxonomyid={taxonomyId}
        onClick={this.handleTaxonomySelect}
      >
        {name}
      </li>
    );
  }

  renderTaxonomies() {
    const { taxonomies } = this.props;
    const { selectedTaxonomies } = this.state;

    const classes = classNames('taxonomy', {
      selected: selectedTaxonomies.length === 0,
    });

    return (
      <section className="term-manager-taxonomies">
        <ul className="taxonomies">
          <li
            id="taxonomy-empty"
            className={classes}
            onClick={this.handleSelectEmptyTaxonomy}
          >
            <span className="taxonomy-slug">
              -- no taxonomy --
            </span>
          </li>
          {taxonomies.map(this.renderTaxonomy)}
        </ul>
      </section>
    );
  }

  render() {
    const { selectedTaxonomies } = this.state;
    const { taxonomies, projectReady } = this.props;

    if (!projectReady || !db.loaded) {
      return (
        <Redirect to="/" />
      );
    }

    const singleTaxonomy = taxonomies.find(
      t => t.$loki === selectedTaxonomies[0]
    );

    const taxonomyName = singleTaxonomy && singleTaxonomy.name || 'tags';

    return (
      <div className="term-manager-wrapper">
        <div className="term-manager taxonomy-manager">
          <SplitPane split="vertical" minSize={0} defaultSize={0} maxSize={0}>
            {this.renderTaxonomies()}
            <TermGroupManager
              taxonomyName={taxonomyName}
            />
          </SplitPane>
        </div>
      </div>
    );
  }
}

const getPropsFromState = createSelector(
  (state) => state.project,
  (state) => state.collections.taxonomies,
  (project, taxonomies) => {
    return {
      taxonomies,
      projectReady: !!project.databaseFilePath,
    };
  }
);

const mapStateToProps = (state) => {
  return getPropsFromState(state);
};

const mapDispatchToProps = (dispatch) => ({
  addNewTaxonomy: (taxonomyName) => dispatch(
    addNewTaxonomyThunk(taxonomyName)
  ),
  renameTaxonomy: (taxonomyId, newName) => dispatch(
    renameTaxonomyThunk(taxonomyId, newName)
  ),
  deleteTaxonomy: (taxonomyId) => dispatch(
    deleteTaxonomyThunk(taxonomyId)
  ),
});

const TaxonomyManagerConnected = connect(mapStateToProps, mapDispatchToProps)(TaxonomyManager);

TaxonomyManagerConnected.defaultProps = {
  taxonomies: [],
};

export default TaxonomyManagerConnected;
