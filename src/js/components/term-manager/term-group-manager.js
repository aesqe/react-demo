import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import SplitPane from 'react-split-pane';
import { createSelector } from 'reselect';
import { Redirect } from 'react-router-dom';
import classNames from 'classnames';
import slugg from 'slugg';
import _ from 'lodash';

import {
  deleteTermGroups,
  addNewTermGroupThunk,
  mergeTermGroupsThunk,
  getTermsForTermGroups,
  getTermsInTaxonomy,
} from 'modules/taxonomies';
import { db } from 'lib/database';

import TermGroupOptions from './term-group-options';
import TermManagerTermGroups from './term-manager-term-groups';
import TermManager from './term-manager';
import { sorters } from './services';
import {
  termsSelector,
  projectSelector,
  termGroupsSelector,
  taxonomyFromNameSelector,
} from './selectors';
import './style.scss';

export class TermGroupManager extends PureComponent {
  constructor(props) {
    super(props);

    this.handleTermClick = this.handleTermClick.bind(this);
    this.handleTermGroupClick = this.handleTermGroupClick.bind(this);
    this.handleAddNewTermGroup = this.handleAddNewTermGroup.bind(this);
    this.handleTermGroupUpdate = this.handleTermGroupUpdate.bind(this);
    this.handleTermGroupDelete = this.handleTermGroupDelete.bind(this);
    this.deleteSelectedTermGroups = this.deleteSelectedTermGroups.bind(this);
    this.deselectTermGroups = this.deselectTermGroups.bind(this);
    this.renderTermGroupsSidebar = this.renderTermGroupsSidebar.bind(this);

    this.addNewTermGroup = this.addNewTermGroup.bind(this);
    this.setBusy = this.setBusy.bind(this);
    this.mergeSelected = this.mergeSelected.bind(this);

    this.sortDirAsc = this.sortDirAsc.bind(this);
    this.sortDirDesc = this.sortDirDesc.bind(this);
    this.sortByAlpha = this.sortByAlpha.bind(this);

    this.searchTermGroups = this.searchTermGroups.bind(this);
    this.handleInputSearchText = this.handleInputSearchText.bind(this);
    this.searchTermGroupsDebounced = _.debounce(this.searchTermGroups.bind(this), 150);

    this.flashingTimer = -1;
    this.addNewTermGroupInput = React.createRef();
    this.mergeTermGroupsInput = React.createRef();

    this.state = {
      selectedTermGroups: [],
      singleSelectedTermGroup: null,
      showOptions: false,
      busy: false,
      sortBy: 'alpha',
      sortDir: 'Asc',
      flashing: -1,
      filterVal: '',
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { termGroupsInTaxonomy, termsInTaxonomy, taxonomy } = nextProps;
    const { selectedTermGroups } = prevState;

    if (!termGroupsInTaxonomy.length) {
      return {
        selectedTermGroups: [],
        termsInSelectedTermGroups: termsInTaxonomy,
        singleSelectedTermGroup: null,
      };
    }

    const newSelectedTermGroups = termGroupsInTaxonomy.filter((termGroup) => {
      const finder = (t) => (t.$loki === termGroup.$loki && t.slug === termGroup.slug);
      return !!selectedTermGroups.find(finder);
    });
    const selectedCount = newSelectedTermGroups.length;
    const termGroupIds = newSelectedTermGroups.map(tg => tg.$loki);
    const singleSelectedTermGroup = selectedCount === 1 ? newSelectedTermGroups[0] : null;

    const termsInSelectedTermGroups = selectedCount ?
      getTermsForTermGroups(termGroupIds) :
      getTermsInTaxonomy(taxonomy.$loki);

    return {
      selectedTermGroups: newSelectedTermGroups,
      termsInSelectedTermGroups,
      singleSelectedTermGroup,
    };
  }

  componentWillUnmount() {
    clearTimeout(this.flashingTimer);
  }

  handleTermGroupClick( event, termGroup ) {
    this.setState({
      showOptions: true,
    });

    if (event === 'empty') {
      this.handleSelectEmptyTermGroup();
      return;
    }

    const { selectedTermGroups } = this.state;
    const { metaKey, ctrlKey } = event;

    const termGroupId = termGroup.$loki;
    const index = this.getSelectedTermGroupsIndex(termGroupId);
    const isSelected = index > -1;

    let newSelectedTermGroups;

    if (isSelected) {
      if (metaKey || ctrlKey) {
        newSelectedTermGroups = [
          ...selectedTermGroups.slice(0, index),
          ...selectedTermGroups.slice(index+1),
        ];
      } else {
        newSelectedTermGroups = [termGroup];
      }
    } else if (metaKey || ctrlKey) {
      newSelectedTermGroups = [
        ...selectedTermGroups,
        termGroup,
      ];
    } else {
      newSelectedTermGroups = [termGroup];
    }

    const termGroupIds = newSelectedTermGroups.map(tg => tg.$loki);
    const singleSelectedTermGroup = (newSelectedTermGroups.length === 1) ? termGroup : null;
    const termsInSelectedTermGroups = getTermsForTermGroups(termGroupIds);


    this.setState({
      selectedTermGroups: newSelectedTermGroups,
      termsInSelectedTermGroups,
      singleSelectedTermGroup,
    });
  }

  handleTermClick() {
    this.setState({
      showOptions: false,
    });
  }

  flashTermGroup(termGroupId) {
    this.setState({
      flashing: termGroupId,
    });

    this.flashingTimer = setTimeout(() => {
      this.setState({
        flashing: -1,
      });
    }, 1500);
  }

  getFilteredTermGroups(){
    const { termGroupsInTaxonomy } = this.props;
    const { filterVal, sortBy, sortDir } = this.state;

    if (termGroupsInTaxonomy.length === 0) {
      return termGroupsInTaxonomy;
    }

    const sortFuncName = `${sortBy}${sortDir}`;
    const sort = sorters[sortFuncName];

    if (!sort) {
      return termGroupsInTaxonomy;
    }

    let filterFunc;

    if (filterVal === '') {
      filterFunc = Boolean;
    } else {
      const rxName = new RegExp(filterVal, 'i');
      const rxSlug = new RegExp(slugg(filterVal), 'i');

      filterFunc = (termGroup) => {
        const { name, slug } = termGroup;
        return (name.match(rxName) || slug.match(rxSlug));
      };
    }

    return termGroupsInTaxonomy
      .filter(filterFunc)
      .sort(sort);
  }

  sortBy(propName) {
    const { sortBy } = this.state;

    if (sortBy === propName) {
      return;
    }

    this.deselectTermGroups();
    this.setState({
      sortBy: propName,
    });
  }

  sortDir(dir) {
    const { sortDir } = this.state;

    if (sortDir === dir) {
      return;
    }

    this.deselectTermGroups();
    this.setState({
      sortDir: dir,
    });
  }

  sortByAlpha() {
    this.sortBy('alpha');
  }

  sortDirAsc() {
    this.sortDir('Asc');
  }

  sortDirDesc() {
    this.sortDir('Desc');
  }

  handleInputSearchText(event) {
    event.persist();

    return this.searchTermGroupsDebounced(event);
  }

  searchTermGroups(event) {
    const filterVal = event.target.value;

    this.setState({
      filterVal,
    });
  }

  deselectTermGroups() {
    this.setState({
      selectedTermGroups: [],
      singleSelectedTermGroup: null,
      termsInSelectedTermGroups: this.props.terms,
    });
  }

  handleSelectEmptyTermGroup() {
    this.deselectTermGroups();
  }

  getSelectedTermGroupsIndex(termGroupId) {
    const { selectedTermGroups } = this.state;
    return selectedTermGroups.findIndex(termGroup => (termGroup.$loki === termGroupId));
  }

  handleTermGroupDelete() {
    this.deselectTermGroups();
    this.setBusy(false);
  }

  handleTermGroupUpdate(termGroupId) {
    const { termGroupsInTaxonomy } = this.props;

    const termGroup = termGroupsInTaxonomy.find(t => t.$loki === termGroupId);

    this.setBusy(false);

    this.setState({
      selectedTermGroups: [termGroup],
      singleSelectedTermGroup: termGroup,
    });
  }

  mergeSelected(event) {
    if (event.keyCode !== 13) {
      return;
    }

    const { selectedTermGroups } = this.state;

    const mergeNameEl = this.mergeTermGroupsInput.current;
    const { value: mergedName } = mergeNameEl;

    const termGroupIds = selectedTermGroups.map(termGroup => termGroup.$loki);

    this.props.mergeTermGroups(mergedName, termGroupIds)
      .then((mergedTermGroup) => {
        mergeNameEl.value = '';

        this.setState({
          selectedTermGroups: [mergedTermGroup],
          singleSelectedTermGroup: mergedTermGroup,
          scrollTo: mergedTermGroup.$loki,
          flashing: mergedTermGroup.$loki,
        });
      });
  }

  handleAddNewTermGroup(event) {
    if (event.keyCode !== 13) {
      return;
    }

    this.addNewTermGroup();
  }

  addNewTermGroup() {
    const { taxonomy: { $loki: taxonomyId } } = this.props;

    const inputEl = this.addNewTermGroupInput.current;
    const { value: termGroupName } = inputEl;

    this.props.addNewTermGroup(termGroupName, taxonomyId)
      .then((newTermGroup) => {
        inputEl.value = '';

        this.setState({
          selectedTermGroups: [newTermGroup],
          singleSelectedTermGroup: newTermGroup,
          scrollTo: newTermGroup.$loki,
          flashing: newTermGroup.$loki,
        });
      });
  }

  deleteSelectedTermGroups() {
    if (confirm('Delete ALL selected term groups: are you sure?')) {
      return;
    }

    const { selectedTermGroups } = this.state;

    deleteTermGroups(selectedTermGroups);

    this.setState({
      selectedTermGroups: [],
      singleSelectedTermGroup: null,
      flashing: -1,
    });
  }

  setBusy(val) {
    this.setState({
      busy: val,
    });
  }

  renderMultiGroupsOptions() {
    const { merging, selectedTermGroups } = this.state;

    const selectedTermGroupsNames = selectedTermGroups.map(termGroup => termGroup.name).join(', ');

    return (
      <React.Fragment>
        {selectedTermGroups.length > 1 && (
          <div className="multi-term-options">
            <div className="merge-terms options-block">
              <label htmlFor="merge-term-groups">Merge as</label>
              <input
                name="merge-term-groups"
                type="text"
                onKeyUp={this.mergeSelected}
                ref={this.mergeTermGroupsInput}
              />
            </div>

            <div className="delete-terms options-block fullwidth">
              <button onClick={this.deleteSelectedTermGroups}>
                Delete Selected Term groups
              </button>
            </div>

            <div className="deselect-terms options-block fullwidth">
              <button onClick={this.deselectTermGroups}>
                Deselect all Term groups
              </button>
            </div>

            <div className="stats">
              {merging && (
                <p>Merging <strong>{selectedTermGroupsNames}</strong></p>
              )}
              {!merging && (
                <p>Term groups <strong>{selectedTermGroups.map(termGroup => termGroup.name).join(', ')}</strong> can be merged</p>
              )}
            </div>
          </div>
        )}
      </React.Fragment>
    );
  }

  renderTermGroupsSidebar() {
    const {
      busy,
      sortBy,
      sortDir,
      singleSelectedTermGroup,
    } = this.state;

    const sortClasses = {
      sortDirAsc: sortDir === 'Asc' ? 'active' : '',
      sortDirDesc: sortDir === 'Desc' ? 'active' : '',
      sortByAlpha: sortBy === 'alpha' ? 'active' : '',
      sortByGroup: sortBy === 'alphaGroup' ? 'active' : '',
    };

    return (
      <div className="options options-terms options-term-groups">

        <div className="sorting options-section">
          <h3 className="item-label">View / sort</h3>

          <div className="search-terms search-term-groups options-block">
            <label htmlFor="search-term-groups">Search</label>
            <input
              type="text"
              id="search-term-groupss"
              onKeyUp={this.handleInputSearchText}
            />
          </div>

          <div className="sort-terms sort-term-groups options-block">
            <button onClick={this.sortDirAsc} className={sortClasses.sortDirAsc}>ASC</button>
            <button onClick={this.sortDirDesc} className={sortClasses.sortDirDesc}>DESC</button>
            <button onClick={this.sortByAlpha} className={sortClasses.sortByAlpha}>ABC</button>
          </div>
        </div>

        <div className="actions options-section">
          <h3 className="item-label">Action</h3>

          <div className="new-term options-block">
            <label htmlFor="new-term">New term group</label>
            <input
              id="new-term-group"
              type="text"
              onKeyUp={this.handleAddNewTermGroup}
              ref={this.addNewTermGroupInput}
            />
          </div>

          {singleSelectedTermGroup && (
            <TermGroupOptions
              termGroup={singleSelectedTermGroup}
              busy={busy}
              busySetter={this.setBusy}
              onTermGroupDelete={this.handleTermGroupDelete}
              onTermGroupUpdate={this.handleTermGroupUpdate}
            />
          )}

          {this.renderMultiGroupsOptions()}
        </div>

      </div>
    );
  }

  render() {
    const {
      busy,
      flashing,
      showOptions,
      selectedTermGroups,
      termsInSelectedTermGroups,
    } = this.state;
    const { projectReady, taxonomyName } = this.props;

    if (!projectReady || !db.loaded) {
      return (
        <Redirect to="/" />
      );
    }

    const filteredTermGroups = this.getFilteredTermGroups();
    const managerClasses = classNames('term-manager', 'term-group-manager', {
      busy,
    });

    const showTermsOptions = !showOptions;
    const termsSidebarRenderer = showOptions ? this.renderTermGroupsSidebar : null;

    return(
      <div className={managerClasses}>
        <SplitPane split="vertical" minSize={200} defaultSize={300} maxSize={400}>
          <TermManagerTermGroups
            termGroups={filteredTermGroups}
            selectedTermGroups={selectedTermGroups}
            flashing={flashing}
            onTermGroupClick={this.handleTermGroupClick}
          />
          <TermManager
            taxonomyName={taxonomyName}
            terms={termsInSelectedTermGroups}
            onTermClick={this.handleTermClick}
            showOptions={showTermsOptions}
            sidebarRenderer={termsSidebarRenderer}
          />
        </SplitPane>
      </div>
    );
  }
}

const stateToPropsSelector = (project, allTerms, allTermGroups, taxonomy) => {
  const projectReady = !!project.databaseFilePath;

  if (!taxonomy) {
    return {
      taxonomy: null,
      termsInTaxonomy: [],
      termGroupsInTaxonomy: [],
      projectReady,
    };
  }

  const taxonomyId = taxonomy.$loki;
  const termGroupsInTaxonomy = allTermGroups.filter(t => t.taxonomyId === taxonomyId);
  const termsInTaxonomy = allTerms.filter(t => t.taxonomyId === taxonomyId);

  return {
    termsInTaxonomy,
    termGroupsInTaxonomy,
    taxonomy,
    projectReady,
  };
};

const getPropsFromState = createSelector(
  projectSelector,
  termsSelector,
  termGroupsSelector,
  taxonomyFromNameSelector,
  stateToPropsSelector,
);

const mapStateToProps = (state, ownProps) => {
  return getPropsFromState(state, ownProps);
};

const mapDispatchToProps = (dispatch) => ({
  addNewTermGroup: (termGroupName, taxonomyId) =>
    dispatch(addNewTermGroupThunk(termGroupName, taxonomyId)),
  mergeTermGroups: (termGroupIds, mergedName) =>
    dispatch(mergeTermGroupsThunk(termGroupIds, mergedName)),
});

const TermGroupManagerConnected = connect(
  mapStateToProps,
  mapDispatchToProps
)(TermGroupManager);

TermGroupManagerConnected.defaultProps = {
  terms: [],
  taxonomyName: '',
};

export default TermGroupManagerConnected;
