import React, { PureComponent } from 'react';
import classNames from 'classnames';

import TermManagerTaxonomy from './term-manager-taxonomy';

/**
 * Displays a list of taxonomies in TermManager
 */
export default class TermManagerTaxonomies extends PureComponent {
  constructor(props) {
    super(props);

    this.renderTaxonomy = this.renderTaxonomy.bind(this);
    this.handleSelectEmptyTaxonomy = this.handleSelectEmptyTaxonomy.bind(this);
  }

  handleSelectEmptyTaxonomy() {
    this.props.onTaxonomyClick('empty');
  }

  isTaxonomySelected(taxonomyId) {
    const { selectedTaxonomies } = this.props;

    const index = selectedTaxonomies.findIndex(t => (t.$loki === taxonomyId));

    return index > -1;
  }

  renderTaxonomy(taxonomy) {
    const { onTaxonomyClick, flashing } = this.props;
    const { $loki: taxonomyId } = taxonomy;

    const key = `term-manager-taxonomy-${taxonomyId}`;

    const isSelected = this.isTaxonomyelected(taxonomyId);
    const isFlashing = flashing === taxonomyId;

    return (
      <TermManagerTaxonomy
        key={key}
        taxonomy={taxonomy}
        flashing={isFlashing}
        selected={isSelected}
        onClick={onTaxonomyClick}
      />
    );
  }

  render() {
    const { taxonomies, selectedTaxonomies } = this.props;

    const classes = classNames('term', {
      selected: selectedTaxonomies.length === 0,
    });

    return(
      <section className="term-manager-taxonomies">
        <ul className="taxonomies">
          <li
            id="taxonomy-empty"
            className={classes}
            onClick={this.handleSelectEmptyTaxonomy}
          >
            <span className="taxonomy-slug">
              -- no taxonomy --
            </span>
          </li>
          {taxonomies.map(this.renderTaxonomy)}
        </ul>
      </section>
    );
  }
}
