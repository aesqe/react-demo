import React, { PureComponent } from 'react';
import classNames from 'classnames';

import TermManagerTerm from './term-manager-term';

/**
 * Displays a list of terms in TermManager
 */
export default class TermManagerTerms extends PureComponent {
  constructor(props) {
    super(props);

    this.renderTerm = this.renderTerm.bind(this);
    this.handleSelectEmptyTerm = this.handleSelectEmptyTerm.bind(this);
  }

  handleSelectEmptyTerm() {
    this.props.onTermClick('empty');
  }

  isTermSelected(termId) {
    const { selectedTerms } = this.props;

    const index = selectedTerms
      .findIndex(t => (t.$loki === termId));

    return index > -1;
  }

  renderTerm(term) {
    const { onTermClick, flashing } = this.props;
    const { $loki: termId } = term;

    const isSelected = this.isTermSelected(termId);
    const key = `term-manager-term-${termId}`;
    const isFlashing = flashing === termId;

    return (
      <TermManagerTerm
        key={key}
        term={term}
        flashing={isFlashing}
        selected={isSelected}
        onClick={onTermClick}
      />
    );
  }

  renderTerms() {
    const { terms } = this.props;

    return terms
      .filter(t => !!t.$loki)
      .map(this.renderTerm);
  }

  render() {
    const { selectedTerms } = this.props;

    const classes = classNames('term', {
      selected: selectedTerms.length === 0,
    });

    return(
      <section className="term-manager-terms">
        <ul className="terms">
          <li id="term-empty" className={classes} onClick={this.handleSelectEmptyTerm}>
            <span className="term-slug">
              -- no term --
            </span>
          </li>
          {this.renderTerms()}
        </ul>
      </section>
    );
  }
}
