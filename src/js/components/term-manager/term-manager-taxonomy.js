import React, { PureComponent} from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';

/**
 * Displays a single taxonomy in TermManager
 */
export default class TermManagerTaxonomy extends PureComponent {
  constructor(props) {
    super(props);

    this.taxonomyNodeRef = React.createRef();
  }

  componentDidMount() {
    this.scrollToDomRef();
  }

  componentDidUpdate() {
    this.scrollToDomRef();
  }

  scrollToDomRef() {
    const { scrollTo } = this.props;

    if (scrollTo) {
      const node = ReactDOM.findDOMNode(this.taxonomyNodeRef.current);
      node.scrollIntoView({
        behavior: 'smooth',
      });
    }
  }

  render() {
    const { taxonomy, flashing, selected, onClick } = this.props;
    const { $loki: taxonomyId, slug, name, color } = taxonomy;

    const id = `taxonomy-${taxonomyId}`;

    const classes = classNames('taxonomy', {
      flashing: flashing === taxonomyId,
      selected,
    });

    function handleClick(event) {
      onClick(event, taxonomy);
    }

    return (
      <li id={id} className={classes} onClick={handleClick} ref={this.taxonomyNodeRef}>
        <span className="taxonomy-slug" title={slug} data-taxonomyid={taxonomyId}>
          {name}
        </span>
        <span className="taxonomy-color" style={{backgroundColor: color}}></span>
      </li>
    );
  }
}
