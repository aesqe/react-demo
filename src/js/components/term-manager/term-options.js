import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import slugg from 'slugg';

import { updateDocumentThunk } from 'reducers';
import { getElData } from 'lib/util';
import {
  renameTermThunk,
  deleteTermThunk,
} from 'modules/taxonomies';
import { Confirm } from 'components/dialog';

/**
 * Displays term options sidebar
 */
class TermOptions extends PureComponent {
  constructor(props) {
    super(props);

    this.inputRef = React.createRef();
    this.colorRef = React.createRef();

    this.updateTerm = this.updateTerm.bind(this);
    this.deleteTerm = this.deleteTerm.bind(this);
    this.updateTermOnKeyUp = this.updateTermOnKeyUp.bind(this);
    this.handleDeleteConfirm = this.handleDeleteConfirm.bind(this);
    this.handleDeleteDeny = this.handleDeleteDeny.bind(this);

    this.state = {
      busy: false,
      hasChanged: false,
      renameError: false,
      showDeleteConfirm: false,
    };
  }

  static getDerivedStateFromProps(nextProps) {
    return {
      busy: nextProps.busy,
    };
  }

  componentDidUpdate(prevProps) {
    const prevTerm = prevProps.term;
    const { term } = this.props;

    if(prevTerm !== term) {
      this.inputRef.current.value = term.name;
      this.colorRef.current.value = term.color;
    }
  }

  setBusy(val) {
    this.setState({
      busy: val,
    });

    this.props.busySetter(val);
  }

  busySetter(val) {
    this.setState({
      busy: val,
    });
  }

  updateTerm(event) {
    const { term } = this.props;

    const el = event.target;
    const propName = getElData(el, 'prop');

    switch(propName) {
      case 'name':
        this.renameTerm();
        break;
      case 'color':
        this.updateTermProp(propName, el.value);
        break;
      case 'visible':
        this.updateTermProp(propName, !term.visible);
        break;
      default:
        return;
    }
  }

  updateTermOnKeyUp(event) {
    const { value } = this.inputRef.current;
    const { name } = this.props.term;

    this.setState({
      hasChanged: value !== name,
    });

    if (event.keyCode !== 13) {
      return;
    }

    this.updateTerm(event);
  }

  updateTermProp(prop, val) {
    const { term } = this.props;

    const patch = { [prop]: val };

    this.props.updateTerm(term, patch)
      .then(() => {
        this.setState({
          hasChanged: false,
        });

        this.props.onTermUpdate(term.$loki);
      });
  }

  renameTerm() {
    const { term: { $loki: termId } } = this.props;

    const el = this.inputRef.current;
    const { value: newName } = el;

    this.setBusy(true);

    this.props.renameTerm(termId, newName)
      .then((updated) => {
        if (!updated) {
          alert(`term with that slug (${slugg(newName)})already exists`);
        }

        this.setState({
          hasChanged: false,
        });

        this.setBusy(false);
        this.props.onTermUpdate(termId);
      });
  }

  deleteTerm() {
    this.setState({
      showDeleteConfirm: true,
    });
  }

  handleDeleteConfirm() {
    const { term } = this.props;

    this.setBusy(false);

    this.setState({
      showDeleteConfirm: false,
    });

    this.props.deleteTerm(term)
      .then(this.props.onTermDelete);
  }

  handleDeleteDeny() {
    this.setBusy(false);

    this.setState({
      showDeleteConfirm: false,
    });
  }

  render() {
    const { term } = this.props;
    const {
      busy,
      hasChanged,
      renameError,
      showDeleteConfirm,
    } = this.state;

    if (!term) {
      // eslint-disable-next-line no-console
      console.log(this.props);
      return null;
    }

    const {
      $loki: termId,
      name,
      slug,
      color,
      visible,
      streetviewimageCount,
      // groupCount,
    } = term;

    const deleteConfirmDescription = `Are you sure you want to delete the term "${term.name}"?`;

    return (
      <div className="single-term-options">
        <div className="rename rename-term options-block with-button">
          <label htmlFor="rename-term">Rename</label>
          <input
            type="text"
            name="rename-term"
            data-prop="name"
            defaultValue={name}
            ref={this.inputRef}
            onKeyUp={this.updateTermOnKeyUp}
          />
          {hasChanged && (
            <button data-prop="name" onClick={this.updateTerm} disabled={busy}>
              OK
            </button>
          )}
          {renameError && (
            <div className="error">
              {renameError}
            </div>
          )}
        </div>

        <div className="term-color options-block">
          <label htmlFor="term-color">Term color</label>
          <input
            data-prop="color"
            name="term-color"
            type="color"
            defaultValue={color}
            ref={this.colorRef}
            onChange={this.updateTerm}
          />
        </div>

        <div className="delete-term options-block fullwidth">
          <button onClick={this.deleteTerm}>
            Delete
          </button>
        </div>

        {showDeleteConfirm && (
          <Confirm
            description={deleteConfirmDescription}
            confirmLabel="Yes"
            denyLabel="No"
            onConfirm={this.handleDeleteConfirm}
            onDeny={this.handleDeleteDeny}
          />
        )}

        <div className="visible-term options-block fullwidth">
          <button data-prop="visible" onClick={this.updateTerm}>
            {visible ? 'Visible' : 'Invisible'}
          </button>
        </div>

        <div className="stats">
          <span className="label"><strong>ID</strong>: {termId}</span><br />
          <span className="label"><strong>Name</strong>: {name}</span><br />
          <span className="label"><strong>Slug</strong>: {slug}</span><br />
          <span className="label">Images tagged with term: {streetviewimageCount}</span><br />
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  renameTerm: (term, name) => dispatch(renameTermThunk(term, name)),
  updateTerm: (term, patch) => dispatch(updateDocumentThunk(term, patch)),
  deleteTerm: (term) => dispatch(deleteTermThunk(term)),
});

const TermOptionsConnected = connect(null, mapDispatchToProps)(TermOptions);

export default TermOptionsConnected;

