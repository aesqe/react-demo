import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { updateDocumentThunk } from 'reducers';
import { getElData } from 'lib/util';
import {
  renameTermGroupThunk,
  deleteTermGroupThunk,
} from 'modules/taxonomies';
import { Confirm } from 'components/dialog';

/**
 * Displays termGroup options sidebar
 */
class TermGroupOptions extends PureComponent {
  constructor(props) {
    super(props);

    this.inputRef = React.createRef();
    this.colorRef = React.createRef();

    this.updateTermGroup = this.updateTermGroup.bind(this);
    this.deleteTermGroup = this.deleteTermGroup.bind(this);
    this.updateTermGroupOnKeyUp = this.updateTermGroupOnKeyUp.bind(this);
    this.handleDeleteConfirm = this.handleDeleteConfirm.bind(this);
    this.handleDeleteDeny = this.handleDeleteDeny.bind(this);

    this.state = {
      busy: false,
      hasChanged: false,
      renameError: false,
      showDeleteConfirm: false,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return {
      ...prevState,
      busy: nextProps.busy,
    };
  }

  componentDidUpdate(prevProps) {
    const prevTermGroup = prevProps.termGroup;
    const { termGroup } = this.props;

    if(prevTermGroup !== termGroup) {
      this.inputRef.current.value = termGroup.name;
      this.colorRef.current.value = termGroup.color;
    }
  }

  setBusy(val) {
    this.setState({
      busy: val,
    });

    this.props.busySetter(val);
  }

  busySetter(val) {
    this.setState({
      busy: val,
    });
  }

  updateTermGroup(event) {
    const { termGroup } = this.props;

    const el = event.target;
    const propName = getElData(el, 'prop');

    switch(propName) {
      case 'name':
        this.renameTermGroup();
        break;
      case 'color':
        this.updateTermGroupProp(propName, el.value);
        break;
      case 'visible':
        this.updateTermGroupProp(propName, !termGroup.visible);
        break;
      default:
        return;
    }
  }

  updateTermGroupOnKeyUp(event) {
    const { value } = this.inputRef.current;
    const { name } = this.props.termGroup;

    this.setState({
      hasChanged: value !== name,
    });

    if (event.keyCode !== 13) {
      return;
    }

    this.updateTermGroup(event);
  }

  updateTermGroupProp(prop, val) {
    const { termGroup } = this.props;

    const patch = { [prop]: val };

    this.props.updateTermGroup(termGroup, patch)
      .then(() => {
        this.setState({
          hasChanged: false,
        });

        this.props.onTermGroupUpdate(termGroup.$loki);
      });
  }

  renameTermGroup() {
    const { termGroup: { $loki: termGroupId } } = this.props;

    const el = this.inputRef.current;
    const { value: newName } = el;

    this.setBusy(true);

    this.props.renameTermGroup(termGroupId, newName)
      .then(() => {
        this.props.onTermGroupUpdate(termGroupId);
      });
  }

  deleteTermGroup() {
    this.setState({
      showDeleteConfirm: true,
    });
  }

  handleDeleteConfirm() {
    const { termGroup } = this.props;

    this.setBusy(false);

    this.setState({
      showDeleteConfirm: false,
    });

    this.props.deleteTermGroup(termGroup)
      .then(this.props.onTermGroupDelete);
  }

  handleDeleteDeny() {
    this.setBusy(false);

    this.setState({
      showDeleteConfirm: false,
    });
  }

  render() {
    const { termGroup } = this.props;

    if (!termGroup) {
      // eslint-disable-next-line no-console
      console.log(this.props);
      return null;
    }

    const {
      busy,
      hasChanged,
      renameError,
      showDeleteConfirm,
    } = this.state;

    const {
      $loki: termGroupId,
      name,
      slug,
      color,
      visible,
    } = termGroup;

    const deleteConfirmDescription = `Are you sure you want to
      delete the term group "${termGroup.name}"?`;

    return (
      <div className="single-term-options single-term-group-options">
        <div className="rename rename-term rename-term-group options-block with-button">
          <label htmlFor="rename-term rename-term-group">Rename</label>
          <input
            type="text"
            name="rename-term-group"
            data-prop="name"
            defaultValue={name}
            ref={this.inputRef}
            onKeyUp={this.updateTermGroupOnKeyUp}
          />
          {hasChanged && (
            <button
              data-prop="name"
              onClick={this.updateTermGroup}
              disabled={busy}
            >
              OK
            </button>
          )}
          {renameError && (
            <div className="error">
              {renameError}
            </div>
          )}
        </div>

        <div className="term-color term-group-color options-block">
          <label htmlFor="term-color term-group-color">Term group color</label>
          <input
            data-prop="color"
            name="term-group-color"
            type="color"
            defaultValue={color}
            ref={this.colorRef}
            onChange={this.updateTermGroup}
          />
        </div>

        <div className="delete-term delete-term-group options-block fullwidth">
          <button onClick={this.deleteTermGroup}>
            Delete
          </button>
        </div>

        {showDeleteConfirm && (
          <Confirm
            description={deleteConfirmDescription}
            confirmLabel="Yes"
            denyLabel="No"
            onConfirm={this.handleDeleteConfirm}
            onDeny={this.handleDeleteDeny}
          />
        )}

        <div className="visible-term visible-term-group options-block fullwidth">
          <button
            data-prop="visible"
            onClick={this.updateTermGroup}
          >
            {visible ? 'Visible' : 'Invisible'}
          </button>
        </div>

        <div className="stats">
          <span className="label"><strong>ID</strong>: {termGroupId}</span><br />
          <span className="label"><strong>Name</strong>: {name}</span><br />
          <span className="label"><strong>Slug</strong>: {slug}</span><br />
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  renameTermGroup: (termGroup, name) => dispatch(renameTermGroupThunk(termGroup, name)),
  updateTermGroup: (termGroup, patch) => dispatch(updateDocumentThunk(termGroup, patch)),
  deleteTermGroup: (termGroup) => dispatch(deleteTermGroupThunk(termGroup)),
});

const TermGroupOptionsConnected = connect(null, mapDispatchToProps)(TermGroupOptions);

export default TermGroupOptionsConnected;

