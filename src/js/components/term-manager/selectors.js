export const taxonomyFromNameSelector = (state, ownProps) => {
  const { taxonomyName } = ownProps;
  const { taxonomies } = state.collections;

  if (!taxonomyName || taxonomies.length === 0) {
    return false;
  }

  const taxonomy = taxonomies.find(t => t.slug === taxonomyName);

  if (!taxonomy) {
    throw new Error(`Taxonomy ${taxonomyName} does not exist`);
  }

  return taxonomy;
};

export const projectSelector = (state) => state.project;

export const taxonomiesSelector = (state) => state.collections.taxonomies;

export const termsSelector = (state) => state.collections.terms;

export const termGroupsSelector = (state) => state.collections.termgroups;
