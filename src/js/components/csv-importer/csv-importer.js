import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import csv from 'csv-parser';
import fs from 'fs-extra';
import lineReader from 'line-reader';
import toReadableStream from 'to-readable-stream';
import slugg from 'slugg';
import camelcase from 'camelcase';
import _ from 'lodash';

import {
  reloadCollectionAction,
  addNewCollectionThunk,
  addNewDocumentThunk,
} from 'reducers';
import { TAXONOMIES, TERMS, PRESETS } from 'data/defaults/default-collections.json';
import { db } from 'lib/database';
import { countEmptyStringSegments, convertToType, toFixed } from 'lib/util';
import { openCSVFileDialog } from 'modules/dialogs';
import { addNewTaxonomyThunk, addNewTermThunk } from 'modules/taxonomies';
import { Prompt } from 'components/dialog';
import './style.scss';

// https://twitter.com/TheOctoNation/status/1111068549483773953
export class CsvImporter extends PureComponent {
  constructor(props) {
    super(props);

    this.parseCsv = this.parseCsv.bind(this);
    this.parseCsvAndCamelSlugHeaders = this.parseCsvAndCamelSlugHeaders.bind(this);
    this.readFile = this.readFile.bind(this);
    this.updateOptions = this.updateOptions.bind(this);
    this.getFirstLines = this.getFirstLines.bind(this);
    this.handleOpenFileDialog = this.handleOpenFileDialog.bind(this);
    this.sortByContentLength = this.sortByContentLength.bind(this);
    this.renderColumn = this.renderColumn.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleTypeChange = this.handleTypeChange.bind(this);
    this.handleImportChange = this.handleImportChange.bind(this);
    this.handleTermNameChange = this.handleTermNameChange.bind(this);
    this.handleTaxonomyNameChange = this.handleTaxonomyNameChange.bind(this);
    this.handleConvertToTermsChange = this.handleConvertToTermsChange.bind(this);
    this.handleDescriptionConversion = this.handleDescriptionConversion.bind(this);
    this.handleTruncateChange = this.handleTruncateChange.bind(this);
    this.handleToFixedChange = this.handleToFixedChange.bind(this);
    this.handleimportAllToggle = this.handleimportAllToggle.bind(this);
    this.closeImportModal = this.closeImportModal.bind(this);
    this.runCsvImport = this.runCsvImport.bind(this);
    this.saveFilePath = this.saveFilePath.bind(this);
    this.addTaxonomyAndTerm = this.addTaxonomyAndTerm.bind(this);
    this.importData = this.importData.bind(this);
    this.checkRequiredValues = this.checkRequiredValues.bind(this);
    this.handleLngLatConversion = this.handleLngLatConversion.bind(this);
    this.saveImportPreset = this.saveImportPreset.bind(this);
    this.handleSaveImportPresetOk = this.handleSaveImportPresetOk.bind(this);
    this.handleSaveImportPresetCancel = this.handleSaveImportPresetCancel.bind(this);
    this.toggleImportPresetNamePrompt = this.toggleImportPresetNamePrompt.bind(this);
    this.handleSaveImportPresetChange = this.handleSaveImportPresetChange.bind(this);
    this.renderPreset = this.renderPreset.bind(this);

    this.state = {
      csvData: [],
      csvFirstLines: [],
      csvFilePath: '',
      separator: ',',
      collectionName: '',
      columnNames: [],
      importSettings: {},
      taxonomies: {},
      workOrders: [],
      importOrders: [],
      importAll: true,
      importErrors: [],
      importing: false,
      finishedImporting: false,
      saveImportPresetError: '',
      importSettingsPresetTitle: 'Import settings preset',
      displayImportPresetNamePrompt: false,
    };

    this.taxonomies = {};
  }

  updateOptions(event) {
    const el = event.target;
    const { name, value } = el;

    this.setState({
      [name]: value,
    }, this.checkRequiredValues);
  }

  parseCsv(input, opts) {
    const { separator } = this.state;

    const inputIsString = (typeof input === 'string');
    const readableStream = inputIsString ? toReadableStream(input) : input;

    return new Promise((resolve, reject) => {
      const options = {
        separator,
        ...opts,
      };
      const results = [];
      const parser = readableStream.pipe(csv(options));

      parser.on('data', (data) => results.push(data));
      parser.on('error', (err) => reject(err.message));
      parser.on('end', () => resolve(results));
    });
  }

  parseCsvAndCamelSlugHeaders(data) {
    const options = {
      mapHeaders: ({ header }) => camelcase(slugg(header)),
    };

    return this.parseCsv(data, options);
  }

  getDefaultColumnData(i, val) {
    return {
      index: i,
      import: true,
      type: 'string',
      originalName: val,
      name: val,
      truncate: 0,
      toFixed: 0,
      toTerm: false,
      termName: `${val}-$VALUE`,
      taxonomyName: 'tags',
    };
  }

  saveFilePath(csvFilePath) {
    this.setState({ csvFilePath });

    return csvFilePath;
  }

  handleOpenFileDialog() {
    return openCSVFileDialog()
      .then(this.saveFilePath)
      .then(this.getFirstLines)
      .then(this.parseCsvAndCamelSlugHeaders)
      .then((csvFirstLines) => {
        const exampleValues = this.getExampleValues(csvFirstLines);
        const columnNames = Object.keys(exampleValues);
        const importSettings = columnNames.reduce((obj, val, i) => {
          obj[`column-${i}`] = this.getDefaultColumnData(i, val);
          return obj;
        }, {});

        this.setState({
          exampleValues,
          columnNames,
          importSettings,
          importErrors: [],
        });
      });
  }

  presetNameExists(title) {
    const res = db.getCollection(PRESETS).find({
      title,
      component: 'csv-importer',
    });

    return res.length > 0;
  }

  handleSaveImportPresetChange(val) {
    if (!val) {
      this.setState({
        saveImportPresetError: 'Value must not be empty',
      });
    } else {
      if (this.presetNameExists(val)) {
        this.setState({
          saveImportPresetError: 'Preset name already exists',
        });
      } else {
        this.setState({
          importSettingsPresetTitle: val,
          saveImportPresetError: '',
        });
      }
    }
  }

  handleSaveImportPresetOk() {
    if (this.state.saveImportPresetError) {
      return;
    }

    this.saveImportPreset();
  }

  handleSaveImportPresetCancel() {
    this.setState({
      displayImportPresetNamePrompt: false,
    });
  }

  toggleImportPresetNamePrompt() {
    this.setState({
      displayImportPresetNamePrompt: !this.state.displayImportPresetNamePrompt,
    });
  }

  async saveImportPreset() {
    const { importSettings, importSettingsPresetTitle } = this.state;

    const preset = {
      collectionName: PRESETS,
      itemData: {
        title: importSettingsPresetTitle,
        component: 'csv-importer',
        settings: _.cloneDeep(importSettings),
      },
    };

    await this.props.saveImportPreset(preset);

    this.setState({
      displayImportPresetNamePrompt: false,
    });
  }

  getExampleValues(csvFirstLines) {
    const values = {};
    const columnNames = Object.keys(csvFirstLines[0]);
    const columns = new Set(columnNames);
    const len = csvFirstLines.length;
    let i = 0;

    for(i; i < len; i++) {
      const row = csvFirstLines[i];

      columnNames.forEach((columnName) => {
        if (typeof values[columnName] === 'undefined') {
          const val = row[columnName];

          if (val) {
            values[columnName] = val.slice(0, 300);
            columns.delete(columnName);
          } else {
            values[columnName] = void 0;
          }
        }
      });

      if (columns.size === 0) {
        break;
      }
    }

    return values;
  }

  readFile(filePath) {
    return fs.createReadStream(filePath);
  }

  getFirstLines(filePath, lineCount = 100) {
    const lines = [];
    let i = 0;

    return new Promise((resolve, reject) => {
      try {
        lineReader.eachLine(filePath, (line) => {
          lines.push(line);

          if (++i === lineCount) {
            lines.sort(this.sortByContentLength);
            resolve(lines.join('\n'));
            return false;
          }

          return true;
        });
      } catch (err) {
        reject(err);
      }
    });
  }

  sortByContentLength(a, b) {
    const { separator } = this.state;

    const ac = countEmptyStringSegments(a, separator);
    const bc = countEmptyStringSegments(b, separator);

    return ac - bc;
  }

  renderImportOptions() {
    const { separator } = this.state;

    return (
      <div>
        <h4>Import CSV file</h4>
        <p className="separator">
          Column separator:&nbsp;
          <span className="radio-group">
            <label>
              <input type="radio" name="separator" value="," checked={separator === ','} onChange={this.updateOptions} />
              <span>,</span>
            </label>
            <label>
              <input type="radio" name="separator" value=";" checked={separator === ';'} onChange={this.updateOptions} />
              <span>;</span>
            </label>
            <label>
              <input type="radio" name="separator" value=":" checked={separator === ':'} onChange={this.updateOptions} /> <span>:</span>
            </label>
          </span>
        </p>
        <button onClick={this.handleOpenFileDialog}>
          Choose CSV file
        </button>
      </div>
    );
  }

  handleImportSettingsChange(event, propName, isCheckbox = false) {
    const { importSettings, columnNames } = this.state;

    const { target } = event;
    let value = isCheckbox ? target.checked : target.value;

    if (propName === 'name') {
      value = value.replace(/[^a-z\d]/ig, '');
    }

    const index = parseInt(target.getAttribute('data-index'), 10);
    const columnProp = `column-${index}`;
    const columnData = importSettings[columnProp];

    const newImportSettings = {
      ...importSettings,
      [columnProp]: {
        ...columnData,
        [propName]: value,
      },
    };

    const importErrors = this.checkRequiredValues(newImportSettings, columnNames, false);

    this.setState({
      importSettings: newImportSettings,
      importErrors,
    });
  }

  handleNameChange(event) {
    return this.handleImportSettingsChange(event, 'name');
  }

  handleImportChange(event) {
    return this.handleImportSettingsChange(event, 'import', true);
  }

  handleConvertToTermsChange(event) {
    return this.handleImportSettingsChange(event, 'toTerm', true);
  }

  handleTermNameChange(event) {
    return this.handleImportSettingsChange(event, 'termName');
  }

  handleTaxonomyNameChange(event) {
    return this.handleImportSettingsChange(event, 'taxonomyName');
  }

  handleTypeChange(event) {
    return this.handleImportSettingsChange(event, 'type');
  }

  handleTruncateChange(event) {
    return this.handleImportSettingsChange(event, 'truncate');
  }

  handleToFixedChange(event) {
    return this.handleImportSettingsChange(event, 'toFixed');
  }

  addTaxonomyAndTerm(taxonomyName, termName, column) {
    return new Promise((resolve, reject) => {
      if (!taxonomyName) {
        reject(`Missing taxonomy name for column "${column}"`);
        return;
      }

      let taxonomyPromise = this.taxonomies[taxonomyName];

      if (typeof taxonomyPromise === 'undefined') {
        taxonomyPromise = this.props.addNewTaxonomy(taxonomyName);
        this.taxonomies[taxonomyName] = taxonomyPromise;
      } else if (typeof taxonomyPromise === 'object') {
        taxonomyPromise = Promise.resolve(taxonomyPromise);
      }

      taxonomyPromise.then((taxonomy) => {
        this.props.addNewTerm(termName, taxonomy.$loki)
          .then(resolve)
          .catch(reject);
      });
    });
  }

  transformValue(value, columnData) {
    const { truncate, toFixed: precision, type } = columnData;

    if (typeof value !== type) {
      value = convertToType(value, type);
    }

    if (type === 'float' && precision > 0) {
      value = toFixed(value, precision);
    }

    if (type === 'string' && truncate > 0) {
      value = value.slice(0, truncate);
    }

    return value;
  }

  checkRequiredValues(importSettings, collectionName, updateState = true) {
    const { importSettings: stateSettings, collectionName: colName } = this.state;
    const importErrors = [];

    if (!importSettings) {
      importSettings = stateSettings;
      collectionName = colName;
    }

    if (!collectionName) {
      importErrors.push('Missing collection name');
    }

    Object.keys(importSettings).forEach((key) => {
      const { import: doImport, name, toTerm, termName, taxonomyName } = importSettings[key];

      if (!doImport) {
        return;
      }

      if (!name) {
        importErrors.push(`Column name missing for ${key} (${name})`);
      }

      if (toTerm) {
        if (!termName) {
          importErrors.push(`Term name missing for ${key} (${name})`);
        }

        if (!taxonomyName) {
          importErrors.push(`Taxonomy name missing for ${key} (${name})`);
        }
      }
    });

    if (updateState) {
      this.setState({
        importErrors,
        finishedImporting: false,
      });
    }

    return importErrors;
  }

  runCsvImport() {
    const { importSettings, csvFilePath, collectionName } = this.state;

    const importErrors =  this.checkRequiredValues();

    if (importErrors.length) {
      return;
    }

    this.setState({
      importing: true,
      finishedImporting: false,
    });

    const options = {
      mapHeaders: (args) => {
        const { index } = args;
        const columnData = importSettings[`column-${index}`];

        if(!columnData) {
          // eslint-disable-next-line no-console
          console.warn(args);
        }

        if (columnData.import) {
          return columnData.name;
        }

        return null;
      },
      mapValues: (args) => {
        const { index, value } = args;

        const columnData = importSettings[`column-${index}`];

        return this.transformValue(value, columnData);
      },
    };

    const input = this.readFile(csvFilePath);

    this.parseCsv(input, options)
      .then(this.importData)
      .then(() => {
        this.setState({
          importing: false,
          finishedImporting: true,
        });
        this.props.reloadCollections(collectionName);
      })
      .catch(alert);
  }

  handleDescriptionConversion(rows) {
    const { importSettings, columnNames } = this.state;
    const [firstRow] = rows;

    const hasDescription = [];

    columnNames.forEach((key, i) => {
      const columnData = importSettings[`column-${i}`];

      if (!columnData.import) {
        return;
      }

      if (columnData.type === 'description') {
        hasDescription.push(key);
      }
    });

    if (hasDescription.length === 0) {
      return rows;
    }

    const hasDescriptionsProp = typeof firstRow.descriptions !== 'undefined';

    if (hasDescriptionsProp) {
      throw new Error('Column named "descriptions" already exists - either rename it or do not use "Description" column type');
    }

    return rows.map((row) => {
      row.descriptions = hasDescription.map(key => row[key]).filter(Boolean);
      return row;
    });
  }

  handleLngLatConversion(rows) {
    const { importSettings, columnNames } = this.state;
    const [firstRow] = rows;

    let hasLat = false;
    let hasLng = false;

    columnNames.forEach((key, i) => {
      const columnData = importSettings[`column-${i}`];

      if (!columnData.import) {
        return;
      }

      if (columnData.type === 'lat') {
        hasLat = key;
      } else if (columnData.type === 'lng') {
        hasLng = key;
      }
    });

    if (!hasLat || !hasLng) {
      return rows;
    }

    const hasLatLng = typeof firstRow.latLng !== 'undefined';
    const hasLatLngShort = typeof firstRow.latLngShort !== 'undefined';

    if (hasLatLng) {
      throw new Error('Column named "latLng" already exists - either rename it or do not use latitude/longitude column type');
    }

    if (hasLatLngShort) {
      throw new Error('Column named "latLngShort" already exists - either rename it or do not use latitude/longitude column type');
    }

    return rows.map((row) => {
      const lat = row[hasLat];
      const lng = row[hasLng];
      const shortLat = toFixed(lat, 2);
      const shortLng = toFixed(lng, 2);

      row.latLng = { lat, lng };
      row.latLngShort = { lat: shortLat, lng: shortLng };

      return row;
    });
  }

  transformRows(rows) {
    return this.handleTermConversion(rows)
      .then(this.handleLngLatConversion)
      .then(this.handleDescriptionConversion);
  }

  async importData(rows) {
    const { collectionName } = this.state;

    const processed = await this.transformRows(rows);
    const collection = await this.props.addNewCollection(collectionName);

    collection.insert(processed);

    await this.props.reloadCollections(collectionName);
  }

  getColumnImportSettings(columnName) {
    const { importSettings } = this.state;

    const keys = Object.keys(importSettings);
    const len = keys.length;
    let column;
    let i = 0;

    for(i; i < len; i++) {
      column = importSettings[`column-${i}`];

      if (column.name === columnName) {
        break;
      }
    }

    return column;
  }

  handleTermConversion(rows) {
    const columns = Object.keys(rows[0]);
    const promises = [];

    rows.forEach((row) => {
      columns.forEach((column) => {
        const settings = this.getColumnImportSettings(column);
        const {
          toTerm,
          taxonomyName,
          termName: templateTermName,
        } = settings;

        const value = row[column];

        if (toTerm) {
          delete row[column];

          if (typeof row.termIds === 'undefined') {
            row.termIds = [];
          }

          const termName = templateTermName.replace('$VALUE', value);

          if (termName) {
            const prom = new Promise((resolve, reject) => {
              this.addTaxonomyAndTerm(taxonomyName, termName)
                .then((term) => resolve(row.termIds.push(term.$loki)))
                .catch(reject);
            });

            promises.push(prom);
          }
        }
      });
    });

    return Promise.all(promises)
      .then(() => rows);
  }

  renderColumn(columnName, index) {
    const { exampleValues, importSettings } = this.state;

    const slug = slugg(columnName);
    const value = exampleValues[columnName];
    const columnProp = `column-${index}`;
    const columnData = importSettings[columnProp];

    return (
      <tr key={slug} className={columnData.import ? '' : 'skip'}>
        <td align="center" style={{ paddingTop: '11px' }}>
          {index}
        </td>
        <td align="center">
          <input
            data-index={index}
            type="checkbox"
            name="import"
            style={{ width: '28px', height: '28px' }}
            checked={columnData.import}
            onChange={this.handleImportChange}
          />
        </td>
        <td>
          <input
            data-index={index}
            value={columnData.name}
            style={{ width: '100%' }}
            onChange={this.handleNameChange}
          />
        </td>
        <td>
          {value}
        </td>
        <td align="center">
          <select
            data-index={index}
            name="type"
            onChange={this.handleTypeChange}
            value={columnData.type}
          >
            <option value="string">Text</option>
            <option value="int">Integer</option>
            <option value="float">Decimal</option>
            <option value="boolean">True / False</option>
            <option value="lat">Latitude</option>
            <option value="lng">Longitude</option>
            <option value="description">Description</option>
          </select>
        </td>
        <td align="center">
          {columnData.type === 'string' && (
            <label style={{ whiteSpace: 'nowrap' }}>Truncate to <input
              data-index={index}
              type="number"
              name="truncate"
              style={{ width: '3.5em' }}
              value={columnData.truncate}
              onChange={this.handleTruncateChange}
            /> chars</label>
          )}
          {columnData.type === 'float' && (
            <label style={{ whiteSpace: 'nowrap' }}>Round to <input
              data-index={index}
              type="number"
              name="toFixed"
              style={{ width: '3.5em' }}
              value={columnData.toFixed}
              onChange={this.handleToFixedChange}
            /> decimals</label>
          )}
        </td>
        <td align="center">
          {columnData.type !== 'description' &&
           columnData.type !== 'lat' &&
           columnData.type !== 'lng' && (
            <input
              data-index={index}
              type="checkbox"
              name="toTerm"
              style={{ width: '28px', height: '28px' }}
              checked={columnData.toTerm}
              onChange={this.handleConvertToTermsChange}
            />
          )}
        </td>
        <td>
          {columnData.toTerm && (
            <input
              data-index={index}
              value={columnData.termName}
              style={{ width: '100%' }}
              onChange={this.handleTermNameChange}
            />
          )}
        </td>
        <td>
          {columnData.toTerm && (
            <input
              data-index={index}
              value={columnData.taxonomyName}
              style={{ width: '100%' }}
              onChange={this.handleTaxonomyNameChange}
            />
          )}
        </td>
      </tr>
    );
  }

  closeImportModal() {
    this.setState({
      columnNames: [],
      finishedImporting: false,
    });
  }

  handleimportAllToggle() {
    const { importAll, importSettings } = this.state;

    const newImportSettings = { ...importSettings };
    const columnNames = Object.keys(newImportSettings);

    columnNames.forEach(n => {
      newImportSettings[n].import = !importAll;
    });

    this.setState({
      importAll: !importAll,
      importSettings: newImportSettings,
    });
  }

  checkPresetCompatibility(settings) {
    const { importSettings } = this.state;

    const currentKeys = Object.keys(importSettings);
    const newKeys = Object.keys(settings);

    return currentKeys.length === newKeys.length;
  }

  handleLoadPreset(preset) {
    const { settings } = preset;

    if (this.checkPresetCompatibility(settings)) {
      this.setState({
        importSettings: _.cloneDeep(preset.settings),
        importAll: false,
      });
    } else {
      alert('Incompatible preset (column number differs)');
    }
  }

  renderErrors() {
    const { importErrors } = this.state;

    if (!importErrors.length) {
      return null;
    }

    return (
      <div className="errors">
        <h4>Errors</h4>
        {importErrors.map((err, i) => (
          <p key={`error-${i}`}>{err}</p>
        ))}
      </div>
    );
  }

  renderImportModal() {
    const {
      csvFilePath,
      columnNames,
      collectionName,
      importAll,
      importing,
      finishedImporting,
      displayImportPresetNamePrompt,
      saveImportPresetError,
    } = this.state;

    if (!columnNames.length) {
      return null;
    }

    return (
      <div className="csv-import-transform">
        <div className="container">
          <div className="data">
            <header className="flex">
              <div className="import-settings">
                <h4>
                  Import settings for file <span style={{ color: '#009688' }}>{csvFilePath}</span>
                </h4>
                <p>
                  Import into collection: <input
                    name="collectionName"
                    placeholder="Enter collection name"
                    style={{ width: '300px' }}
                    value={collectionName}
                    onChange={this.updateOptions}
                  />
                </p>
              </div>
              {this.renderErrors()}
            </header>
            <hr />
            <div className="flex data-main">
              {this.renderPresets()}
              <div className="data-table">
                <table>
                  <thead>
                    <tr>
                      <th style={{ width: '40px' }}>
                        <input
                          type="checkbox"
                          name="importAll"
                          style={{ width: '28px', height: '28px' }}
                          checked={importAll}
                          onChange={this.handleimportAllToggle}
                        />
                      </th>
                      <th style={{ width: '40px' }}>Import</th>
                      <th style={{ width: '300px' }}>Column name</th>
                      <th style={{ width: '250px' }}>Example value</th>
                      <th style={{ width: '100px' }}>Value type</th>
                      <th style={{ width: '200px' }}>Transform value</th>
                      <th style={{ width: '120px' }}>Convert to terms</th>
                      <th style={{ width: '200px' }}>Term name</th>
                      <th style={{ width: '200px' }}>Taxonomy name</th>
                    </tr>
                  </thead>
                  <tbody>
                    {columnNames.map(this.renderColumn)}
                  </tbody>
                </table>
                {importing && (
                  <div className="importing-indicator">
                    <h2>Importing...</h2>
                    <div className="donut-spinner"></div>
                  </div>
                )}
              </div>
            </div>
          </div>
          <div className="toolbar">
            <button
              className="button-primary"
              onClick={this.runCsvImport}
              disabled={importing}
            >
              Import data
            </button>
            {finishedImporting && (
              <span className="import-success">Successfully imported!</span>
            )}
            <div className="right-section">
              <button onClick={this.toggleImportPresetNamePrompt}>
                Save Import Settings as a preset
              </button>
              {displayImportPresetNamePrompt && (
                <Prompt
                  description="Import preset name"
                  onOk={this.handleSaveImportPresetOk}
                  onCancel={this.handleSaveImportPresetCancel}
                  onChange={this.handleSaveImportPresetChange}
                  error={saveImportPresetError}
                />
              )}
              <button onClick={this.closeImportModal}>
                Close
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }

  renderPreset(preset) {
    const presetId = preset.$loki;

    return (
      <li
        className="nolink"
        key={`preset-${presetId}`}
        onClick={() => this.handleLoadPreset(preset)}
      >
        {preset.title}
      </li>
    );
  }

  renderPresets() {
    const { presets } = this.props;

    if (!presets || !presets.length) {
      return null;
    }

    return (
      <div className="importer-presets">
        <h5>Presets</h5>
        <ul className="list">
          {presets.map(this.renderPreset)}
        </ul>
      </div>
    );
  }

  render() {
    return (
      <React.Fragment>
        {this.renderImportOptions()}
        {this.renderImportModal()}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  const projectReady = !!state.project.databaseFilePath;

  if (!db.loaded || !projectReady) {
    return {
      presets: [],
    };
  }

  return {
    presets: state.collections.presets.filter(p => p.component === 'csv-importer'),
  };
};

const mapDispatchToProps = (dispatch) => ({
  addNewTaxonomy: (taxonomyName) => {
    return dispatch(
      addNewTaxonomyThunk(taxonomyName)
    );
  },
  addNewTerm: (termName, taxonomyId) => dispatch(
    addNewTermThunk(termName, taxonomyId, false)
  ),
  addNewCollection: (newCollectionName) => {
    return dispatch(addNewCollectionThunk(newCollectionName));
  },
  reloadCollections: (newCollectionName) => {
    dispatch(reloadCollectionAction(TAXONOMIES));
    dispatch(reloadCollectionAction(TERMS));
    dispatch(reloadCollectionAction(newCollectionName));
  },
  saveImportPreset: (preset) => {
    dispatch(addNewDocumentThunk(preset));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(CsvImporter);
