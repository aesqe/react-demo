import React, { PureComponent } from 'react';

import './style.scss';

export default class Output extends PureComponent {
  render() {
    return (
      <div className="output">
        Nothing yet.
      </div>
    );
  }
}
