import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { ipcRenderer } from 'electron';

import {
  appUpdateCheckForUpdatesThunk,
  appUpdateValidateGithubTokenThunk,
  APP_UPDATE_INVALID_GITHUB_TOKEN,
  APP_UPDATE_VALID_GITHUB_TOKEN,
  APP_UPDATE_CHECKING_FOR_UPDATE,
  APP_UPDATE_DOWNLOAD_PROGRESS,
  APP_UPDATE_NOT_AVAILABLE,
  APP_UPDATE_DOWNLOADED,
  APP_UPDATE_AVAILABLE,
  APP_UPDATE_ERROR,
} from 'reducers';

class AppUpdatesProvider extends PureComponent {
  constructor() {
    super();

    this.autoupdaterHandler = this.autoupdaterHandler.bind(this);
    ipcRenderer.removeAllListeners('autoupdater');
  }

  componentDidMount() {
    ipcRenderer.on('autoupdater', this.autoupdaterHandler);
    this.props.initialCheckForUpdates();
  }

  convertMessageToActionType(message) {
    switch(message) {
      case 'checking-for-update':
        return APP_UPDATE_CHECKING_FOR_UPDATE;
      case 'update-available':
        return APP_UPDATE_AVAILABLE;
      case 'update-not-available':
        return APP_UPDATE_NOT_AVAILABLE;
      case 'error':
        return APP_UPDATE_ERROR;
      case 'download-progress':
        return APP_UPDATE_DOWNLOAD_PROGRESS;
      case 'update-downloaded':
        return APP_UPDATE_DOWNLOADED;
      case 'invalid-github-token':
        return APP_UPDATE_INVALID_GITHUB_TOKEN;
      case 'valid-github-token':
        return APP_UPDATE_VALID_GITHUB_TOKEN;
      default:
        return message;
    }
  }

  autoupdaterHandler(event, message, payload) {
    const type = this.convertMessageToActionType(message);
    this.props.updateState(type, payload);
  }

  render() {
    return (
      <React.Fragment>
        {this.props.children}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  appUpdate: state.appUpdate,
});

const mapDispatchToProps = (dispatch) => {
  return {
    initialCheckForUpdates: async () => {
      await dispatch(appUpdateValidateGithubTokenThunk('current'));
      dispatch(appUpdateCheckForUpdatesThunk());
    },
    updateState: (type, payload) => dispatch({ type, payload }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AppUpdatesProvider);
