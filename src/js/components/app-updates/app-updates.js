import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import {
  appUpdateCheckForUpdatesThunk,
  appUpdateDownloadUpdateThunk,
  appUpdateInstallUpdateThunk,
} from 'reducers';

class AppUpdates extends PureComponent {
  render() {
    const {
      appUpdate,
      checkForUpdates,
      downloadUpdate,
      installUpdate,
    } = this.props;
    const {
      invalidGithubToken,
      downloadProgress,
      updateAvailable,
      updateReady,
      downloading,
      checking,
      error,
      // info,
    } = appUpdate;
    const { percent = 0 } = downloadProgress;

    if (error) {
      // eslint-disable-next-line no-console
      console.log(error);

      return (
        <section id="app-updates">
          <h2>Updates</h2>
          {invalidGithubToken && (
            <p>Invalid Github token, <Link to="/">check your configuration.</Link></p>
          )}
          {!invalidGithubToken && (
            <p>Autoupdater error, check console output (⌥⌘I) for more info.</p>
          )}
          <p>
            <button onClick={checkForUpdates}>
              Check again
            </button>
          </p>
        </section>
      );
    }

    return (
      <section id="app-updates">
        <h2>Updates</h2>
        {checking && (
          <p>Checking for updates...</p>
        )}
        {updateAvailable && !downloading && (
          <React.Fragment>
            <p>An update is available.</p>
            <p>
              <button onClick={downloadUpdate}>
                Download the update
              </button>
            </p>
          </React.Fragment>
        )}
        {downloading && (
          <p>Downloading update: <span style={{color: '#0000ff'}}>
            {parseInt(percent, 10)}%
          </span></p>
        )}
        {updateAvailable && updateReady && (
          <React.Fragment>
            <p>An update has been downloaded.</p>
            <p>
              <button onClick={installUpdate}>
                Restart the app and install now
              </button>
            </p>
          </React.Fragment>
        )}
        {!checking && !updateAvailable && (
          <React.Fragment>
            <p>No updates available</p>
            <p>
              <button onClick={checkForUpdates}>
                Check again
              </button>
            </p>
          </React.Fragment>
        )}
      </section>
    );
  }
}

const mapStateToProps = (state) => ({
  appUpdate: state.appUpdate,
});

const mapDispatchToProps = (dispatch) => {
  return {
    checkForUpdates: () => dispatch(appUpdateCheckForUpdatesThunk()),
    downloadUpdate: () => dispatch(appUpdateDownloadUpdateThunk()),
    installUpdate: () => dispatch(appUpdateInstallUpdateThunk()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AppUpdates);
