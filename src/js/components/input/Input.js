import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import nanoid from 'nanoid';

class Input extends PureComponent {
  componentWillMount() {
    const { id, type = 'text', defaultValue, value } = this.props;

    this.id = id ? id : `input-${type}-${nanoid()}`;
    this.values = { value };

    if (defaultValue) {
      this.values = { defaultValue };
    }
  }

  render() {
    const {
      label,
      type,
      size,
      className,
      required,
      inputRef,
      onInput,
      onKeyDown,
      onChange,
      placeholder,
    } = this.props;

    const classes = classNames(className, 'form-group');
    const inputId = this.id;

    return (
      <span className={classes}>
        {label && (
          <label htmlFor={inputId}>
            {label}
          </label>
        )}
        <input
          className="form-control"
          type={type}
          id={inputId}
          placeholder={placeholder}
          onInput={onInput}
          onKeyDown={onKeyDown}
          onChange={onChange}
          required={required}
          ref={inputRef}
          size={size}
          {...this.values}
        />
      </span>
    );
  }
}

Input.propTypes = {
  id: PropTypes.string,
  label: PropTypes.string,
  type: PropTypes.string,
  className: PropTypes.string,
  required: PropTypes.bool,
  inputRef: PropTypes.object,
  onInput: PropTypes.func,
  onKeyDown: PropTypes.func,
  onChange: PropTypes.func,
};

Input.defaultProps = {
  id: '',
  label: '',
  type: 'text',
  size: 10,
  className: '',
  required: false,
  inputRef: null,
  onInput: () => {},
  onKeyDown: () => {},
  onChange: () => {},
};

export default Input;
