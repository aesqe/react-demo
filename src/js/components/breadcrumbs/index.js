import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { MdKeyboardArrowRight } from 'react-icons/md';

import './style.scss';

export default class Breadcrumbs extends Component {
  constructor(props) {
    super(props);

    this.renderItem = this.renderItem.bind(this);
    this.displayName = 'Breadcrumbs';
  }

  renderItem(item, index) {
    const parts = window.location.pathname.split('/').filter(Boolean);
    const path = parts.slice(0, index+1).join('/');
    const href = `/${path}`;

    return (
      <span key={item}>
        <MdKeyboardArrowRight />
        <Link to={href}>{item}</Link>
      </span>
    );
  }

  render() {
    const id = this.props.id || 'breadcrumbs';

    const parts = window.location.pathname.split('/').filter(Boolean);

    return (
      <div className="breadcrumbs" id={id}>
        <span>
          <Link to="/">home</Link>
        </span>
        {parts.map(this.renderItem)}
      </div>
    );
  }
}
