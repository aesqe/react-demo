import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { updateProjectAction } from 'reducers';
import ProjectSettingsMenu from 'components/project-settings-menu';
import config from 'modules/config';
import { getCurrentWindow } from 'modules/window';
import { platform, IS_OSX } from 'src/const';

import WindowControls from './window-controls';
import './style.scss';

const { productName } = config.get('packageJson');

const noop = () => {};
const currentWindow = getCurrentWindow();
const shouldDrawControls = (platform !== 'osx');

class AppToolbar extends PureComponent {
  constructor(props) {
    super(props);

    this.toggleRenameProject = this.toggleRenameProject.bind(this);
    this.onProjectNameChange = this.onProjectNameChange.bind(this);
    this.toggleMaximize = this.toggleMaximize.bind(this);
    this.renameProject = this.renameProject.bind(this);

    this.projectNameRef = React.createRef();

    this.state = {
      renamingProject: false,
      inputShown: false,
      projectName: '',
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.projectName !== this.props.projectName) {
      this.setState({
        projectName: this.props.projectName,
      });
    }

    const inputFirstShown = this.state.renamingProject && !prevState.renamingProject;

    if (inputFirstShown) {
      this.focusAndSelectProjectNameInput();
    }
  }

  toggleMaximize(event) {
    if (event && event.target && event.target.id === 'rename-project') {
      return;
    }

    if (currentWindow.isMaximized()) {
      currentWindow.unmaximize();
    } else {
      currentWindow.maximize();
    }
  }

  toggleRenameProject() {
    const { renamingProject } = this.state;

    this.setState({
      renamingProject: !renamingProject,
    });
  }

  onProjectNameChange(event) {
    const projectName = event.target.value;

    this.setState({
      projectName,
    });
  }

  revertProjectName() {
    this.setState({
      projectName: this.props.projectName,
    });
  }

  renameProject(event) {
    const projectName = this.state.projectName.trim();

    if (event.keyCode === 27 || event.keyCode === 13) {
      this.toggleRenameProject();
    }

    if (event.keyCode === 27) {
      this.revertProjectName();
    } else if (event.keyCode === 13) {
      if (projectName) {
        this.props.updateProjectData({
          projectName,
        });
      }
    }
  }

  focusAndSelectProjectNameInput() {
    this.projectNameRef.current.focus();
    this.projectNameRef.current.select();
  }

  renderProjectRenameInput() {
    return (
      <input
        type="text"
        ref={this.projectNameRef}
        value={this.state.projectName}
        onChange={this.onProjectNameChange}
        onKeyUp={this.renameProject}
        size={this.state.projectName.length}
      />
    );
  }

  renderProjectRename() {
    const { renamingProject } = this.state;
    const { projectName } = this.props;

    if (!projectName) {
      return null;
    }

    const projectRenameInput = renamingProject && this.renderProjectRenameInput();
    const toggleRenameProject = !renamingProject && this.toggleRenameProject || noop;

    return (
      <span
        id="rename-project"
        title="Double-click to rename project"
        onDoubleClick={toggleRenameProject}
      >
        <sup>topic</sup> {projectRenameInput}
        {!renamingProject && projectName}
      </span>
    );
  }

  render() {
    const { projectReady } = this.props;

    return (
      <div className="titlebar" onDoubleClick={this.toggleMaximize}>
        <div className="window-title">
          <span className="product-name">
            {productName}
          </span>
          {projectReady && !IS_OSX && <ProjectSettingsMenu />}
          {this.renderProjectRename()}
        </div>
        {projectReady && IS_OSX && <ProjectSettingsMenu />}
        {shouldDrawControls && <WindowControls />}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { project } = state;
  const { projectName, databaseFilePath } = project;

  const projectReady = !!databaseFilePath;

  return {
    projectName,
    projectReady,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateProjectData(data) {
      return dispatch(updateProjectAction(data));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AppToolbar);
