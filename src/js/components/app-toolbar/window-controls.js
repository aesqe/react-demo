import React, { PureComponent } from 'react';
import { ipcRenderer } from 'electron';
import classnames from 'classnames';

import { getCurrentWindow } from 'modules/window';

const currentWindow = getCurrentWindow();

export default class AppToolbar extends PureComponent {
  constructor(props) {
    super(props);

    this.hideWindow = this.hideWindow.bind(this);
    this.minimizeWindow = this.minimizeWindow.bind(this);
    this.toggleMaximizeWindow = this.toggleMaximizeWindow.bind(this);

    this.state = {
      isMaximized: currentWindow.isMaximized(),
    };

    this.setMax = () => this.setState({ isMaximized: true });
    this.unsetMax = () => this.setState({ isMaximized: false });

    currentWindow.on('maximize', this.setMax);
    currentWindow.on('unmaximize', this.unsetMax);
  }

  componentWillUnmount() {
    currentWindow.off('maximize', this.setMax);
    currentWindow.off('unmaximize', this.unsetMax);
  }

  hideWindow() {
    ipcRenderer.send('save-window-state');
    currentWindow.hide();
  }

  minimizeWindow() {
    currentWindow.minimize();
  }

  toggleMaximizeWindow() {
    const isMaximized = currentWindow.isMaximized();
    isMaximized ? currentWindow.unmaximize() : currentWindow.maximize();
  }

  render() {
    const { isMaximized } = this.state;

    const maximizeClasses = classnames('window-icon', {
      'window-unmaximize': isMaximized,
      'window-maximize': !isMaximized,
    });

    return (
      <div className="window-controls-container">
        <div className="window-icon-bg" onClick={this.minimizeWindow}>
          <div className="window-icon window-minimize"></div>
        </div>
        <div className="window-icon-bg" onClick={this.toggleMaximizeWindow}>
          <div className={maximizeClasses}></div>
        </div>
        <div className="window-icon-bg window-close-bg" onClick={this.hideWindow}>
          <div className="window-icon window-close"></div>
        </div>
      </div>
    );
  }
}
