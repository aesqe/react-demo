import Breadcrumbs from 'components/breadcrumbs';
import AppStatusbarUpdates from 'components/app-statusbar-updates';

export default [
  AppStatusbarUpdates,
  Breadcrumbs,
];
