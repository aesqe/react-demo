import React, { PureComponent } from 'react';
import slugg from 'slugg';

import { db } from 'lib/database';
import statusBarItems from './app-statusbar-items';

import './style.scss';

class AppStatusbar extends PureComponent {
  constructor(props) {
    super(props);

    this.logDb = this.logDb.bind(this);
  }

  logDb() {
    // eslint-disable-next-line no-console
    console.log(db);
  }

  renderItem(Item) {
    let { displayName } = Item;

    if (displayName && displayName.startsWith('Connect(')) {
      displayName = displayName.replace('Connect(', '').replace(')', '');
    }

    const key = slugg(`statusbar-${displayName}`);

    return (
      <Item key={key} />
    );
  }

  render() {
    return (
      <div className="statusbar">
        {statusBarItems.map(this.renderItem)}
        <span style={{ cursor: 'pointer'}} onClick={this.logDb}>LDB</span>
      </div>
    );
  }
}

export default AppStatusbar;
