import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { isNumeric, getElData } from 'lib/util';
import './style.scss';

export default class Pagination extends PureComponent {
  constructor(props) {
    super(props);

    this.handleInputPage = this.handleInputPage.bind(this);
    this.handleInputItemsPerPage = this.handleInputItemsPerPage.bind(this);
    this.goToPreviousPage = this.goToPreviousPage.bind(this);
    this.goToNextPage = this.goToNextPage.bind(this);
    this.goToPage = this.goToPage.bind(this);
  }

  handleInputPage(event) {
    const { value } = event.target;

    if (isNumeric(value)) {
      const page = parseInt(value, 10);
      this.props.onPageChange(page);
    }
  }

  goToPage(event) {
    const el = event.target;
    const value = getElData(el, 'pagenum', 'int');

    if (isNumeric(value)) {
      const page = parseInt(value, 10);
      this.props.onPageChange(page);
    }
  }

  handleInputItemsPerPage(event) {
    const { value } = event.target;

    if (isNumeric(value)) {
      const itemsPerPage = parseInt(value, 10);
      this.props.onItemsPerPageChange(itemsPerPage);
    }
  }

  isValidPageNum(pageNum) {
    const { currentPage, totalPages } = this.props;

    return pageNum > 0 && pageNum !== currentPage && pageNum <= totalPages;
  }

  goToPreviousPage() {
    const { currentPage } = this.props;

    if (currentPage === 1) {
      return;
    }

    this.props.onPageChange(currentPage - 1);
  }

  goToNextPage() {
    const { currentPage, totalPages } = this.props;

    if (currentPage === totalPages) {
      return;
    }

    this.props.onPageChange(currentPage + 1);
  }

  renderSmallSize() {
    const { totalPages, currentPage, itemsPerPage, className } = this.props;

    const classes = classnames('pagination small', className);

    return (
      <div className={classes}>
        <div className="previous">
          <button onClick={this.goToPreviousPage}>
            &lt;
          </button>
        </div>
        <div className="pages">
          <span className="current">
            <input
              type="number"
              value={currentPage}
              size={totalPages + 1}
              onChange={this.handleInputPage}
            />
          </span>
          <span className="sep">/</span>
          <span className="pages-total">{totalPages}</span>
          <span className="items-per-page">
            <label> x:</label>
            <input
              type="number"
              value={itemsPerPage}
              size={itemsPerPage + 1}
              onChange={this.handleInputItemsPerPage}
            />
          </span>
        </div>
        <div className="next">
          <button onClick={this.goToNextPage}>
            &gt;
          </button>
        </div>
      </div>
    );
  }

  renderNormalSize() {
    const { itemCount, totalPages, currentPage, itemsPerPage, className } = this.props;

    const classes = classnames('pagination normal', className);

    return (
      <div className={classes}>
        <div className="previous">
          <button onClick={this.goToPreviousPage}>
            Prev
          </button>
        </div>
        <div className="pages">
          <span className="current">
            <label>Page:</label>
            <input
              type="number"
              value={currentPage}
              size={totalPages + 1}
              onChange={this.handleInputPage}
            />
          </span>
          <span className="sep">/</span>
          <span className="pages-total">{ totalPages }</span>
          <span className="items-per-page">
            <label>Items per page: </label>
            <input
              type="number"
              value={itemsPerPage}
              size={itemsPerPage + 1}
              onChange={this.handleInputItemsPerPage}
            />
          </span>
          <span className="items-total">Items: {itemCount}</span>
        </div>
        <div className="next">
          <button onClick={this.goToNextPage}>
            Next
          </button>
        </div>
      </div>
    );
  }

  render() {
    const { size } = this.props;

    if (size === 'normal') {
      return this.renderNormalSize();
    }

    return this.renderSmallSize();
  }
}

Pagination.propTypes = {
  currentPage: PropTypes.number.isRequired,
  itemCount: PropTypes.number.isRequired,
  itemsPerPage: PropTypes.number.isRequired,
  totalPages: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  onItemsPerPageChange: PropTypes.func.isRequired,
  size: PropTypes.string,
};

Pagination.defaultProps = {
  currentPage: 1,
  itemCount: 0,
  itemsPerPage: 10,
  totalPages: 1,
  size: 'normal',
  onPageChange: () => {},
  onItemsPerPageChange: () => {},
};
