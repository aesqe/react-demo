import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import fs from 'fs-extra';
import log from 'electron-log';
import smalltalk from 'smalltalk';
import { ipcRenderer } from 'electron';
import { boundMethod } from 'autobind-decorator';

import {
  updateProjectAction,
  loadProjectAndDatabaseThunk,
  createNewProjectWithDatabaseThunk,
} from 'reducers';
import config from 'modules/config';
import { getPreviousSessionProjectData } from 'modules/project';
import { openProjectFileDialog, showFolderDialog } from 'modules/dialogs';
import CsvImporter from 'components/csv-importer';
import './style.scss';

const { error: errorLog } = log;

class ProjectOptions extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      error: false,
      loading: false,
      canResume: false,
      loadedProject: false,
      projectUpdated: false,
      resumedProject: false,
      createdNewProject: false,
      previousSessionData: false,
    };
  }

  componentWillMount() {
    const previousSessionData = getPreviousSessionProjectData();
    const canResume = !!previousSessionData;
    const projectFilePath = config.get('projectFilePath');

    this.setState({
      previousSessionData,
      canResume,
      projectFilePath,
    });
  }

  @boundMethod
  resumeProject() {
    const projectFilePath = config.get('projectFilePath');
    const resumedState = {
      createdNewProject: false,
      resumedProject: true,
      loadedProject: false,
      loading: false,
      error: false,
    };

    this.setState({
      loading: true,
    }, () => {
      this.props.loadProject(projectFilePath)
        .then(() => this.setState(resumedState))
        .catch((err) => this.setState({
          ...resumedState,
          resumedProject: false,
          error: err.message,
        }));
    });
  }

  @boundMethod
  loadProject() {
    const loadedState = {
      createdNewProject: false,
      resumedProject: false,
      loadedProject: true,
      loading: false,
      error: false,
    };

    openProjectFileDialog()
      .then(this.props.loadProject)
      .then(() => this.setState(loadedState))
      .catch((err) => this.setState({
        ...loadedState,
        loadedProject: false,
        error: err.message,
      }));
  }

  @boundMethod
  createNewProject() {
    const { createNewProject } = this.props;

    const title = 'Project setup';
    const message = 'Please enter a project name';
    const defaultValue = 'New project';

    smalltalk.prompt(title, message, defaultValue)
      .then((projectName) => {
        if (!projectName || String(projectName).trim() === '') {
          projectName = defaultValue;
        }

        return createNewProject(projectName)
          .then(() => {
            this.setState({
              createdNewProject: true,
              resumedProject: false,
              loadedProject: false,
            });
          });
      })
      .catch(err => err && errorLog(err));
  }

  closeProject() {
    this.props.closeProject()
      .then(() => {
        this.setState({
          createdNewProject: false,
          resumedProject: false,
          loadedProject: false,
        });
      });
  }

  @boundMethod
  updateProject(data) {
    const { updateProjectData } = this.props;

    // eslint-disable-next-line no-unused-vars
    return new Promise((resolve, reject) => {
      this.setState({
        projectUpdated: false,
      }, () => resolve(updateProjectData(data)));
    });
  }

  @boundMethod
  chooseStreetViewImagesFolder() {
    showFolderDialog()
      .then((svImagesDir) => {
        this.updateProject({ svImagesDir });
      });
  }

  @boundMethod
  chooseGeoImagesFolder() {
    showFolderDialog()
      .then((geoImagesDir) => {
        this.updateProject({ geoImagesDir });
      });
  }

  @boundMethod
  chooseAdditionalImagesFolder() {
    showFolderDialog()
      .then((additionalImagesDir) => {
        this.updateProject({ additionalImagesDir });
      });
  }

  @boundMethod
  updateGithubToken() {
    const { productName } = config.get('packageJson');
    const githubToken = config.get('githubToken');

    const title = `${productName} setup`;
    const message = 'Enter your Github API key';
    const defaultValue = githubToken;

    smalltalk.prompt(title, message, defaultValue)
      .then((newToken) => {
        if (!newToken || String(newToken).trim() === '') {
          newToken = defaultValue;
        }

        config.set({ githubToken: newToken });
        ipcRenderer.send('autoupdater', 'update-github-token', newToken);
      })
      .catch(() => console.log('updateGithubToken:cancel'));
  }

  checkFolder(folderPath) {
    if (!folderPath) {
      return -1;
    }

    if (!fs.existsSync(folderPath)) {
      return -2;
    }

    return 1;
  }

  folderPathStatusMessage(folderPath) {
    const code = this.checkFolder(folderPath);

    let className = 'path-ok';
    let message = (
      <React.Fragment>
        currently set to <strong>{folderPath}</strong>
      </React.Fragment>
    );

    if (code === -1) {
      className = 'path-missing';
      message = (
        <React.Fragment>
          path is not currently set
        </React.Fragment>
      );
    } else if (code === -2) {
      className = 'path-not-exist';
      message = (
        <React.Fragment>
          currently set path does not exist: <strong>{folderPath}</strong>
        </React.Fragment>
      );
    }

    return (
      <span className={className}>
        {message}
      </span>
    );
  }

  projectReady() {
    const {
      createdNewProject,
      resumedProject,
      loadedProject,
    } = this.state;
    const { project } = this.props;
    const { databaseFilePath } = project;

    return (createdNewProject || resumedProject || loadedProject || !!databaseFilePath);
  }

  render() {
    const { project } = this.props;
    const {
      error,
      loading,
      canResume,
      projectFilePath,
      previousSessionData,
      projectUpdated,
    } = this.state;
    const { geoImagesDir, svImagesDir, additionalImagesDir } = project;

    const githubToken = config.get('githubToken');
    const projectReady = this.projectReady();

    if (loading) {
      return (
        <div className="project-options">
          <h2>Project options</h2>
          <div className="donut-spinner"></div>
        </div>
      );
    }

    return (
      <div className="project-options">
        <h2>Project options</h2>
        {error && (
          <div className="error">
            {error}
          </div>
        )}
        <div>
          {canResume && !projectReady && (
            <div className="field">
              <button onClick={this.resumeProject}>
                Resume from last time
              </button>&nbsp;
              (<em>
                &quot;{previousSessionData.projectName}&quot;
              </em> &mdash;&nbsp;
              <strong>
                {config.get('projectFilePath')}
              </strong>)
            </div>
          )}
          <div className="field">
            <button onClick={this.loadProject}>
              Load project
            </button>
          </div>
          <div className="field">
            <button onClick={this.createNewProject}>
              Create new project
            </button>
          </div>
          {projectReady && (
            <div>
              {projectFilePath}

              <h3>Folders</h3>
              <h4>GeoImages</h4>
              <button onClick={this.chooseGeoImagesFolder}>
                Choose Folder
              </button> <br /> ({this.folderPathStatusMessage(geoImagesDir)})

              <h4>StreetViewImages</h4>
              <button onClick={this.chooseStreetViewImagesFolder}>
                Choose Folder
              </button> <br /> ({this.folderPathStatusMessage(svImagesDir)})

              <h4>Additional images</h4>
              <button onClick={this.chooseAdditionalImagesFolder}>
                Choose Folder
              </button> <br /> ({this.folderPathStatusMessage(additionalImagesDir)})
            </div>
          )}
          <h3>Other options</h3>
          {projectReady && (
            <CsvImporter />
          )}
          <h4>Github token (for app updates)</h4>
          <button onClick={this.updateGithubToken}>
            {githubToken && 'Update Github token'}
            {!githubToken && 'Set Github token'}
          </button>
          {projectUpdated && (
            <p>Project updated.</p>
          )}
        </div>
      </div>
    );
  }
}

const getPropsFromState = createSelector(
  state => state.project,
  state => state.database,
  (project, database) => {
    return {
      database,
      project,
    };
  }
);

const mapStateToProps = (state) => {
  return getPropsFromState(state);
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadProject: (filePath) => dispatch(loadProjectAndDatabaseThunk(filePath)),
    createNewProject: (name) => dispatch(createNewProjectWithDatabaseThunk(name)),
    updateProjectData: (data) => dispatch(updateProjectAction(data)),
  };
};

ProjectOptions.propTypes = {
  database: PropTypes.object,
  project: PropTypes.object,
  loadProject: PropTypes.func,
  createNewProject: PropTypes.func,
};

export default connect(mapStateToProps, mapDispatchToProps)(ProjectOptions);
