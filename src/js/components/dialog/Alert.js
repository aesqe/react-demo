import React from 'react';
import PropTypes from 'prop-types';

import './style.scss';

export default function Alert(props) {
  const {
    description,
    onClose,
  } = props;

  return (
    <div className="dialog-container dialog-alert-container">
      <div className="dialog-backdrop"></div>

      <div className="dialog">

        <div className="dialog-toolbar">
          <button className="button-close" onClick={onClose}>
            <span className="text">Close</span>
          </button>
        </div>

        <div className="dialog-description">
          {description}
        </div>

      </div>
    </div>
  );
}

Alert.defaultProps = {
  description: 'Alert',
  onClose: () => {},
};

Alert.propTypes = {
  description: PropTypes.string,
  onClose: PropTypes.function,
};
