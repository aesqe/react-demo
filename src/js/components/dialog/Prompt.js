import React, { useState } from 'react';
import PropTypes from 'prop-types';

import './style.scss';

export default function Prompt(props) {
  const {
    description,
    onOk,
    onCancel,
    okLabel,
    cancelLabel,
    onChange,
    error,
  } = props;

  const [val, setVal] = useState('');

  const updateVal = (event) => {
    const { value } = event.target;

    onChange(value);
    setVal(value);
  };

  return (
    <div className="dialog-container dialog-prompt-container">
      <div className="dialog-backdrop"></div>

      <div className="dialog">

        <div className="dialog-toolbar">
          <button className="button-close" onClick={onCancel}>
            <span className="text">Close</span>
          </button>
        </div>

        <div className="dialog-description">
          <label>
            {description} <input
              onChange={updateVal}
              value={val}
            />
          </label>
          {error && (
            <div className="error">
              {error}
            </div>
          )}
        </div>

        <div className="dialog-footer">
          <menu>
            <button className="button-confirm button-primary" onClick={onOk}>
              {okLabel}
            </button>
            <button className="button-cancel" onClick={onCancel}>
              {cancelLabel}
            </button>
          </menu>
        </div>

      </div>
    </div>
  );
}

Prompt.defaultProps = {
  description: 'Confirm action',
  okLabel: 'OK',
  cancelLabel: 'Cancel',
  onOk: () => {},
  onCancel: () => {},
  onChange: () => {},
  error: '',
};

Prompt.propTypes = {
  description: PropTypes.string,
  okLabel: PropTypes.string,
  cancelLabel: PropTypes.string,
  onOk: PropTypes.func,
  onCancel: PropTypes.func,
  onChange: PropTypes.func,
};
