import React from 'react';
import PropTypes from 'prop-types';

import './style.scss';

export default function Confirm(props) {
  const {
    description,
    onConfirm,
    onDeny,
    confirmLabel,
    denyLabel,
  } = props;

  return (
    <div className="dialog-container dialog-confirm-container">
      <div className="dialog-backdrop"></div>

      <div className="dialog">

        <div className="dialog-toolbar">
          <button className="button-close" onClick={onDeny}>
            <span className="text">Close</span>
          </button>
        </div>

        <div className="dialog-description">
          {description}
        </div>

        <div className="dialog-footer">
          <menu>
            <button className="button-confirm button-primary" onClick={onConfirm}>
              {confirmLabel}
            </button>
            <button className="button-deny" onClick={onDeny}>
              {denyLabel}
            </button>
          </menu>
        </div>

      </div>
    </div>
  );
}

Confirm.defaultProps = {
  description: 'Confirm action',
  confirmLabel: 'Confirm',
  denyLabel: 'Deny',
  onConfirm: () => {},
  onDeny: () => {},
};

Confirm.propTypes = {
  description: PropTypes.string,
  confirmLabel: PropTypes.string,
  denyLabel: PropTypes.string,
  onConfirm: PropTypes.func,
  onDeny: PropTypes.func,
};
