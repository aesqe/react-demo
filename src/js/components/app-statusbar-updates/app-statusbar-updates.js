import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { FaCloudDownloadAlt } from 'react-icons/fa';
import classNames from 'classnames';

import './style.scss';

class AppStatusbarUpdates extends PureComponent {
  render() {
    const { updateAvailable } = this.props.appUpdate;

    const updateClasses = classNames('app-statusbar-updates', {
      'update-available': updateAvailable,
    });

    return (
      <span className={updateClasses}>
        <Link to="/updates">
          <FaCloudDownloadAlt />
        </Link>
      </span>
    );
  }
}

const mapStateToProps = (state) => ({
  appUpdate: state.appUpdate,
});

export default connect(mapStateToProps)(AppStatusbarUpdates);
