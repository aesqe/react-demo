import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { Redirect } from 'react-router-dom';
import SplitPane from 'react-split-pane';
import MarkerClusterer from '@google/markerclusterer';

import { reloadCollectionAction } from 'reducers';
import GoogleMapsWrapper from 'lib/google-maps';
import { getTermsByIds } from 'modules/taxonomies';
import {
  PointEditor,
  StreetViewPoint,
  getTermIdsFromPointImages,
} from 'components/streetview';
import { GoogleMap } from 'components/maps';

import './style.scss';

const markerClustererOptions = {
  gridSize: 50,
  maxZoom: 13,
  imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m',
};

const dummySequence = {$loki: 'all', connections: []};
const defaultIcon = 'https://raw.githubusercontent.com/Concept211/Google-Maps-Markers/master/images/marker_red.png';
const activeIcon = 'https://raw.githubusercontent.com/Concept211/Google-Maps-Markers/master/images/marker_orange.png';

class Heatmap extends PureComponent {
  constructor(props) {
    super(props);

    this.mapReady = this.mapReady.bind(this);
    this.handlePointDataChange = this.handlePointDataChange.bind(this);
    this.handleImageDataChange = this.handleImageDataChange.bind(this);
    this.handleImageClick = this.handleImageClick.bind(this);
    this.handleMarkerClick = this.handleMarkerClick.bind(this);
    this.nextPoint = this.nextPoint.bind(this);
    this.prevPoint = this.prevPoint.bind(this);

    this.state = {
      points: [],
      currentMarker: null,
      currentPoint: null,
      currentImageId: null,
      clustering: false,
    };
  }

  mapReady({ map }) {
    const { points } = this.props;

    const locations = points.map((point) => ([point.lng, point.lat]));
    const bounds = GoogleMapsWrapper.getBounds(locations);

    map.fitBounds(bounds);

    this.setState({ clustering: true });

    setTimeout(() => {
      const markers = this.createMarkers();
      new MarkerClusterer(map, markers, markerClustererOptions);
      this.setState({
        clustering: false,
        markers,
      });
    }, 500);
  }

  getPointById(pointId) {
    const { points } = this.props;
    return points.find(p => p.$loki === pointId);
  }

  getImageById(imageId) {
    const { streetviewimages } = this.props;
    return streetviewimages.find(i => i.$loki === imageId);
  }

  updateCurrentPoint(pointId) {
    return new Promise((resolve) => {
      this.setState({
        currentPointId: pointId,
        currentPoint: this.getPointById(pointId),
      }, resolve);
    });
  }

  updateCurrentImage(imageId) {
    this.setState({
      currentImageId: imageId,
      currentImage: this.getImageById(imageId),
    });
  }

  async handleMarkerClick(pointId, marker) {
    const { currentMarker } = this.state;

    await this.updateCurrentPoint(pointId);

    if (currentMarker) {
      currentMarker.setIcon(defaultIcon);
      currentMarker.setZIndex();
    }

    marker.setIcon(activeIcon);
    marker.setZIndex(window.google.maps.Marker.MAX_ZINDEX + 1);

    this.setState({
      currentMarker: marker,
    });
  }

  handleImageClick(pointId, imageId) {
    this.updateCurrentImage(imageId);
  }

  handlePointDataChange() {
    this.updateCurrentPoint(this.state.currentPointId);
  }

  handleImageDataChange() {
    this.updateCurrentImage(this.state.currentImageId);
  }

  createMarker(point) {
    const position = new window.google.maps.LatLng(point.lat, point.lng);
    const marker = new window.google.maps.Marker({
      position,
      icon: defaultIcon,
      pointId: point.$loki,
    });

    marker.addListener('click', () =>
      this.handleMarkerClick(point.$loki, marker)
    );

    return marker;
  }

  createMarkers() {
    const { points, images } = this.props;

    return points
      // .slice(0, 25)
      .reduce((res, point) => {
        const termIds = getTermIdsFromPointImages(point, images);
        const terms = getTermsByIds(termIds);
        const markers = terms.map(() => this.createMarker(point));

        return res.concat(markers);
      }, []);
  }

  prevPoint() {
    const { currentPoint, markers } = this.state;
    const { points } = this.props;

    const currentIndex = points.findIndex(
      p => p.$loki === currentPoint.$loki
    );

    if (currentIndex === 0) {
      return;
    }

    const nextPoint = points[currentIndex - 1];
    const pointId = nextPoint.$loki;
    const nextMarker = markers.find(m => m.pointId === pointId);

    this.handleMarkerClick(pointId, nextMarker);
  }

  nextPoint() {
    const { currentPoint, markers } = this.state;
    const { points } = this.props;

    const currentIndex = points.findIndex(
      p => p.$loki === currentPoint.$loki
    );

    if (currentIndex + 1 === points.length) {
      return;
    }

    const nextPoint = points[currentIndex + 1];
    const pointId = nextPoint.$loki;
    const nextMarker = markers.find(m => m.pointId === pointId);

    this.handleMarkerClick(pointId, nextMarker);
  }

  renderPoint() {
    const { currentPoint, currentImageId } = this.state;

    if (!currentPoint) { return ( <div>no point</div> ); }

    return (
      <div>
        <nav className="flex" style={{
          justifyContent: 'space-between',
          backgroundColor: '#eee',
          padding: '3px 5px',
        }}>
          <button onClick={this.prevPoint}>
            Prev
          </button>
          <button onClick={this.nextPoint}>
            Next
          </button>
        </nav>
        <StreetViewPoint
          point={currentPoint}
          currentImageId={currentImageId}
          onClick={this.handleImageClick}
          selected={true}
        />
      </div>
    );
  }

  renderPointEditor() {
    const { currentPoint, currentImage } = this.state;

    if (!currentPoint) { return ( <div>no point</div> ); }

    return (
      <PointEditor
        currentPoint={currentPoint}
        currentImage={currentImage}
        sequence={dummySequence}
        onImageDataChange={this.handleImageDataChange}
        onPointDataChange={this.handlePointDataChange}
      />
    );
  }

  render() {
    const { clustering } = this.state;
    const { database, projectReady } = this.props;

    if (!projectReady || !database.loaded) {
      return (
        <Redirect to="/" />
      );
    }

    return (
      <section className="heatmap flex flex-column">
        <SplitPane size={600}>
          <SplitPane size={300}>
            {this.renderPoint()}
            {this.renderPointEditor()}
          </SplitPane>
          <GoogleMap
            id="streetview-directions"
            onReady={this.mapReady}
          />
        </SplitPane>
        {clustering && (
          <div className="importing-indicator">
            <h2>Creating markers...</h2>
            <div className="donut-spinner"></div>
          </div>
        )}
      </section>
    );
  }
}

const getPropsFromState = createSelector(
  state => state.database,
  state => state.project,
  state => state.collections.sequences,
  state => state.collections.points,
  state => state.collections.streetviewimages,
  (database, project, sequences, allPoints, streetviewimages) => {
    const projectReady = !!project.databaseFilePath;

    if (!projectReady || !database.loaded) {
      return {
        database,
        project,
        projectReady,
        points: [],
      };
    }

    const pointIdsInSequences = sequences.reduce((arr, s) => arr.concat(s.points), []);
    const points = allPoints.filter((point) => pointIdsInSequences.includes(point.$loki));
    const pointIds = points.map(point => point.$loki);
    const images = streetviewimages.filter((image) => pointIds.includes(image.pointId));

    return {
      points,
      pointIds,
      images,
      database,
      sequences,
      streetviewimages,
      projectReady,
    };
  }
);

const mapStateToProps = (state) => {
  return getPropsFromState(state);
};

const mapDispatchToProps = (dispatch) => ({
  reloadCollection: (name) => dispatch(
    reloadCollectionAction(name)
  ),
});

export default connect(mapStateToProps, mapDispatchToProps)(Heatmap);
