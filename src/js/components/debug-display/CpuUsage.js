import React, { PureComponent } from 'react';

export default class CpuUsage extends PureComponent {
  constructor(props) {
    super(props);

    this.frame = this.frame.bind(this);

    this.state = {
      cpuUsage: 0,
    };
  }

  componentDidMount() {
    requestAnimationFrame(this.frame);
  }

  frame() {
    if (typeof process.getCPUUsage !== 'function') {
      return;
    }

    const cpuUsage = process.getCPUUsage().percentCPUUsage.toFixed(2);

    this.setState({
      cpuUsage,
    });

    setTimeout(() =>
      requestAnimationFrame(this.frame),
    500);
  }

  render() {
    const { cpuUsage } = this.state;

    return (
      <div className="cpu-usage">
        CPU: { cpuUsage }
      </div>
    );
  }
}
