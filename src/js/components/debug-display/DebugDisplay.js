import React, { PureComponent } from 'react';
import classNames from 'classnames';

import CpuUsage from './CpuUsage';
import './style.scss';

export default class DebugDisplay extends PureComponent {
  constructor(props) {
    super(props);

    this.toggleDisplay = this.toggleDisplay.bind(this);

    this.state = {
      open: false,
    };
  }

  toggleDisplay() {
    this.setState({
      open: !this.state.open,
    });
  }

  render() {
    const { open } = this.state;

    if (!process.env.DEBUG) {
      return null;
    }

    const classes = classNames('debug-display', {
      open,
    });

    return (
      <div className={classes}>
        <button onClick={this.toggleDisplay}>
          DD
        </button>
        <div className="drawer">
          <CpuUsage />
        </div>
      </div>
    );
  }
}

