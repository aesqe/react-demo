import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { hexToRgb } from 'lib/color';
import { getElData } from 'lib/util';
import Input from 'components/input';
import './style.scss';

class Terms extends PureComponent {
  constructor(props) {
    super(props);

    this.processInputText = this.processInputText.bind(this);
    this.handleInputText = this.handleInputText.bind(this);
    this.focusInput = this.focusInput.bind(this);
    this.renderTerm = this.renderTerm.bind(this);
    this.showSuggestions = this.showSuggestions.bind(this);
    this.hideSuggestions = this.hideSuggestions.bind(this);
    this.hideSuggestionsWithDelay = this.hideSuggestionsWithDelay.bind(this);
    this.renderSuggestion = this.renderSuggestion.bind(this);
    this.navigateSuggestions = this.navigateSuggestions.bind(this);
    this.handleInputCommands = this.handleInputCommands.bind(this);
    this.handleSelectSuggestion = this.handleSelectSuggestion.bind(this);
    this.handleTermRemove = this.handleTermRemove.bind(this);

    // this.processInputTextDebounced = _.debounce(this.processInputText, 250);
    this.processInputTextDebounced = this.processInputText;

    this.inputRef = React.createRef();

    this.clickedSuggestion = false;
    this.hideSuggestionsTimeoutId = -1;

    this.state = {
      typedText: '',
      activeSuggestionIndex: -1,
      displaySuggestions: false,
    };
  }

  componentDidMount() {
    this.bindInput();
  }

  componentWillUnmount() {
    clearTimeout(this.hideSuggestionsTimeoutId);
    this.unbindInput();
  }

  bindInput() {
    this.inputRef.current.addEventListener('click', this.showSuggestions);
    this.inputRef.current.addEventListener('keydown', this.showSuggestions);
    this.inputRef.current.addEventListener('focus', this.showSuggestions);
    this.inputRef.current.addEventListener('blur', this.hideSuggestionsWithDelay);
  }

  unbindInput() {
    this.inputRef.current.removeEventListener('click', this.showSuggestions);
    this.inputRef.current.removeEventListener('keydown', this.showSuggestions);
    this.inputRef.current.removeEventListener('focus', this.showSuggestions);
    this.inputRef.current.removeEventListener('blur', this.hideSuggestionsWithDelay);
  }

  hideSuggestionsWithDelay() {
    this.hideSuggestionsTimeoutId = setTimeout(this.hideSuggestions, 150);
  }

  hideSuggestions() {
    this.setState({
      displaySuggestions: false,
    });
  }

  showSuggestions() {
    clearTimeout(this.hideSuggestionsTimeoutId);

    this.setState({
      displaySuggestions: true,
    });
  }

  focusInput() {
    if (!this.inputRef.current) {
      return;
    }

    this.resetSuggestionIndex();
    this.inputRef.current.focus();
  }

  clearTypedText() {
    if (!this.inputRef.current) {
      return;
    }

    this.setState({
      typedText: '',
    });

    this.resetSuggestionIndex();
    this.inputRef.current.value = '';
  }

  previousSuggestion() {
    const { suggestions } = this.props;
    const { activeSuggestionIndex } = this.state;

    if (!suggestions.length) {
      return;
    }

    let index = activeSuggestionIndex - 1;

    if( index === -1 ) {
      index = suggestions.length - 1;
    }

    this.setState({
      activeSuggestionIndex: index,
    });

    this.inputRef.current.value = suggestions[index].name;
  }

  nextSuggestion() {
    const { suggestions } = this.props;
    const { activeSuggestionIndex } = this.state;

    if (!suggestions.length) {
      return;
    }

    let index = activeSuggestionIndex + 1;

    if( index === suggestions.length ) {
      index = 0;
    }

    this.setState({
      activeSuggestionIndex: index,
    });

    this.inputRef.current.value = suggestions[index].name;
  }

  resetSuggestionIndex() {
    this.setState({
      activeSuggestionIndex: -1,
    });
  }

  navigateSuggestions(event) {
    if (event.keyCode === 38) {
      this.previousSuggestion();
    }

    if (event.keyCode === 40) {
      this.nextSuggestion();
    }
  }

  getSuggestionById(id) {
    const { suggestions } = this.props;

    return suggestions.find((s) => s.$loki === id);
  }

  handleSelectSuggestion(event) {
    const el = event.target;
    const suggestionId = getElData(el, 'suggestionid', 'int');
    const term = this.getSuggestionById(suggestionId);

    this.clearTypedText();

    return this.props.onTermSelect(term);
  }

  handleTermRemove(event) {
    const el = event.target;
    const term = getElData(el, 'term', 'json');

    return this.props.onTermRemove(term);
  }

  handleUndo() {
    this.props.onUndo();
  }

  processInputText(event) {
    const typedText = event.target.value;

    this.setState({
      typedText,
    });

    this.props.onInput(typedText);
  }

  handleInputText(event) {
    event.persist();

    return this.processInputTextDebounced(event);
  }

  handleInputCommands(event) {
    // enter
    if (event.keyCode === 13) {
      this.handleEnterKey();
    }

    // esc
    if (event.keyCode === 27) {
      this.hideSuggestions();
    }

    // ctrl/cmd + z
    if (event.keyCode === 90 && (event.ctrlKey || event.metaKey)) {
      this.handleUndo();
    }
  }

  handleEnterKey() {
    const { suggestions, onTermSelect, onTermAdd } = this.props;
    const { activeSuggestionIndex, typedText } = this.state;

    this.clearTypedText();

    const trimmedInput = typedText.trim();
    const nothingTyped = !trimmedInput;
    const suggestionSelected = activeSuggestionIndex > -1;

    if (nothingTyped && suggestionSelected) {
      const suggestion = suggestions[activeSuggestionIndex];

      if (suggestion) {
        onTermSelect(suggestion);
        this.resetSuggestionIndex();
      }
    } else {
      onTermAdd(trimmedInput);
      this.resetSuggestionIndex();
    }
  }

  renderSuggestion(suggestion, i) {
    const { activeSuggestionIndex } = this.state;
    const { $loki: suggestionId, name } = suggestion;

    const uniqueId = `suggestion-${suggestionId}`;
    const classes = classNames({
      selected: activeSuggestionIndex === i,
    });

    return (
      <li
        key={uniqueId}
        className={classes}
        data-suggestionid={suggestionId}
        onClick={this.handleSelectSuggestion}
      >
        {name}
      </li>
    );
  }

  renderSuggestions() {
    const { suggestions } = this.props;
    const hasSuggestions = suggestions.length > 0;

    return (
      <ul className="suggestions">
        {hasSuggestions && suggestions.map(this.renderSuggestion)}
        {!hasSuggestions && (
          <li className="empty">(No results found)</li>
        )}
      </ul>
    );
  }

  renderTerm(term) {
    const { $loki: termId, name, color } = term;

    const uniqueId = `term-${termId}`;
    const jsonTerm = JSON.stringify(term);
    const termColor = hexToRgb(color, 0.1);
    const termStyle = {
      backgroundColor: termColor,
    };

    return (
      <span
        className="term"
        key={uniqueId}
        style={termStyle}
      >
        {name}
        <span
          className="remove"
          data-term={jsonTerm}
          onClick={this.handleTermRemove}>
          x
        </span>
      </span>
    );
  }

  calculateInputSize() {
    if (this.inputRef.current) {
      return this.inputRef.current.value.length || 1;
    }

    return 1;
  }

  renderTerms() {
    const { terms } = this.props;

    return terms.map(this.renderTerm);
  }

  render() {
    const { displaySuggestions } = this.state;

    return (
      <div
        className="terms-container"
        onClick={this.focusInput}
        onKeyDown={this.navigateSuggestions}
      >
        <div className="terms-inner-container">
          {this.renderTerms()}

          <Input
            {...this.props}
            onInput={this.handleInputText}
            onKeyDown={this.handleInputCommands}
            inputRef={this.inputRef}
            size={this.calculateInputSize()}
          />
        </div>

        {displaySuggestions && this.renderSuggestions()}
      </div>
    );
  }
}

Terms.propTypes = {
  terms: PropTypes.array.isRequired,
  suggestions: PropTypes.array.isRequired,
  onInput: PropTypes.func.isRequired,
  onTermAdd: PropTypes.func.isRequired,
  onTermRemove: PropTypes.func.isRequired,
  onTermSelect: PropTypes.func.isRequired,
  onUndo: PropTypes.func.isRequired,
};

Terms.defaultProps = {
  terms: [],
  suggestions: [],
};

export default Terms;
