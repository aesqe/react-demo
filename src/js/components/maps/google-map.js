import React, { PureComponent } from 'react';
import log from 'electron-log';

import GoogleMapsWrapper, { injectGoogleMapsApiScript } from 'lib/google-maps';
import { areValidCoordinates } from 'lib/util';
import { googleApiKey } from 'components/maps';
import './style.scss';

const { info: infoLog, error: errorLog } = log;

export default class GoogleMap extends PureComponent {
  constructor(props) {
    super(props);

    this.mapContainer = React.createRef();

    this.googleMapsApiLoaded = this.googleMapsApiLoaded.bind(this);
  }

  componentDidMount() {
    injectGoogleMapsApiScript(googleApiKey)
      .then(this.googleMapsApiLoaded);
  }

  componentDidUpdate(prevProps) {
    const {
      overlays,
      traveler,
      location,
      travelerPosition,
      origin,
      destination,
      directions,
      geoJSON,
      route,
    } = this.props;

    const {
      origin: prevOrigin,
      destination: prevDestination,
      travelerPosition: prevTravelerPosition,
      location: prevLocation,
      route: prevRoute,
    } = prevProps;

    if (geoJSON) {
      this.updateGeoJSONData();
      return;
    }

    if (directions && (!areValidCoordinates(origin) || !areValidCoordinates(destination))) {
      infoLog('GoogleMap: invalid directions', this.props.directions);
      return;
    }

    if (traveler && !areValidCoordinates(travelerPosition)) {
      infoLog('GoogleMap: invalid traveler position', this.props.travelerPosition);
      return;
    }

    if (!areValidCoordinates(location)) {
      infoLog('GoogleMap: invalid location', this.props.location);
      return;
    }

    const directionsChanged = directions && (
      prevRoute !== route,
      prevOrigin.lat !== origin.lat ||
      prevOrigin.lng !== origin.lng ||
      prevDestination.lat !== destination.lat ||
      prevDestination.lng !== destination.lng
    );

    const travelerChanged = traveler && (
      prevTravelerPosition.lat !== travelerPosition.lat ||
      prevTravelerPosition.lng !== travelerPosition.lng
    );

    const locationChanged = location.lat && location.lng && (
      prevLocation.lat !== location.lat ||
      prevLocation.lng !== location.lng
    );

    if (this.mapper) {
      if (directionsChanged) {
        try {
          if (route && route.length) {
            this.mapper.directionsRenderer.setMap(null);
            this.mapper.clearPolyLines();
            this.mapper.drawPolyline({ path: route });
          } else {
            this.mapper.getDirections({
              origin,
              destination,
            });
          }

          const bounds = new window.google.maps.LatLngBounds();

          bounds.extend(origin);
          bounds.extend(destination);

          this.mapper.map.fitBounds(bounds);
        } catch (err) {
          errorLog(err);
        }
      }

      if (prevProps.overlays !== overlays) {
        this.mapper.clearOverlays();

        if( overlays.length ) {
          const bounds = new window.google.maps.LatLngBounds();

          overlays.forEach((o) => bounds.union(o.bounds));

          this.mapper.addOverlays(overlays);

          this.mapper.map.fitBounds(bounds);
          this.mapper.map.setZoom(this.mapper.map.getZoom() + 3);
          this.mapper.map.setCenter(travelerPosition);
        }
      }

      if (locationChanged) {
        this.mapper.map.setCenter(location);
      }

      if (travelerChanged) {
        this.updateTravelerPosition();
      }
    }
  }

  updateGeoJSONData() {
    const { geoJSON } = this.props;

    this.mapper.directionsRenderer.setMap(null);
    this.mapper.dataLayerClear();
    this.mapper.dataLayerAddGeoJson(geoJSON);
    this.mapper.dataLayerSetStyle({
      strokeColor: '#FF0000',
      strokeWeight: 3,
    });
    this.mapper.dataLayerZoomAndCenter();
  }

  googleMapsApiLoaded() {
    const {
      styles,
      location,
      overlays,
      polylines,
      overlayOpacity,
      disableDefaultUI,
      traveler,
      travelerPosition,
      directions,
      onDirectionsChange,
      origin,
      destination,
      mapTypeId,
      onReady,
      geoJSON,
    } = this.props;

    const mapContainer = this.mapContainer.current;

    const mapper = new GoogleMapsWrapper({
      mapContainer,
      disableDefaultUI,
      mapTypeId: window.google.maps.MapTypeId[mapTypeId || 'HYBRID'],
      center: [location.lat, location.lng],
      apiKey: googleApiKey,
      zoom: 13,
      onDirectionsChange,
    });

    mapper.drawPolylines(polylines);
    mapper.addOverlays(overlays, {opacity: overlayOpacity});
    mapper.setStyles(styles);

    if (traveler) {
      const markerData = {
        position: areValidCoordinates(travelerPosition) && travelerPosition || location,
        icon: {
          path: window.google.maps.SymbolPath.CIRCLE,
          strokeColor: '#FF0000',
          scale: 10,
        },
      };

      if( mapper.overlays.length ) {
        markerData.position = mapper.overlays[0].bounds.getCenter();
      }

      mapper.traveler = mapper.addMarker(markerData);
      mapper.map.setCenter(mapper.traveler.position);
    }

    if (directions) {
      mapper.getDirections({
        origin,
        destination,
      });
    }

    this.mapper = mapper;

    if (geoJSON) {
      this.updateGeoJSONData();
    }

    if (typeof onReady === 'function') {
      onReady(this.mapper);
    }
  }

  updateTravelerPosition() {
    const {
      travelerPosition,
      recenterOnTravelerPositionChange,
    } = this.props;

    if (!this.mapper || !this.mapper.traveler) {
      return;
    }

    if (!areValidCoordinates(travelerPosition)) {
      return;
    }

    this.mapper.traveler.setPosition(travelerPosition);

    if (recenterOnTravelerPositionChange) {
      this.mapper.map.setCenter(travelerPosition);
    }

    const panorama = this.mapper.map.getStreetView();
    panorama.setPosition(travelerPosition);
  }

  render() {
    const { id, overlays } = this.props;

    const labels = overlays.map(o => o.imageId);

    return (
      <React.Fragment>
        {labels.join(',')}
        <div
          id={id}
          ref={this.mapContainer}
          className="google-map"
          style={{ height: '100%' }}
        ></div>
      </React.Fragment>
    );
  }
}

GoogleMap.defaultProps = {
  id: '',
  origin: { lat: 45.8002204, lng: 15.980564299999969 },
  destination: { lat: 45.8002204, lng: 15.980564299999969 },
  location: {lat: 45.8002204, lng: 15.980564299999969},
  overlayOpacity: 0.75,
  disableDefaultUI: false,
  recenterOnTravelerPositionChange: true,
  polylines: [],
  overlays: [],
  styles: {},
};
