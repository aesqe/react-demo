import { TERM_GROUPS } from 'data/defaults/default-collections.json';
import {
  addNewTermGroup,
  renameTermGroup,
  deleteTermGroup,
  mergeTermGroups,
  addTermToTermGroup,
  removeTermFromTermGroup,
} from './term-groups';
import {
  ADD_NEW_TERM_GROUP_REQUEST,
  ADD_NEW_TERM_GROUP_SUCCESS,
  RENAME_TERM_GROUP_REQUEST,
  RENAME_TERM_GROUP_SUCCESS,
  DELETE_TERM_GROUP_REQUEST,
  DELETE_TERM_GROUP_SUCCESS,
  MERGE_TERM_GROUPS_REQUEST,
  MERGE_TERM_GROUPS_SUCCESS,
  ADD_TERM_TO_TERM_GROUP_REQUEST,
  ADD_TERM_TO_TERM_GROUP_SUCCESS,
  REMOVE_TERM_FROM_TERM_GROUP_REQUEST,
  REMOVE_TERM_FROM_TERM_GROUP_SUCCESS,
} from './const';
import { reloadCollectionAction } from '../../reducers/actions';

export function addingNewTermGroupAction(termGroupName, taxonomyId) {
  return {
    type: ADD_NEW_TERM_GROUP_REQUEST,
    termGroupName,
    taxonomyId,
  };
}

export function addedNewTermGroupAction(termGroup) {
  return {
    type: ADD_NEW_TERM_GROUP_SUCCESS,
    termGroup,
  };
}

export function mergingTermGroupsAction(termGroupName, taxonomyId) {
  return {
    type: MERGE_TERM_GROUPS_REQUEST,
    termGroupName,
    taxonomyId,
  };
}

export function mergedTermGroupsAction(termGroup) {
  return {
    type: MERGE_TERM_GROUPS_SUCCESS,
    termGroup,
  };
}

export function renameTermGroupAction(termGroupId, newName) {
  return {
    type: RENAME_TERM_GROUP_REQUEST,
    termGroupId,
    newName,
  };
}

export function renamedTermGroupAction(termGroup) {
  return {
    type: RENAME_TERM_GROUP_SUCCESS,
    termGroup,
  };
}

export function deleteTermGroupAction(termGroupId, newName) {
  return {
    type: DELETE_TERM_GROUP_REQUEST,
    termGroupId,
    newName,
  };
}

export function deletedTermGroupAction(termGroup) {
  return {
    type: DELETE_TERM_GROUP_SUCCESS,
    termGroup,
  };
}

export function addingTermToTermGroupAction(termGroupId, termId) {
  return {
    type: ADD_TERM_TO_TERM_GROUP_REQUEST,
    termGroupId,
    termId,
  };
}

export function addedTermToTermGroupAction(termGroupId, termId) {
  return {
    type: ADD_TERM_TO_TERM_GROUP_SUCCESS,
    termGroupId,
    termId,
  };
}

export function removingTermFromTermGroupAction(termGroupId, termId) {
  return {
    type: REMOVE_TERM_FROM_TERM_GROUP_REQUEST,
    termGroupId,
    termId,
  };
}

export function removedTermFromTermGroupAction(termGroupId, termId) {
  return {
    type: REMOVE_TERM_FROM_TERM_GROUP_SUCCESS,
    termGroupId,
    termId,
  };
}


// thunks

export function addNewTermGroupThunk(termGroupName, taxonomyId) {
  return function(dispatch) {
    dispatch(addingNewTermGroupAction(termGroupName, taxonomyId));

    return addNewTermGroup(termGroupName, taxonomyId)
      .then((termGroup) => {
        dispatch(addedNewTermGroupAction(termGroup));
        dispatch(reloadCollectionAction(TERM_GROUPS));

        return termGroup;
      });
  };
}

export function mergeTermGroupsThunk(termGroupIds, mergedName) {
  return function(dispatch) {
    dispatch(mergingTermGroupsAction(termGroupIds, mergedName));

    return mergeTermGroups(termGroupIds, mergedName)
      .then((mergedTermGroup) => {
        dispatch(mergedTermGroupsAction(mergedTermGroup));
        dispatch(reloadCollectionAction(TERM_GROUPS));

        return mergedTermGroup;
      });
  };
}

export function renameTermGroupThunk(termGroupId, newName) {
  return function(dispatch) {
    dispatch(renameTermGroupAction(termGroupId, newName));

    return renameTermGroup(termGroupId, newName)
      .then((termGroup) => {
        dispatch(renamedTermGroupAction(termGroup));
        dispatch(reloadCollectionAction(TERM_GROUPS));

        return termGroup;
      });
  };
}

export function deleteTermGroupThunk(termGroup) {
  return function(dispatch) {
    dispatch(deleteTermGroupAction(termGroup));

    return deleteTermGroup(termGroup)
      .then((deletedTermGroup) => {
        dispatch(deletedTermGroupAction(deletedTermGroup));
        dispatch(reloadCollectionAction(TERM_GROUPS));

        return deletedTermGroup;
      });
  };
}



export function addTermToTermGroupThunk(termId, termGroupId) {
  return function(dispatch) {
    dispatch(addingTermToTermGroupAction(termId, termGroupId));

    return addTermToTermGroup(termId, termGroupId)
      .then((deletedTermGroup) => {
        dispatch(addedTermToTermGroupAction(termId, termGroupId));
        dispatch(reloadCollectionAction(TERM_GROUPS));

        return deletedTermGroup;
      });
  };
}

export function addTermToNewTermGroupThunk(termGroupName, term) {
  return function(dispatch) {
    const { $loki: termId, taxonomyId } = term;

    return dispatch(addNewTermGroupThunk(termGroupName, taxonomyId))
      .then((termGroup) => {
        // eslint-disable-next-line no-console
        console.log(termGroup);

        return dispatch(addTermToTermGroupThunk(termId, termGroup.$loki));
      });
  };
}

export function removeTermFromTermGroupThunk(termId, termGroupId) {
  return function(dispatch) {
    dispatch(removingTermFromTermGroupAction(termId, termGroupId));

    return removeTermFromTermGroup(termId, termGroupId)
      .then((deletedTermGroup) => {
        dispatch(removedTermFromTermGroupAction(termId, termGroupId));
        dispatch(reloadCollectionAction(TERM_GROUPS));

        return deletedTermGroup;
      });
  };
}
