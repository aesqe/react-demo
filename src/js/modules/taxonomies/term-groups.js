import slugg from 'slugg';

import { getCollection } from 'lib/database';
import { addNewDocument, updateDocument } from 'modules/documents';

import { TERMS, TERM_GROUPS } from 'data/defaults/default-collections.json';

import { getItemsForTerms, addNewTerm } from './terms';
import { getTaxonomyById } from './taxonomies';

// getters
export function getTermGroupsByIds(termGroupIds = []) {
  const collection = getCollection(TERM_GROUPS);

  return collection.find({
    $loki: { $in: termGroupIds },
  });
}

export function getTermGroupsForTerm(termId) {
  const collection = getCollection(TERM_GROUPS);

  return collection.find({
    termIds: { $contains: termId },
  });
}

export function getTermGroupsForTerms(termIds, includeAllTerms = false) {
  const collection = getCollection(TERM_GROUPS);
  let query = {
    termIds: { $containsAny: termIds },
  };

  if (termIds.length === 1) {
    query = {
      termIds: { $contains: termIds[0] },
    };
  } else if (includeAllTerms) {
    query = {
      $and: [],
    };

    termIds.forEach((id) => {
      query.$and.push({
        termIds: { $contains: id },
      });
    });
  }

  return collection.find(query);
}

export function getTermGroupsWithoutTerms(termIds) {
  const terms = getCollection(TERMS);
  const termGroups = getCollection(TERM_GROUPS);
  const singleTerm = terms.findOne({
    $loki: {
      $in: termIds,
    },
  });

  if (!singleTerm) {
    const jsonIds = JSON.stringify(termIds);
    throw new Error(`Invalid termIds: ${jsonIds}`);
  }

  return termGroups.find({
    taxonomyId: singleTerm.taxonomyId,
    termIds: {
      $containsNone: termIds,
    },
  });
}

export function getTermGroupBySlug(slug) {
  const collection = getCollection(TERM_GROUPS);

  return collection.findOne({
    slug: slugg(slug),
  });
}

export function getTermGroupById(id) {
  const collection = getCollection(TERM_GROUPS);

  return collection.get(id);
}

export function getItemsForTermGroup(termGroupId, itemsCollectionName) {
  const collection = getCollection(TERM_GROUPS);
  const termGroup = collection.get(termGroupId);

  if (!termGroup) {
    return [];
  }

  return getItemsForTerms(termGroup.termIds, itemsCollectionName);
}

export function getItemsForTermGroups(termGroupIds, itemsCollectionName) {
  const collection = getCollection(TERM_GROUPS);
  const termGroups = collection.find({
    $loki: { $in: termGroupIds },
  });

  if (!termGroups) {
    return [];
  }

  const termIds = termGroups.reduce(
    (arr, termGroup) => arr.concat(termGroup.termIds), []
  );

  return getItemsForTerms(termIds, itemsCollectionName);
}

export function getTermsForTermGroup(termGroupId) {
  const terms = getCollection(TERMS);
  const termGroups = getCollection(TERM_GROUPS);
  const termGroup = termGroups.get(termGroupId);

  if (!termGroup) {
    return [];
  }

  return terms.find({
    $loki: {
      $in: termGroup.termIds,
    },
  });
}

export function getTermsForTermGroups(termGroupIds = []) {
  const terms = getCollection(TERMS);

  if (termGroupIds.length === 0) {
    return [];
  }

  const collection = getCollection(TERM_GROUPS);
  const termGroups = collection.find({
    $loki: { $in: termGroupIds },
  });

  if (!termGroups) {
    return [];
  }

  const termIds = termGroups.reduce((result, termGroup) => (
    result.concat(termGroup.termIds)
  ), []);

  return terms.find({
    $loki: { $in: termIds },
  });
}

export function getItemTermsInTermGroup(
  itemId, termGroupId, itemsCollectionName
) {
  const items = getCollection(itemsCollectionName);
  const item = items.get(itemId);
  const termGroup = (typeof termGroupId === 'number') ?
    getTermGroupById(termGroupId) :
    getTermGroupBySlug(termGroupId.toLowerCase());

  if (!termGroup) {
    return [];
  }

  const groupTerms = termGroup.termIds;

  return item.termIds.filter(termId => groupTerms.indexOf(termId) > -1);
}



// adders
export function addNewTermGroup(name, _taxonomyId) {
  const collectionName = TERM_GROUPS;
  const compare = ['slug'];
  const titleField = 'name';
  const slug = slugg(name);
  let taxonomyId = _taxonomyId;

  if (typeof taxonomyId !== 'number') {
    taxonomyId = taxonomyId && taxonomyId.$loki;
  }

  if (!getTaxonomyById(taxonomyId)) {
    throw new Error(`Taxonomy with id ${taxonomyId} not found`);
  }

  const itemData = {
    name,
    slug,
    taxonomyId,
  };

  return addNewDocument({
    collectionName,
    itemData,
    titleField,
    compare,
  });
}

// updaters
export function updateTermTermGroupCount(termId) {
  const collection = getCollection(TERMS);

  if (!termId) {
    const terms = collection.find().map(t => t.$loki);
    return terms.forEach(tid => updateTermTermGroupCount(tid));
  }

  const termGroups = getTermGroupsForTerm(termId);
  const termGroupsCount = termGroups.length;
  const term = collection.get(termId);

  return updateDocument(term, { termGroupsCount });
}

export async function addTermToTermGroup(termId, termGroupId) {
  const termGroup = getTermGroupById(termGroupId);

  if(!termGroup.termIds.includes(termId)) {
    const termIds = [...termGroup.termIds, termId];
    await updateDocument(termGroup, { termIds });
    await updateTermTermGroupCount(termId);
  }

  return {
    termId,
    termGroupId,
  };
}

export async function addTermToTermGroups(termId, termGroupIds) {
  await termGroupIds.forEach(async (termGroupId) => {
    const termGroup = getTermGroupById(termGroupId);

    if(termGroup.termIds.includes(termId)) {
      return;
    }

    const termIds = [...termGroup.termIds, termId];
    await updateDocument(termGroup, { termIds });
  });

  await updateTermTermGroupCount(termId);

  return {
    termId,
    termGroupIds,
  };
}

export function addTermsToTermGroup(termIds, termGroupId) {
  return Promise.all(
    termIds.map((termId) => addTermToTermGroup(termId, termGroupId))
  );
}

export function addStringTermsToTermGroup(termNames, termGroupId, taxonomy) {
  const addTermPromises = termNames.map(name => addNewTerm(name, taxonomy));

  return Promise.all(addTermPromises)
    .then((termIds) => {
      const addToGroupPromises = termIds.map(
        termId => addTermToTermGroup(termId, termGroupId)
      );

      return Promise.all(addToGroupPromises);
    });
}

// modifiers
export function renameTermGroup(termGroupId, newName) {
  const termGroup = getTermGroupById(termGroupId);

  if (termGroup && termGroup.name !== newName) {
    const name = newName;
    const slug = slugg(newName);

    return updateDocument(termGroup, { name, slug });
  }

  return termGroup;
}

// deleters
export function removeTermFromTermGroup(termId, termGroupId) {
  const collection = getCollection(TERM_GROUPS);
  const termGroup = collection.get(termGroupId);
  const termIndex = termGroup.termIds.indexOf(termId);

  if (termIndex > -1) {
    const termIds = termGroup.termIds.filter((t) => t !== termId);

    updateTermTermGroupCount(termId);

    return updateDocument(termGroup, { termIds });
  }

  return termGroup;
}

export function removeTermFromAllTermGroups(termId) {
  const collection = getCollection(TERM_GROUPS);
  const termGroups = collection.find({
    termIds: { $contains: termId },
  });

  return termGroups.map((termGroup) => {
    const termGroupId = termGroup.$loki;
    removeTermFromTermGroup(termId, termGroupId);
    return termGroupId;
  });
}

export function removeTermsFromTermGroups(termIds, termGroupIds) {
  const collection = getCollection(TERM_GROUPS);
  const termGroups = collection.find({
    $loki: { $in: termGroupIds },
  });

  termGroups
    .forEach(termGroup => termIds
      .forEach(termId => removeTermFromTermGroup(termId, termGroup.$loki)));
}

export function deleteTermGroup(termGroup) {
  const collection = getCollection(TERM_GROUPS);

  collection.remove(termGroup);

  return Promise.resolve(termGroup);
}

export function deleteTermGroups(termGroups) {
  const collection = getCollection(TERM_GROUPS);

  collection.remove(termGroups);

  return termGroups;
}

export function filterTermsToTermGroup(termIds, termGroupId) {
  const termGroup = (typeof termGroupId === 'number') ?
    getTermGroupById(termGroupId) :
    getTermGroupBySlug(termGroupId);

  const termGroupTermIds = (termGroup && termGroup.termIds) || [];

  return termIds.filter(termId => termGroupTermIds.indexOf(termId) > -1);
}

// checkers
export function isTermInTermGroup(termId, termGroupId) {
  const termGroup = getTermGroupById(termGroupId);
  return termGroup && termGroup.termIds.indexOf(termId) > -1;
}

export function isTermInAnyTermGroup(termId) {
  const termGroups = getTermGroupsForTerm(termId);
  return termGroups.length > 0;
}

// merging
export function mergeTermGroups(mergeName, termGroupIds) {
  const collection = getCollection(TERM_GROUPS);
  const termGroups = collection.find({
    $loki: { $in: termGroupIds },
  });
  const termIds = termGroups.reduce((arr, termGroup) => (
    arr.concat(termGroup.termIds)
  ), []);

  collection.removeWhere({
    $loki: { $in: termGroupIds },
  });

  return addNewTermGroup(mergeName)
    .then((mergedTermGroupId) => {
      const termGroup = collection.get(mergedTermGroupId);

      updateDocument(termGroup, { termIds });

      return Promise.resolve(mergedTermGroupId);
    });
}
