import { TERMS, TERM_GROUPS } from 'data/defaults/default-collections.json';
import { TAGGABLE_COLLECTION_NAMES } from 'modules/collections';

import { reloadCollectionAction } from '../../reducers/actions';
import {
  addNewTerm,
  renameTerm,
  deleteTerm,
  addTermToItem,
  removeTermFromItem,
  mergeTerms,
  addTermsToItem,
  addNewTerms,
} from './terms';
import {
  ADD_TERM_TO_ITEM_REQUEST,
  ADD_TERM_TO_ITEM_SUCCESS,
  ADD_NEW_TERM_REQUEST,
  ADD_NEW_TERM_SUCCESS,
  REMOVE_TERM_FROM_ITEM_REQUEST,
  REMOVE_TERM_FROM_ITEM_SUCCESS,
  RENAME_TERM_REQUEST,
  RENAME_TERM_SUCCESS,
  DELETE_TERM_REQUEST,
  DELETE_TERM_SUCCESS,
  MERGE_TERMS_REQUEST,
  MERGE_TERMS_SUCCESS,
  ADDING_TERMS_TO_ITEM,
  ADDED_TERMS_TO_ITEM,
  ADDING_NEW_TERMS,
  ADDED_NEW_TERMS,
} from './const';

const debug = require('debug')('taxonomies:actions');

// actions

export function removingTermFromItemAction(termId, itemId) {
  return {
    type: REMOVE_TERM_FROM_ITEM_REQUEST,
    termId,
    itemId,
  };
}

export function removedTermFromItemAction(termId, item) {
  return {
    type: REMOVE_TERM_FROM_ITEM_SUCCESS,
    termId,
    item,
    collectionName: item.collection,
  };
}

export function addingTermToItemAction(term, item) {
  return {
    type: ADD_TERM_TO_ITEM_REQUEST,
    term,
    item,
  };
}

export function addedTermToItemAction(term, item) {
  return {
    type: ADD_TERM_TO_ITEM_SUCCESS,
    term,
    item,
    collectionName: item.collection,
  };
}

export function addingNewTermAction(termName, taxonomyId) {
  return {
    type: ADD_NEW_TERM_REQUEST,
    termName,
    taxonomyId,
  };
}

export function addedNewTermAction(term) {
  return {
    type: ADD_NEW_TERM_SUCCESS,
    term,
  };
}

export function mergingTermsAction(termName, taxonomyId) {
  return {
    type: MERGE_TERMS_REQUEST,
    termName,
    taxonomyId,
  };
}

export function mergedTermsAction(term) {
  return {
    type: MERGE_TERMS_SUCCESS,
    term,
  };
}

export function renameTermAction(termId, newName) {
  return {
    type: RENAME_TERM_REQUEST,
    termId,
    newName,
  };
}

export function renamedTermAction(term) {
  return {
    type: RENAME_TERM_SUCCESS,
    item: term,
    collectionName: TERMS,
  };
}

export function deleteTermAction(term) {
  return {
    type: DELETE_TERM_REQUEST,
    term,
  };
}

export function deletedTermAction(term) {
  return {
    type: DELETE_TERM_SUCCESS,
    term,
  };
}

export function addingTermsToItemAction(terms) {
  return {
    type: ADDING_TERMS_TO_ITEM,
    terms,
  };
}
export function addedTermsToItemAction(terms) {
  return {
    type: ADDED_TERMS_TO_ITEM,
    terms,
  };
}
export function addingNewTermsAction(terms) {
  return {
    type: ADDING_NEW_TERMS,
    terms,
  };
}
export function addedNewTermsAction(terms) {
  return {
    type: ADDED_NEW_TERMS,
    terms,
  };
}

// thunks

export function addNewTermThunk(termName, taxonomyId, reloadCollection = true) {
  return function(dispatch) {
    dispatch(addingNewTermAction(termName, taxonomyId));

    return addNewTerm(termName, taxonomyId)
      .then((term) => {
        dispatch(addedNewTermAction(term));

        if (reloadCollection) {
          dispatch(reloadCollectionAction(TERMS));
        }

        return term;
      });
  };
}

export function mergeTermsThunk(
  termIds, mergedName, collectionsToUpdateTermIdsIn, reloadCollections = true
) {
  return function(dispatch) {
    dispatch(mergingTermsAction(termIds, mergedName));

    return mergeTerms(termIds, mergedName, collectionsToUpdateTermIdsIn)
      .then((mergedTerm) => {
        dispatch(mergedTermsAction(mergedTerm));

        if (reloadCollections) {
          dispatch(reloadCollectionAction(TERMS));
          dispatch(reloadCollectionAction(TERM_GROUPS));

          collectionsToUpdateTermIdsIn.map((collectionName) =>
            dispatch(reloadCollectionAction(collectionName))
          );
        }

        return mergedTerm;
      });
  };
}

export function addNewTermWithTermGroupsThunk(
  termName, taxonomyId//, termGroupIds
) {
  return function(dispatch) {
    dispatch(addNewTermThunk(termName, taxonomyId))
      .then((term) => {
        debug('addNewTermAddToTermGroupsThunk', term);
        // addTermToTermGroups(term.$loki, termGroupIds);
      });
  };
}

export function renameTermThunk(termId, newName, reloadCollection = true) {
  return function(dispatch) {
    dispatch(renameTermAction(termId, newName));

    return renameTerm(termId, newName)
      .catch((term) => {
        return false;
      })
      .then((term) => {
        dispatch(renamedTermAction(term));

        if (reloadCollection) {
          dispatch(reloadCollectionAction(TERMS));
        }

        return term;
      });
  };
}

export function deleteTermThunk(term, reloadCollections = true) {
  return function(dispatch) {
    dispatch(deleteTermAction(term));

    return deleteTerm(term, TAGGABLE_COLLECTION_NAMES)
      .then((deletedTerm) => {
        dispatch(deletedTermAction(deletedTerm));

        if (reloadCollections) {
          dispatch(reloadCollectionAction(TERMS));
          dispatch(reloadCollectionAction(TERM_GROUPS));
        }

        return deletedTerm;
      });
  };
}

export function addTermToItemThunk(term, item, reloadCollections = true) {
  return async function(dispatch) {
    dispatch(addingTermToItemAction(term, item));

    const updatedItem = await addTermToItem(term.$loki, item.$loki, item.collection);
    await dispatch(addedTermToItemAction(term, updatedItem));

    if (reloadCollections) {
      await dispatch(reloadCollectionAction(updatedItem.collection));
      await dispatch(reloadCollectionAction(TERMS));
    }

    return [updatedItem, term];
  };
}

export function addTermsToItemThunk(terms, item, reloadCollections = true) {
  return async function(dispatch) {
    const termIds = terms.map(t => t.$loki);
    dispatch(addingTermsToItemAction(terms, item));
    const updatedItem = await addTermsToItem(termIds, item.$loki, item.collection);
    dispatch(addedTermsToItemAction(terms, updatedItem));

    if (reloadCollections) {
      dispatch(reloadCollectionAction(updatedItem.collection));
      dispatch(reloadCollectionAction(TERMS));
    }

    return updatedItem;
  };
}

export function addNewTermToItemThunk(termName, taxonomyId, item, reloadCollection = true) {
  return async function(dispatch) {
    const term = await dispatch(addNewTermThunk(termName, taxonomyId));
    return dispatch(addTermToItemThunk(term, item, reloadCollection));
  };
}

export function addNewTermsToItemThunk(termNames, taxonomyId, item, reloadCollection = true) {
  return async function(dispatch) {
    dispatch(addingNewTermsAction(termNames, taxonomyId));
    const terms = await addNewTerms(termNames, taxonomyId);
    await dispatch(addedNewTermsAction(terms));
    return dispatch(addTermsToItemThunk(terms, item, reloadCollection));
  };
}

export function removeTermFromItemThunk(termId, item, reloadCollection = true) {
  const { $loki: itemId, collection: collectionName } = item;

  return async function(dispatch) {
    dispatch(removingTermFromItemAction(termId, itemId));
    const updatedItem = await removeTermFromItem(termId, itemId, collectionName);
    dispatch(removedTermFromItemAction(termId, updatedItem));

    if (reloadCollection) {
      dispatch(reloadCollectionAction(updatedItem.collection));
    }
  };
}
