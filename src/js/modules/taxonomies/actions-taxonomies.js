import { TAXONOMIES } from 'data/defaults/default-collections.json';

import { reloadCollectionAction } from '../../reducers/actions';
import {
  addNewTaxonomy,
  renameTaxonomy,
  deleteTaxonomy,
} from './taxonomies';
import {
  ADD_NEW_TAXONOMY_REQUEST,
  ADD_NEW_TAXONOMY_SUCCESS,
  RENAME_TAXONOMY_REQUEST,
  RENAME_TAXONOMY_SUCCESS,
  DELETE_TAXONOMY_REQUEST,
  DELETE_TAXONOMY_SUCCESS,
} from './const';

export function addingNewTaxonomyAction(taxonomyName) {
  return {
    type: ADD_NEW_TAXONOMY_REQUEST,
    taxonomyName,
  };
}

export function addedNewTaxonomyAction(taxonomy) {
  return {
    type: ADD_NEW_TAXONOMY_SUCCESS,
    taxonomy,
  };
}

export function renameTaxonomyAction(taxonomyId, newName) {
  return {
    type: RENAME_TAXONOMY_REQUEST,
    taxonomyId,
    newName,
  };
}

export function renamedTaxonomyAction(taxonomy) {
  return {
    type: RENAME_TAXONOMY_SUCCESS,
    taxonomy,
  };
}

export function deleteTaxonomyAction(taxonomyId) {
  return {
    type: DELETE_TAXONOMY_REQUEST,
    taxonomyId,
  };
}

export function deletedTaxonomyAction(taxonomy) {
  return {
    type: DELETE_TAXONOMY_SUCCESS,
    taxonomy,
  };
}

export function addNewTaxonomyThunk(taxonomyName) {
  return function(dispatch) {
    dispatch(addingNewTaxonomyAction(taxonomyName));

    return addNewTaxonomy(taxonomyName)
      .then((taxonomy) => {
        dispatch(addedNewTaxonomyAction(taxonomy));

        return taxonomy;
      });
  };
}

export function renameTaxonomyThunk(taxonomyId, newName) {
  return function(dispatch) {
    dispatch(renameTaxonomyAction(taxonomyId, newName));

    return renameTaxonomy(taxonomyId, newName)
      .then((taxonomy) => {
        dispatch(renamedTaxonomyAction(taxonomy));
        dispatch(reloadCollectionAction(TAXONOMIES));

        return taxonomy;
      });
  };
}

export function deleteTaxonomyThunk(taxonomy) {
  return function(dispatch) {
    dispatch(deleteTaxonomyAction(taxonomy));

    return deleteTaxonomy(taxonomy)
      .then((deletedTaxonomy) => {
        dispatch(deletedTaxonomyAction(deletedTaxonomy));
        dispatch(reloadCollectionAction(TAXONOMIES));

        return deletedTaxonomy;
      });
  };
}
