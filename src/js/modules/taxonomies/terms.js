import slugg from 'slugg';
import _ from 'lodash';

import { getCollection } from 'lib/database';
import { addNewDocument, updateDocument } from 'modules/documents';

import { TERMS } from 'data/defaults/default-collections.json';

import { removeTermFromAllTermGroups, addTermToTermGroup } from './term-groups';
import { getTaxonomyById, getTaxonomyBySlug } from './taxonomies';

export function getTermBySlug(slug, taxonomyId) {
  const collection = getCollection(TERMS);

  return collection.findOne({
    slug,
    taxonomyId,
  });
}

export function getTermById(id = -1) {
  const collection = getCollection(TERMS);

  return collection.get(id);
}

export function getTermsByIds(termIds = []) {
  const collection = getCollection(TERMS);

  return collection.find({
    $loki: { $in: termIds },
  });
}

export function getItemsForTerm(termId = -1, itemsCollectionName) {
  const collection = getCollection(itemsCollectionName);

  return collection.find({
    termIds: { $contains: termId },
  });
}

export function getItemsForTerms(
  _termIds, itemsCollectionName, includeAllTerms = false
) {
  let termIds = _termIds;

  if (!Array.isArray(termIds)) {
    termIds = [termIds];
  }

  const collection = getCollection(itemsCollectionName);
  let query = {
    termIds: { $containsAny: termIds },
  };

  if (includeAllTerms) {
    query = {
      $and: [],
    };

    termIds.forEach((id) => {
      query.$and.push({
        termIds: { $contains: id },
      });
    });
  }

  return collection.find(query);
}

export function getTermsInTaxonomy(taxonomyId) {
  const terms = getCollection(TERMS);

  return terms.find({
    taxonomyId,
  });
}

export function addNewTerm(name, taxonomyIdSlugOrObj) {
  if (typeof name !== 'string') {
    throw new Error(`Invalid term type ${typeof name}`);
  }

  if (name.length === 0) {
    throw new Error(`Invalid term name "${name}"`);
  }

  const collectionName = TERMS;
  const slug = slugg(name);
  const titleField = 'name';
  const compare = ['slug', 'taxonomyId'];
  let taxonomyId = taxonomyIdSlugOrObj;

  if (typeof taxonomyId !== 'number') {
    if (typeof taxonomyId === 'string') {
      taxonomyId = getTaxonomyBySlug(taxonomyId);
    }

    taxonomyId = taxonomyId && taxonomyId.$loki;
  }

  if (!getTaxonomyById(taxonomyId)) {
    throw new Error(`Taxonomy with id ${taxonomyId} not found`);
  }

  const itemData = {
    name,
    slug,
    taxonomyId,
  };

  return addNewDocument({
    collectionName,
    itemData,
    titleField,
    compare,
  });
}

export function addNewTerms(names, taxonomyIdSlugOrObj) {
  if (!Array.isArray(names)) {
    if (typeof names !== 'string' || names === '') {
      throw new Error(`Wrong input type for ${names}`);
    }
  }

  const promises = [].concat(names).map((name) => addNewTerm(name, taxonomyIdSlugOrObj));
  return Promise.all(promises);
}

// updaters
export function updateTermItemCount(termId, itemCollectionName) {
  const collection = getCollection(TERMS);

  if (!termId) {
    const terms = collection.find().map(t => t.$loki);
    return terms.forEach(updateTermItemCount);
  }

  const collectionSingularName = itemCollectionName.slice(0, -1);
  const propName = `${collectionSingularName}Count`;
  const items = getItemsForTerm(termId, itemCollectionName);
  const term = collection.get(termId);

  return updateDocument(term, { [propName]: items.length });
}

export function addTermToItem(termId, itemId, collectionName) {
  const collection = getCollection(collectionName);
  const item = collection.get(itemId);
  const itemHasTerm = item && item.termIds.includes(termId);

  if (item && !itemHasTerm) {
    const termIds = _.uniq([...item.termIds, termId]);
    const updatedDoc = updateDocument(item, { termIds });

    updateTermItemCount(termId, collectionName);

    return updatedDoc;
  }

  return Promise.resolve(item);
}

export function addTermsToItem(termIds, itemId, collectionName) {
  const promises = termIds.map((termId) => addTermToItem(termId, itemId, collectionName));
  return Promise.all(promises);
}

// modifiers
export function renameTerm(termId, newName) {
  const term = getTermById(termId);

  if (term && term.name !== newName) {
    const name = newName;
    const slug = slugg(name);
    const existing = getTermBySlug(slug, term.taxonomyId);

    if (existing) {
      return Promise.reject(existing);
    }

    return updateDocument(term, { name, slug });
  }

  return Promise.resolve(term);
}

// deleters

export function removeTermFromItem(termId, itemId, itemsCollectionName) {
  const collection = getCollection(itemsCollectionName);
  const item = collection.get(itemId);
  const termIds = item.termIds.filter((t) => t !== termId);

  const updatedDoc = updateDocument(item, { termIds });

  updateTermItemCount(termId, itemsCollectionName);

  return updatedDoc;
}

export function removeTermFromItems(termId, itemsCollectionName) {
  const collection = getCollection(itemsCollectionName);

  const items = collection.find({
    termIds: { $contains: termId },
  });

  return items.map((item) => {
    const itemId = item.$loki;
    removeTermFromItem(termId, itemId, itemsCollectionName);
    return itemId;
  });
}

export async function deleteTerms(terms, itemsCollectionNames) {
  const collection = getCollection(TERMS);
  const termIds = terms.map(t => t.$loki);

  const termGroupPromises = termIds.map(removeTermFromAllTermGroups);

  const itemsPromises = [].concat(itemsCollectionNames)
    .reduce((arr, collectionName) => {
      const proms = termIds.map((termId) => {
        return removeTermFromItems(termId, collectionName);
      });

      return [
        ...arr,
        ...proms,
      ];
    }, []);

  await Promise.all([
    ...termGroupPromises,
    ...itemsPromises,
  ]);

  collection.remove(terms);

  return terms;
}

export async function deleteTerm(term, itemsCollectionNames) {
  const termId = term.$loki;
  const collection = getCollection(TERMS);

  await removeTermFromAllTermGroups(termId);

  const removePromises = [].concat(itemsCollectionNames).map(
    (itemsCollectionName) => removeTermFromItems(termId, itemsCollectionName));

  await Promise.all(removePromises);

  collection.remove(term);

  return Promise.resolve(term);
}

// checkers
export function isTermged(item) {
  return item.termIds && item.termIds.length > 0;
}

export function hasTerm(term = null, item) {
  if (item) {
    if (item.termIds) {
      if (!term) { // hasTerm() checks if any terms are present
        return item.termIds.length > 0;
      }

      if (item.termIds.indexOf(term) > -1) {
        return true;
      }
    }
  }

  return false;
}

export function isTermUsed(termId, itemsCollectionName) {
  return getItemsForTerm(termId, itemsCollectionName);
}

// merging

async function updateTermInItemsAfterTermsMerge(originalTermId, mergedTermId, collectionName) {
  // remove the original term from all items
  const itemIds = await removeTermFromItems(originalTermId, collectionName);

  // add the new, merged term to those same items
  const itemsPromises = itemIds.map((itemId) =>
    addTermToItem(mergedTermId, itemId, collectionName)
  );

  return Promise.all(itemsPromises);
}

async function updateTermAfterTermMerge(originalTermId, mergedTermId, itemsCollectionsNames) {
  // remove the term from all the termGroups
  const groupIds = await removeTermFromAllTermGroups(originalTermId);

  // add the new, merged term to those same groups
  const groupPromises = groupIds.map((groupId) =>
    addTermToTermGroup(mergedTermId, groupId)
  );

  // update all affected items in selected collections
  const itemsPromises = itemsCollectionsNames.map((collectionName) =>
    updateTermInItemsAfterTermsMerge(originalTermId, mergedTermId, collectionName)
  );

  return Promise.all(groupPromises.concat(itemsPromises));
}

export function updateMergedTerms(originalTermIds, mergedTermId, itemsCollectionsNames = ['items']) {
  const termsPromises = originalTermIds.map((originalTermId) =>
    updateTermAfterTermMerge(originalTermId, mergedTermId, itemsCollectionsNames)
  );

  return Promise.all(termsPromises);
}

// TODO: maybe add itemsCollectionsNames '___all___' variant which updates all collections
export async function mergeTerms(mergeName, termIds, itemsCollectionsNames) {
  if (!itemsCollectionsNames || itemsCollectionsNames.length === 0) {
    throw new Error('func mergeTerms() in terms.js: "itemsCollectionsNames" argument is missing');
  }

  const collection = getCollection(TERMS);

  // get the terms
  const terms = collection.find({ $loki: { $in: termIds } });

  // get the taxonomy id
  const [firstTerm] = terms;
  const { taxonomyId } = firstTerm;

  // add the new term
  const mergedTerm = await addNewTerm(mergeName, taxonomyId);
  const mergedTermId = mergedTerm.$loki;

  // update termGroups
  await updateMergedTerms(termIds, mergedTermId, itemsCollectionsNames);

  collection.remove(terms.filter(t => t.$loki !== mergedTermId));
  return mergedTerm;
}
