import slugg from 'slugg';

import { getCollection } from 'lib/database';
import { addNewDocument, updateDocument } from 'modules/documents';

import { TAXONOMIES } from 'data/defaults/default-collections.json';
import defaultTaxonomies from 'data/defaults/default-taxonomies.json';

// getters
export function getTaxonomyById(id) {
  const collection = getCollection(TAXONOMIES);

  return collection.get(id);
}

export function getTaxonomyBySlug(slug) {
  const collection = getCollection(TAXONOMIES);

  return collection.findOne({
    slug: slugg(slug),
  });
}

// adders
export function addNewTaxonomy(name, description = '') {
  const collectionName = TAXONOMIES;
  const titleField = 'name';
  const compare = ['slug'];
  const slug = slugg(name);
  const itemData = {
    name,
    slug,
    description,
  };

  return addNewDocument({
    collectionName,
    itemData,
    titleField,
    compare,
  });
}

export function addDefaultTaxonomies() {
  const promises = defaultTaxonomies.map((t) => {
    return addNewTaxonomy(t.name, t.description);
  });

  return Promise.all(promises);
}

export function addTaxonomy(taxonomyName) {
  if (taxonomyName.length) {
    return addNewTaxonomy(taxonomyName);
  }

  return Promise.reject('Missing taxonomy name');
}

export function renameTaxonomy(taxonomyId, newName) {
  const taxonomy = getTaxonomyById(taxonomyId);

  if (taxonomy && taxonomy.name !== newName) {
    const name = newName;
    const slug = slugg(newName);

    return updateDocument(taxonomy, { name, slug });
  }

  return taxonomy;
}

export function deleteTaxonomy(taxonomy) {
  const collection = getCollection(TAXONOMIES);

  collection.remove(taxonomy);

  return taxonomy;
}

export function deleteTaxonomyEvent(event) {
  const taxonomy = event.get();

  const collection = getCollection(TAXONOMIES);
  const confirmMessage = `Delete the ${taxonomy.name} taxonomy?`;
  const confirmDelete = confirm(confirmMessage);

  if (confirmDelete) {
    collection.remove(taxonomy);
  }

  return taxonomy;
}
