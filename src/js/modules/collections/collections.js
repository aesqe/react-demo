import { addCollection } from 'lib/database';
import defaultCollections from 'data/defaults/default-collections.json';

const defaultCollectionsNames = Object.values(defaultCollections);

export function addDefaultCollections() {
  const promises = defaultCollectionsNames.map(addCollection);

  return Promise.all(promises);
}
