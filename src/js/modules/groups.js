import _ from 'lodash';

import { db, getCollection } from 'lib/database';

import addNewDocument from './documents';

export function addNewGroup(title = 'New group') {
  return addNewDocument({
    collectionName: 'groups',
    itemData: {
      title,
    },
  });
}

export function deleteGroup(group) {
  const collection = getCollection('groups');

  return collection.remove(group);
}

export function getGroupById(groupId) {
  const collection = getCollection('groups');

  return collection.get(groupId);
}

export function getItemsForGroup(groupId) {
  const collection = getCollection('items');

  return collection.find({
    group: groupId,
  });
}

export function getTagsFromItems(items) {
  const tags = items.reduce((arr, item) => (arr.concat(item.tags)), []);

  return _.uniq(tags);
}

export function itemIsInGroup(itemId, groupId) {
  const collection = getCollection('items');
  const item = collection.findOne({
    $loki: itemId,
    group: groupId,
  });

  return !!item;
}

export function getImagesForItemsInGroup(groupId) {
  const images = db.getCollectionItems('images');

  if(!images.length) {
    return [];
  }

  const annotatedImages = images.filter(image =>
    image.annotations && image.annotations.length
  );

  return annotatedImages.filter((image) => {
    const annotationsInGroup = image.annotations.filter((annotation) => {
      const { relatedItemId } = annotation;
      const invalidRelatedItemId = relatedItemId === null || relatedItemId < 0;

      if (invalidRelatedItemId) {
        return false;
      }

      return itemIsInGroup(relatedItemId, groupId);
    });

    return annotationsInGroup.length > 0;
  });
}

export function getTagsForGroup(groupId)	{
  const items = getItemsForGroup(groupId);

  return getTagsFromItems(items);
}



export function getNumberOfArticlesInGroup(groupId) {
  const items = getCollection('items');
  const itemCount = items.count({
    group: groupId,
  });

  const articleCount = items.count({
    $and: [
      { group: groupId },
      { isSource: true },
    ],
  });

  return {
    itemCount,
    articleCount,
  };
}
