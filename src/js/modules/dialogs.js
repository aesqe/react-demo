import { remote } from 'electron';

import config from 'modules/config';

const { productName, fileExtension } = config.get('packageJson');

const { dialog } = remote;

export function showOpenDialog(options) {
  // eslint-disable-next-line no-unused-vars
  return new Promise((resolve, reject) =>
    dialog.showOpenDialog(options, (paths) => {
      if (!paths || !paths.length) {
        return;
      }

      resolve(paths.pop());
    })
  );
}

export function showOpenDialogMulti(options) {
  const opts = Object.assign(options, {
    properties: {
      multiSelections: true,
    },
  });

  // eslint-disable-next-line no-unused-vars
  return new Promise((resolve, reject) =>
    dialog.showOpenDialog(opts, resolve)
  );
}

export function showFolderDialog() {
  return showOpenDialog({
    buttonLabel: 'Select',
    properties: ['openDirectory'],
  });
}

export function openFileDialog() {
  return showOpenDialog({
    properties: ['openFile'],
  });
}

export function openFileDialogWithExtension(extDescription, extsStrArray, opts = {}) {
  const options = {
    properties: ['openFile'],
    filters: [{
      name: extDescription,
      extensions: extsStrArray,
    }],
    ...opts,
  };

  return showOpenDialog(options);
}

export function openCSVFileDialog() {
  return openFileDialogWithExtension('CSV file', ['csv']);
}

// loadDatabaseFile
export function openDatabaseFileDialog() {
  return openFileDialogWithExtension('JSON file', ['json']);
}

// loadProjectFile
export function openProjectFileDialog() {
  return openFileDialogWithExtension(`${productName} project files`, [fileExtension]);
}

export function saveFileDialog(options) {
  const {
    title,
    extension,
    extensionDescription,
  } = options;

  const dialogOptions = {
    defaultPath: title,
    filters: [{
      name: extensionDescription,
      extensions: [extension],
    }],
  };

  // eslint-disable-next-line no-unused-vars
  return new Promise((resolve, reject) =>
    dialog.showSaveDialog(dialogOptions, resolve)
  );
}
