import config from 'modules/config';

export function closeProject() {
  config.set('projectFilePath', '');
  config.set('databaseFilePath', '');
}
