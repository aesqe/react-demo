import fs from 'fs-extra';
import path from 'path';
import slugg from 'slugg';
import slash from 'slash';

import schemas from 'data/schemas';
import config from 'modules/config';
import { addDefaultTaxonomies } from 'modules/taxonomies';
import { addDefaultCollections } from 'modules/collections';
import { saveFileDialog } from 'modules/dialogs';
import {
  fileExistsAndIsReadable,
  getJsonFileContents,
} from 'lib/util';
import {
  loadDatabaseFromFile,
} from 'lib/database';

const { productName, fileExtension } = config.get('packageJson');

export function saveProjectData(data = false) {
  if (data.constructor !== Object) {
    throw new Error(`Invalid data format, needs to be an object: ${data}`);
  }

  const projectFilePath = config.get('projectFilePath');

  if (!projectFilePath) {
    throw new Error(`Invalid projectFilePath "${projectFilePath}" supplied`);
  }

  const defaultProjectData = schemas.projectData;
  const projectFileContents = fs.readJsonSync(projectFilePath, { throws: false });

  const projectData = Object.assign(
    {},
    (projectFileContents || defaultProjectData),
    data
  );

  // the file will be created if it doesn't exist already
  fs.writeJsonSync(
    projectFilePath,
    projectData,
  );

  // eslint-disable-next-line no-console
  console.log('Project file saved');

  return projectData;
}

export function createNewProject(projectName) {
  return new Promise((resolve, reject) => {
    if (!projectName) {
      reject('Missing project name');
      return;
    }

    const options = {
      title: slugg(projectName),
      extension: fileExtension,
      extensionDescription: `${productName} project files`,
    };

    saveFileDialog(options)
      .then((filePath) => {
        if (!filePath) {
          reject();
          return;
        }

        const projectFilePath = slash(filePath);

        config.set('projectFilePath', projectFilePath);

        const projectData = saveProjectData({
          projectName,
        });

        resolve(projectData);
      });
  });
}

export async function loadDatabaseWithDefaults(databaseFilePath, dispatch) {
  const slashedDbPath = slash(databaseFilePath);
  const db = await loadDatabaseFromFile(slashedDbPath, { isNew: true }, dispatch);

  await addDefaultCollections();
  await addDefaultTaxonomies();
  await db.save(true);

  return db;
}

export async function createNewDatabase(
  name = 'new-database',
  filePath = '',
  dispatch = () => {}
) {
  if (filePath) {
    return loadDatabaseWithDefaults(filePath, dispatch);
  }

  const saveDialogOptions = {
    title: slugg(name),
    extension: 'json',
    extensionDescription: 'JSON file',
  };

  const databaseFilePath = await saveFileDialog(saveDialogOptions);

  return loadDatabaseWithDefaults(databaseFilePath, dispatch);
}

export function getProjectFileContents(filePath) {
  const fileContents = getJsonFileContents(filePath);

  if (!fileContents) {
    return false;
  }

  config.set('projectFilePath', filePath);

  return fileContents;
}

export function canResumePreviousSession() {
  const projectFile = config.get('projectFilePath');

  return fileExistsAndIsReadable(projectFile);
}

export function getPreviousSessionProjectData() {
  if (canResumePreviousSession()) {
    const projectFile = config.get('projectFilePath');
    return getProjectFileContents(projectFile);
  }

  return false;
}

export function getDatabaseFilePath(projFilePath = false) {
  const projectFilePath = projFilePath || config.get('projectFilePath');

  if (!projectFilePath || !fs.existsSync(projectFilePath)) {
    throw new Error(`Invalid project file path: ${projectFilePath}`);
  }

  const projectDir = path.dirname(projectFilePath);
  const projectName = path.basename(projectFilePath, '.lmap');
  const databaseFileName = `${projectName}-database.json`;
  const databaseFilePath = path.join(projectDir, databaseFileName);

  if (!databaseFilePath || !fs.existsSync(databaseFilePath)) {
    throw new Error(`Invalid database file path: ${databaseFilePath}`);
  }

  return databaseFilePath;
}
