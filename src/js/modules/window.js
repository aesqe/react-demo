import { remote } from 'electron';

export function getCurrentWindow() {
  return remote.getCurrentWindow();
}
