import fs from 'fs-extra';

import { route } from 'data/json/route-14k.json';
import routePoints from 'data/json/route-points.json';
import aerialPhotosJson from 'data/json/aerialphotos-geojson.json';

import GoogleMapsWrapper, {
  injectGoogleMapsApiScript,
  polygonPathsFromBounds,
} from 'lib/google-maps';

import { googleApiKey } from 'components/maps';

import { fileServerPort } from 'src/const';

function prepareGeoPhoto(location) {
  const { properties, geometry } = location;
  const [coords] = geometry.coordinates;
  const imageId = properties.ImageID;
  const url = `http://localhost:${fileServerPort}/geoimages/${imageId}.png`;
  const id = properties.OBJECTID;
  const bounds = GoogleMapsWrapper.getPolyBounds(coords);
  const paths = polygonPathsFromBounds(bounds);
  const poly = new window.google.maps.Polygon({ paths });

  return {
    url,
    imageId,
    properties,
    coords,
    id,
    bounds,
    poly,
  };
}

function prepareGeoImages(project) {
  const { geoImagesDir } = project;

  if (typeof geoImagesDir !== 'string' || geoImagesDir === '') {
    return {};
  }

  if (!fs.existsSync(geoImagesDir)) {
    return {};
  }

  const existingAerialPhotos = fs.readdirSync(geoImagesDir)
    .map((fileName) => fileName.replace('.png', ''));
  const aerialPhotos = aerialPhotosJson.features
    .map(prepareGeoPhoto)
    .filter((p) => existingAerialPhotos.includes(p.imageId));
  const sequences = GoogleMapsWrapper.transformPointsToSequences(routePoints, route);

  return {
    route,
    routePoints,
    aerialPhotos,
    sequences,
  };
}

export default function prepareData(project) {
  return injectGoogleMapsApiScript(googleApiKey)
    .then(() => {
      return prepareGeoImages(project);
    });
}
