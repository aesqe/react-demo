const electron = require('electron');
const Config = require('electron-store');
const path = require('path');
const packageJson = require('../../../package.json');

const config = new Config();

const { app } = electron.remote || electron;

if (!config.has('projectfolder')) {
  config.set('projectfolder', '');
}

const appName = app.getName();
const rootDir = app.getAppPath();
const userdataDir = app.getPath('userData');

const projectFolder = config.get('projectfolder');
const dataDir = projectFolder || path.join(rootDir, 'data');

config.set({
  rootDir,
  userdataDir,
  dataDir,
  appName,
  packageJson,
});

module.exports = config;
