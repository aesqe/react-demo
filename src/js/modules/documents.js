import _ from 'lodash';

import schemas from 'data/schemas';
import { getCollection } from 'lib/database';
import { getTaxonomyById } from './taxonomies';

export function addNewDocument(data) {
  return new Promise((resolve, reject) => {
    const {
      collectionName,
      titleField = 'title',
      itemData = {},
      compare = [],
    } = data;

    const collection = getCollection(collectionName);

    if (!collection) {
      reject(`Collection ${collectionName} does not exist.`);
      return;
    }

    const schema = schemas[collectionName] || {};
    let collectionSingularName = collectionName.slice(0, -1);

    if (collectionName === 'terms') {
      const taxonomy = getTaxonomyById(itemData.taxonomyId);
      collectionSingularName = taxonomy.name.slice(0, -1);
    }

    const calculatedDefaults = {
      [titleField]: `New ${collectionSingularName} ${collection.maxId}`,
    };

    const doc = {
      ...schema,
      ...calculatedDefaults,
      ...itemData,
    };

    // `compare` is an array of prop names
    // if an item that has the same prop values as the new item
    // already exists, return that instead
    if (compare.length) {
      const compareQuery = {};

      compare.forEach((prop) => {
        compareQuery[prop] = doc[prop];
      });

      const existingDoc = collection.findOne(compareQuery);

      if (existingDoc) {
        resolve(existingDoc);
        return;
      }
    }

    const newDoc = collection.insert(doc);

    resolve(newDoc);
  });
}

export function getDocumentById(id, collectionName) {
  if (typeof id !== 'number') {
    return false;
  }

  return getCollection(collectionName).get(id);
}

export function duplicateDocument(doc) {
  const collection = getCollection(doc.collection);
  const newDoc = _.cloneDeep(doc);

  newDoc.title += ' (copy)';

  delete newDoc.$loki;
  delete newDoc.meta;

  collection.insert(newDoc);

  return newDoc;
}

export function deleteDocument(doc, collectionName) {
  if (doc && collectionName) {
    if (confirm('Really delete?')) {
      getCollection(collectionName).remove(doc);
      return doc;
    }
  }

  // eslint-disable-next-line no-console
  console.log(doc, collectionName);

  throw new Error('Trying to delete a document, but it has no collection specified.');
}

export function updateDocument(doc, patch = {}) {
  const collection = getCollection(doc.collection);
  const updatedDoc = collection.update({
    ...doc,
    ...patch,
  });

  return Promise.resolve(updatedDoc);
}
