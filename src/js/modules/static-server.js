// https://adrianmejia.com/blog/2016/08/24/
// building-a-node-js-static-file-server-files-over-http-using-es6/
const fs = require('fs');
const url = require('url');
const http = require('http');
const path = require('path');

const config = require('./config');
const { fileServerPort } = require('../../const.js');

const mimeType = {
  '.html': 'text/html',
  '.json': 'application/json',
  '.ico': 'image/x-icon',
  '.css': 'text/css',
  '.png': 'image/png',
  '.jpg': 'image/jpeg',
  '.wav': 'audio/wav',
  '.mp3': 'audio/mpeg',
  '.svg': 'image/svg+xml',
  '.pdf': 'application/pdf',
  '.doc': 'application/msword',
  '.eot': 'appliaction/vnd.ms-fontobject',
  '.ttf': 'aplication/font-sfnt',
  '.js': 'text/javascript',
};

function staticServer(pathMap, port = null, spa = false) {
  const server = http.createServer((req, res) => {
    const parsedUrl = url.parse(req.url);

    // Extract URL path
    // Avoid https://en.wikipedia.org/wiki/Directory_traversal_attack
    // e.g curl --path-as-is http://localhost:9000/../fileInDanger.txt
    // by limiting the path to current directory only
    const pathname = decodeURIComponent(parsedUrl.pathname);
    let sanitizedPath = pathname.replace(/^(\.\.[/\\])+/, '');

    if (sanitizedPath.slice(0, 1) === '/') {
      sanitizedPath = sanitizedPath.slice(1);
    }

    const firstPart = sanitizedPath.split('/').shift();
    let rootPath = pathMap.default;

    if (pathMap[firstPart]) {
      rootPath = pathMap[firstPart];
      sanitizedPath = sanitizedPath.split('/').slice(1).join('/');
    }

    if (typeof rootPath !== 'string' || rootPath.trim() === '') {
      res.end(`invalid rootPath: ${rootPath}`);
      return;
    }

    let filePath = path.join(rootPath, sanitizedPath);

    fs.exists(filePath, (exist) => {
      if(!exist) {
        if (!spa) {
          res.statusCode = 404;
          res.end(`File ${filePath} not found!`);
          return;
        }

        filePath = rootPath;
      }

      // If it's a directory, then look for index.html
      if (fs.statSync(filePath).isDirectory()) {
        filePath = path.join(filePath, '/index.html');
      }

      // read file from file system
      fs.readFile(filePath, (err, data) => {
        if (err) {
          res.statusCode = 500;
          res.end(`Error getting the file: ${err}.`);
        } else {
          const ext = path.extname(filePath);
          // if the file is found, set Content-type and send data
          res.setHeader('Content-type', mimeType[ext] || 'text/plain' );
          res.end(data);
        }
      });
    });
  });

  const serverWithShutdown = require('http-shutdown')(server);

  if (port) {
    serverWithShutdown.listen(port);
  }

  return serverWithShutdown;
}

function startFileServer(project) {
  const dataDir = config.get('dataDir');
  const { svImagesDir, geoImagesDir, additionalImagesDir } = project;

  const pathMap = {
    'default': dataDir,
    'streetviewimages': svImagesDir,
    'geoimages': geoImagesDir,
    'images': additionalImagesDir,
  };

  return staticServer(pathMap, fileServerPort);
}

let fileServer = null;

function handleFileServer(project) {
  if (fileServer !== null) {
    fileServer.shutdown(() => {
      fileServer = startFileServer(project);
      // eslint-disable-next-line no-console
      console.log('Static file server restarted successfully');
    });
  } else {
    fileServer = startFileServer(project);
    // eslint-disable-next-line no-console
    console.log('Static file server started successfully');
  }
}

module.exports = {
  staticServer,
  startFileServer,
  handleFileServer,
};
