import { combineReducers } from 'redux';
import _ from 'lodash';

import {
  db,
  getCollectionData,
  collectionExists,
} from 'lib/database';
import {
  ADD_NEW_TERM_SUCCESS,
  ADD_TERM_TO_ITEM_SUCCESS,
  REMOVE_TERM_FROM_ITEM_SUCCESS,
  RENAME_TERM_SUCCESS,
} from 'modules/taxonomies';

import {
  RELOAD_COLLECTION,
  LOAD_PROJECT_SUCCESS,
  UPDATE_DOCUMENT_SUCCESS,
  CREATE_NEW_PROJECT_SUCCESS,
} from './actions';

const debug = require('debug')('reducers');

function updateDocumentReducer(collectionItems, action) {
  const updatedItem = action.item;

  return collectionItems.map((item) => {
    if (item.$loki === updatedItem.$loki) {
      return updatedItem;
    }

    return item;
  });
}

function addNewTermToCollection(state = [], action) {
  const { term } = action;

  return [
    ...state,
    term,
  ];
}

function defaultCollectionReducer(collectionName, state = [], action) {
  if (!db.loaded) {
    return state;
  }

  if (action.type === CREATE_NEW_PROJECT_SUCCESS) {
    return [];
  }

  if (!collectionExists(collectionName)) {
    debug(`Collection ${collectionName} doesn't exist`);
    return state;
  }

  const collectionRequired = [
    UPDATE_DOCUMENT_SUCCESS,
    RELOAD_COLLECTION,
    ADD_TERM_TO_ITEM_SUCCESS,
    REMOVE_TERM_FROM_ITEM_SUCCESS,
    RENAME_TERM_SUCCESS,
  ];
  const actionRequiresCollection = collectionRequired.includes(action.type);
  const matchingCollection = (action.collectionName === collectionName);
  const shouldSkip = actionRequiresCollection && !matchingCollection;

  if (shouldSkip) {
    return state;
  }

  switch (action.type) {
    case 'DATABASE_LOADED':
    case RELOAD_COLLECTION:
      return getCollectionData(collectionName);

    case UPDATE_DOCUMENT_SUCCESS:
    case ADD_TERM_TO_ITEM_SUCCESS:
    case REMOVE_TERM_FROM_ITEM_SUCCESS:
    case RENAME_TERM_SUCCESS:
      return updateDocumentReducer(state, action);

    default:
      return state;
  }
}

function terms(state = [], action) {
  switch(action.type) {
    case ADD_NEW_TERM_SUCCESS:
      return addNewTermToCollection(state, action);
    default:
      return defaultCollectionReducer('terms', state, action);
  }
}

function genericCollection(name) {
  return (state, action) => defaultCollectionReducer(name, state, action);
}

const defaultCollectionsReducers = {
  terms,
};

const defaultCollectionsNames = Object.keys(defaultCollectionsReducers);

let collectionReducers = combineReducers(defaultCollectionsReducers);

let firstRun = true;

export function resetCollections() {
  collectionReducers = combineReducers(defaultCollectionsReducers);
}

export function updateCollectionReducers() {
  const collectionNames = db.getCollectionsNames().filter(Boolean);
  const otherCollectionsNames = collectionNames.filter(
    (n) => !defaultCollectionsNames.includes(n)
  );

  const otherCollectionsReducers = otherCollectionsNames.reduce((obj, name) => {
    obj[name] = genericCollection(name);
    return obj;
  }, {});

  collectionReducers = combineReducers({
    ...defaultCollectionsReducers,
    ...otherCollectionsReducers,
  });
}

export function collections(state = {}, action) {
  if (!db.loaded) {
    return {};
  }

  if (action.type === LOAD_PROJECT_SUCCESS) {
    firstRun = true;
  }

  if (firstRun) {
    updateCollectionReducers();
    firstRun = false;
  }

  if (action.type === RELOAD_COLLECTION) {
    if (!collectionExists(action.collectionName)) {
      debug(`Collection ${action.collectionName} doesn't exist, it was probably deleted.`);
      db.save(true);
      return _.omit(state, action.collectionName);
    }
  }

  if (action.type === 'ADD_NEW_COLLECTION_SUCCESS') {
    updateCollectionReducers();

    return {
      ...state,
      [action.payload.name]: action.payload,
    };
  }

  return collectionReducers(state, action);
}
