const initialState = {
  project: {},
  collections: {},
  database: {
    ready: false,
    loaded: false,
  },
  appUpdate: {
    invalidGithubToken: false,
    downloadProgress: {},
    updateAvailable: false,
    updateReady: false,
    downloading: false,
    checking: false,
    error: false,
    info: {},
  },
};

export default initialState;
