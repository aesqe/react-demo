import { applyMiddleware, createStore, compose } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import electronIsDev from 'electron-is-dev';

import mainReducer from './reducers';
import initialState from './initial-state';

export * from './actions';

export { updateCollectionReducers, resetCollections } from './collections';

const IS_DEV = electronIsDev && process.env.NODE_ENV !== 'production';

const middleware = [
  thunk,
];

if (IS_DEV) {
  const reduxLogger = createLogger({
    collapsed: true,
    diff: false,
  });

  middleware.push(reduxLogger);
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  mainReducer,
  initialState,
  composeEnhancers(
    applyMiddleware(...middleware)
  )
);

export default store;
