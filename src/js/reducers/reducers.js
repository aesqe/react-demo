import { combineReducers } from 'redux';

import { handleFileServer } from 'modules/static-server';

import {
  LOAD_PROJECT_SUCCESS,
  UPDATE_PROJECT_DATA,
  CLOSE_PROJECT,
  LOAD_DATABASE_SUCCESS,
  CREATE_NEW_PROJECT_REQUEST,
  APP_UPDATE_CHECKING_FOR_UPDATE,
  APP_UPDATE_DOWNLOAD_PROGRESS,
  APP_UPDATE_NOT_AVAILABLE,
  APP_UPDATE_DOWNLOADED,
  APP_UPDATE_AVAILABLE,
  APP_UPDATE_ERROR,
  APP_UPDATE_INVALID_GITHUB_TOKEN,
  APP_UPDATE_VALID_GITHUB_TOKEN,
} from './actions';

import { collections } from './collections';

function data(state = {}, action = {}) {
  switch(action.type) {
    case 'DATA_READY':
      return action.data;
    default:
      return state;
  }
}

function appUpdate(state = {}, action = {}) {
  const { type, payload } = action;

  switch (type) {
    case APP_UPDATE_DOWNLOAD_PROGRESS:
      return {
        ...state,
        checking: false,
        updateAvailable: true,
        downloading: true,
        downloadProgress: payload,
      };
    case APP_UPDATE_DOWNLOADED:
      return {
        ...state,
        checking: false,
        updateAvailable: true,
        updateReady: true,
      };
    case APP_UPDATE_AVAILABLE:
      return {
        ...state,
        checking: false,
        updateAvailable: true,
        info: payload,
      };
    case APP_UPDATE_NOT_AVAILABLE:
      return {
        ...state,
        checking: false,
        updateAvailable: false,
        info: payload,
      };
    case APP_UPDATE_CHECKING_FOR_UPDATE:
      return {
        ...state,
        error: false,
        checking: true,
        updateAvailable: false,
      };
    case APP_UPDATE_ERROR:
      return {
        ...state,
        checking: false,
        error: payload,
      };
    case APP_UPDATE_INVALID_GITHUB_TOKEN:
      return {
        ...state,
        error: true,
        checking: false,
        invalidGithubToken: true,
      };
    case APP_UPDATE_VALID_GITHUB_TOKEN:
      return {
        ...state,
        error: false,
        checking: false,
        invalidGithubToken: false,
      };
    default:
      return state;
  }
}

function database(state = {}, action) {
  switch(action.type) {
    case 'DATABASE_LOADED':
      return {
        ...state,
        loaded: true,
      };
    case LOAD_DATABASE_SUCCESS:
      return {
        ...state,
        ready: true,
        filePath: action.filePath,
      };
    case CREATE_NEW_PROJECT_REQUEST:
      return {
        ...state,
        ready: false,
        loaded: false,
      };
    default:
      return state;
  }
}

function project(state = {}, action) {
  switch(action.type) {
    case UPDATE_PROJECT_DATA:
      handleFileServer(action.payload);
      return action.payload;
    case LOAD_PROJECT_SUCCESS:
      return action.payload;
    case CLOSE_PROJECT:
      return {};
    default:
      return state;
  }
}

const mainReducer = combineReducers({
  data,
  project,
  database,
  collections,
  appUpdate,
});

export default mainReducer;
