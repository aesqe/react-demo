import { ipcRenderer } from 'electron';
import path from 'path';
import slugg from 'slugg';
import fs from 'fs-extra';

import {
  loadDatabaseFromFile,
  databaseLoaded,
  addCollection,
  collectionExists,
} from 'lib/database';
import {
  createNewProject,
  createNewDatabase,
  getProjectFileContents,
  getDatabaseFilePath,
  saveProjectData,
} from 'modules/project';
import config from 'modules/config';
import prepareData from 'modules/prepare-data';
import { updateDocument, addNewDocument, deleteDocument } from 'modules/documents';
import { handleFileServer } from 'modules/static-server';

import { TAXONOMIES } from 'data/defaults/default-collections.json';

export const GENERATE_NEW_ID = 'GENERATE_NEW_ID';

export const CREATE_NEW_PROJECT_REQUEST = 'CREATE_NEW_PROJECT_REQUEST';
export const CREATE_NEW_PROJECT_SUCCESS = 'CREATE_NEW_PROJECT_SUCCESS';
export const LOAD_PROJECT_REQUEST = 'LOAD_PROJECT_REQUEST';
export const LOAD_PROJECT_SUCCESS = 'LOAD_PROJECT_SUCCESS';
export const UPDATE_PROJECT_DATA = 'UPDATE_PROJECT_DATA';
export const CLOSE_PROJECT = 'CLOSE_PROJECT';

export const RELOAD_COLLECTION = 'RELOAD_COLLECTION';
export const LOAD_DATABASE_REQUEST = 'LOAD_DATABASE_REQUEST';
export const LOAD_DATABASE_SUCCESS = 'LOAD_DATABASE_SUCCESS';
export const CREATE_NEW_DATABASE_REQUEST = 'CREATE_NEW_DATABASE_REQUEST';
export const CREATE_NEW_DATABASE_SUCCESS = 'CREATE_NEW_DATABASE_SUCCESS';
export const CLOSE_DATABASE = 'CLOSE_DATABASE';

export const UPDATE_DOCUMENT_REQUEST = 'UPDATE_DOCUMENT_REQUEST';
export const UPDATE_DOCUMENT_SUCCESS = 'UPDATE_DOCUMENT_SUCCESS';

export const APP_UPDATE_INVALID_GITHUB_TOKEN = 'APP_UPDATE_INVALID_GITHUB_TOKEN';
export const APP_UPDATE_VALID_GITHUB_TOKEN = 'APP_UPDATE_VALID_GITHUB_TOKEN';
export const APP_UPDATE_CHECKING_FOR_UPDATE = 'APP_UPDATE_CHECKING_FOR_UPDATE';
export const APP_UPDATE_DOWNLOAD_PROGRESS = 'APP_UPDATE_DOWNLOAD_PROGRESS';
export const APP_UPDATE_NOT_AVAILABLE = 'APP_UPDATE_NOT_AVAILABLE';
export const APP_UPDATE_DOWNLOADED = 'APP_UPDATE_DOWNLOADED';
export const APP_UPDATE_AVAILABLE = 'APP_UPDATE_AVAILABLE';
export const APP_UPDATE_ERROR = 'APP_UPDATE_ERROR';

export function prepareDataThunk(project) {
  return async function (dispatch) {
    const data = await prepareData(project);
    return dispatch({
      type: 'DATA_READY',
      data,
    });
  };
}

export function createNewProjectAction(filePath) {
  return {
    type: CREATE_NEW_PROJECT_REQUEST,
    filePath,
  };
}

export function createdNewProjectAction(filePath) {
  return {
    type: CREATE_NEW_PROJECT_SUCCESS,
    filePath,
  };
}

export function createNewDatabaseAction(filePath) {
  return {
    type: CREATE_NEW_DATABASE_REQUEST,
    filePath,
  };
}

export function createdNewDatabaseAction(filePath) {
  return {
    type: CREATE_NEW_DATABASE_SUCCESS,
    filePath,
  };
}

export function loadProjectAction(filePath) {
  return {
    type: LOAD_PROJECT_REQUEST,
    filePath,
  };
}

export function projectLoadedAction(filePath, payload) {
  return {
    type: LOAD_PROJECT_SUCCESS,
    filePath,
    payload,
  };
}

export function loadDatabaseAction(filePath) {
  return {
    type: LOAD_DATABASE_REQUEST,
    filePath,
  };
}

export function databaseLoadedAction(filePath) {
  return {
    type: LOAD_DATABASE_SUCCESS,
    filePath,
  };
}

export function reloadCollectionAction(collectionName) {
  return {
    type: RELOAD_COLLECTION,
    collectionName,
  };
}

export function updateDocumentAction(item) {
  return {
    type: UPDATE_DOCUMENT_REQUEST,
    collectionName: item.collection,
    item,
  };
}

export function updatedDocumentAction(item) {
  return {
    type: UPDATE_DOCUMENT_SUCCESS,
    collectionName: item.collection,
    item,
  };
}

export function updateDocumentThunk(item, patch = {}) {
  return async function (dispatch) {
    dispatch(updateDocumentAction(item));
    const updatedItem = await updateDocument(item, patch);
    dispatch(updatedDocumentAction(updatedItem));
    // return dispatch(reloadCollectionAction(item.collection));
  };
}

export function loadDatabaseFromFileThunk(filePath, options) {
  return async function (dispatch) {
    dispatch(loadDatabaseAction(filePath));
    await loadDatabaseFromFile(filePath, options, dispatch);

    return dispatch(databaseLoadedAction(filePath));
  };
}

export function loadProjectThunk(projectFilePath) {
  return async function(dispatch) {
    dispatch(loadProjectAction(projectFilePath));
    const project = getProjectFileContents(projectFilePath);
    await dispatch(projectLoadedAction(projectFilePath, project));
    await dispatch(prepareDataThunk(project));

    handleFileServer(project);

    return Promise.resolve(project);
  };
}

export function loadProjectAndDatabaseThunk(projectFilePath) {
  return async function (dispatch) {
    const databaseFilePath = getDatabaseFilePath(projectFilePath);
    await dispatch(loadDatabaseFromFileThunk(databaseFilePath));
    return dispatch(loadProjectThunk(projectFilePath));
  };
}

export function createNewProjectThunk(name) {
  return async function (dispatch) {
    dispatch(createNewProjectAction(name));
    await createNewProject(name);
    dispatch(createdNewProjectAction(name));

    return dispatch(loadProjectThunk(config.get('projectFilePath')));
  };
}

export function createNewDatabaseThunk(name, filePath = '') {
  return async function (dispatch) {
    const dirName = path.dirname(filePath);
    const ext = path.extname(filePath);
    const fileName = path.basename(filePath, ext);

    let dbFilePath = filePath;
    let counter = 1;

    while (fs.existsSync(dbFilePath)) {
      counter += 1;

      if (counter > 20) {
        throw new Error(
          `Error trying to create a new database file - too many iterations: ${filePath}`
        );
      }

      dbFilePath = path.join(dirName, `${fileName}-${counter}${ext}`);
    }

    dispatch(createNewDatabaseAction(dbFilePath));
    const db = await createNewDatabase(name, dbFilePath, dispatch);
    dispatch(createdNewDatabaseAction(dbFilePath));

    return db;
  };
}

export function createNewProjectWithDatabaseThunk(name) {
  return async function (dispatch) {
    dispatch(createNewProjectAction(name));
    await createNewProject(name);

    const projectFilePath = config.get('projectFilePath');
    const projectDir = path.dirname(projectFilePath);
    const ext = path.extname(projectFilePath);
    const projectFileName = slugg(path.basename(projectFilePath, ext));
    const databaseFileName = `${projectFileName}-database.json`;
    const databaseFilePath = path.join(projectDir, databaseFileName);

    dispatch(createdNewProjectAction(name));
    await dispatch(createNewDatabaseThunk(name, databaseFilePath));
    saveProjectData({ databaseFilePath });
    await dispatch(loadDatabaseFromFileThunk(databaseFilePath, { isNew: true }));
    await dispatch(loadProjectThunk(projectFilePath));
    databaseLoaded(true);

    dispatch({
      type: 'DATABASE_LOADED',
    });

    return dispatch(reloadCollectionAction(TAXONOMIES));
  };
}

export function updateProjectAction(data) {
  return {
    type: UPDATE_PROJECT_DATA,
    payload: saveProjectData(data),
  };
}

export function appUpdateCheckForUpdatesThunk() {
  return function(dispatch) {
    dispatch({
      type: 'APP_UPDATE_CHECK_FOR_UPDATES',
    });

    return ipcRenderer.send('autoupdater', 'check-for-updates');
  };
}

export function appUpdateValidateGithubTokenThunk(token) {
  return function(dispatch) {
    dispatch({
      type: 'APP_UPDATE_VALIDATE_GITHUB_TOKEN',
      payload: token,
    });

    return ipcRenderer.send('autoupdater', 'validate-github-token', token);
  };
}

export function appUpdateDownloadUpdateThunk() {
  return function(dispatch) {
    dispatch({
      type: 'APP_UPDATE_DOWNLOAD_UPDATE',
    });

    return ipcRenderer.send('autoupdater', 'download-update');
  };
}
export function appUpdateInstallUpdateThunk() {
  return function(dispatch) {
    dispatch({
      type: 'APP_UPDATE_INSTALL_UPDATE',
    });

    return ipcRenderer.send('autoupdater', 'install-update');
  };
}

export function addNewCollectionAction(collectionName) {
  return {
    type: 'ADD_NEW_COLLECTION_REQUEST',
    payload: collectionName,
  };
}

export function addedNewCollectionAction(collection) {
  return {
    type: 'ADD_NEW_COLLECTION_SUCCESS',
    payload: collection,
  };
}

export function addNewCollectionThunk(collectionName) {
  return (dispatch) => {
    dispatch(addNewCollectionAction(collectionName));
    const collection = addCollection(collectionName);
    dispatch(addedNewCollectionAction(collection));
    return collection;
  };
}

export function addNewDocumentThunk(doc, type = 'DOCUMENT') {
  const { collectionName } = doc;

  return async (dispatch) => {
    dispatch({ type: `ADD_NEW_${type}_REQUEST`, payload: doc });

    if (!collectionExists(collectionName)) {
      await addNewCollectionThunk(collectionName);
    }

    const res = await addNewDocument(doc);

    dispatch({ type: `ADD_NEW_${type}_SUCCESS`, payload: doc });
    dispatch(reloadCollectionAction(collectionName));

    return res;
  };
}

export function deleteDocumentThunk(doc, type = 'DOCUMENT') {
  const { collection } = doc;

  return async (dispatch) => {
    dispatch({ type: `DELETE_${type}_REQUEST`, payload: doc });

    const res = await deleteDocument(doc, collection);

    dispatch({ type: `DELETE_${type}_SUCCESS`, payload: doc });
    dispatch(reloadCollectionAction(collection));

    return res;
  };
}
