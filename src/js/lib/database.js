import fs from 'fs-extra';
import path from 'path';
import Lokijs from 'lokijs';
import moment from 'moment';

import defaultCollections from 'data/defaults/default-collections.json';

export const defaultCollectionNames = Object.values(defaultCollections);

const debug = require('debug')('Database');

const autoBackupInterval = 30 * 60 * 1000; // 1 hour in ms
let lastAutoBackupTimeStamp = Date.now();

export let db = {
  name: 'fake',
  ready: false,
  loaded: false,
};

export function database(dbFilePath, opts = {}) {
  const options = Object.assign({
    autosave: true,
    autosaveInterval: 10000,
  }, opts);

  const loki = new Lokijs(dbFilePath, options);

  const lokidb = {
    name: 'real',
    ready: false,
    loaded: false,
    dbClass: Lokijs,
    root: loki,
    addCollection: loki.addCollection.bind(loki),
    getCollection: loki.getCollection.bind(loki),
    loadCollection: loki.loadCollection.bind(loki),
    removeCollection: loki.removeCollection.bind(loki),
    listCollections: loki.listCollections.bind(loki),
    loadDatabase: loki.loadDatabase.bind(loki),
    loadJSONObject: loki.loadJSONObject.bind(loki),
    close: loki.close.bind(loki),

    setReady(val) {
      this.ready = val;
    },

    setLoaded(val) {
      this.loaded = val;
    },

    save(force = false) {
      return new Promise((resolve, reject) => {
        if (options.autosave && !loki.autosave && !force) {
          return resolve();
        }

        return loki.save((err) => {
          if (err) {
            reject(err);
            return;
          }

          resolve(true);
        });
      });
    },

    insert(collectionName, items) {
      const collection = loki.getCollection(collectionName);

      if (!collection) {
        throw new Error(`Collection ${collectionName} doesn't exist`);
      }

      return [].concat(items).map((item) => {
        delete item.$loki;

        collection.insert(item);

        let newTitle = '';

        if (item.title === 'New document') {
          if (!item.parent) {
            newTitle = `New document ${item.$loki}`;
          } else {
            newTitle = `New child document ${item.$loki}`;
          }

          item.title = newTitle;

          collection.update(item);
        }

        return item;
      });
    },

    update(collectionName, items) {
      const collection = loki.getCollection(collectionName);

      if (!collection) {
        throw new Error(`Collection ${collectionName} doesn't exist`);
      }

      return collection.update(items);
    },

    remove(collectionName, item) {
      const collection = loki.getCollection(collectionName);

      if (!collection) {
        throw new Error(`Collection ${collectionName} doesn't exist`);
      }

      return collection.remove(item);
    },

    trash(collectionName, items) {
      const collection = loki.getCollection(collectionName);

      if (!collection) {
        throw new Error(`Collection ${collectionName} doesn't exist`);
      }

      [].concat(items).forEach((item) => {
        item.status = 'deleted';
      });

      return collection.update(items);
    },

    emptyTrash(collectionName) {
      const collection = loki.getCollection(collectionName);

      if (!collection) {
        throw new Error(`Collection ${collectionName} doesn't exist`);
      }

      return collection.removeWhere({ status: 'deleted' });
    },

    getCollectionItems(collectionName, opt = {}) {
      const collection = loki.getCollection(collectionName);

      if (!collection) {
        throw new Error(`Collection ${collectionName} doesn't exist`);
      }

      const defaults = {
        sortby: '$loki',
        limit: 0,
        offset: 0,
        find: {},
      };

      const o = Object.assign(defaults, opt);

      const find = Object.assign({
        status: { $ne: 'deleted' },
        type: { $ne: 'collectionMetadata' },
      }, o.find);

      const { sortby, limit, offset } = o;

      let chain = collection.chain().find(find);

      if (limit > 0) {
        chain = chain.limit(limit);
      }

      if (offset > 0) {
        chain = chain.offset(offset);
      }

      return chain
        .simplesort(sortby)
        .data();
    },

    getCollectionName(collection) {
      return collection.name || collection;
    },

    getCollectionsNames() {
      return loki.listCollections().map(c => c.name);
    },

    // meta && schemas
    getDefaultMeta(collectionName) {
      return {
        type: 'collectionMetadata',
        title: collectionName,
        schema: [{
          name: 'title',
          type: 'textfield',
          maxlength: -1,
        }],
      };
    },

    getCollectionMeta(collectionName, key) {
      const collection = loki.getCollection(collectionName);

      if (!collection) {
        throw new Error(`Collection ${collectionName} doesn't exist`);
      }

      let meta = collection.findOne({ type: 'collectionMetadata' });

      if (!meta) {
        meta = collection.insert(this.getDefaultMeta(collectionName));
      }

      if (key && Object.prototype.hasOwnProperty.call(meta, key)) {
        return meta[key];
      }

      return meta;
    },

    resetCollectionMeta(collectionName) {
      const collection = loki.getCollection(collectionName);

      if (!collection) {
        throw new Error(`Collection ${collectionName} doesn't exist`);
      }

      const meta = collection.findOne({ type: 'collectionMetadata' });

      if (meta) {
        collection.remove(meta);
      }

      return collection.insert(this.getDefaultMeta(collectionName));
    },

    getCollectionSchema(collectionName) {
      return this.getCollectionMeta(collectionName, 'schema');
    },
  };

  if (fs.existsSync(dbFilePath) === false) {
    lokidb.save();
  }

  return lokidb;
}

export function databaseLoaded(val) {
  db.setLoaded(val);
}

export function databaseReady(val) {
  db.setReady(val);
}

function autoBackup(databaseFilePath) {
  const timeStamp = Date.now();

  if (timeStamp - lastAutoBackupTimeStamp >= autoBackupInterval) {
    const extname = path.extname(databaseFilePath);
    const fileName = databaseFilePath.replace(extname, '');
    const dateTime = moment().format('YYYY-MM-DD-HH-mm-ss');
    const dbFileNameWithDateTime = `${fileName}-${dateTime}${extname}`;

    fs.copySync(databaseFilePath, dbFileNameWithDateTime);

    lastAutoBackupTimeStamp = timeStamp;

    debug('Created a database backup', dbFileNameWithDateTime);
  }
}

export function loadDatabaseFromFile(databaseFilePath, opts = {}, dispatch = () => {}) {
  return new Promise((resolve) => {
    let options = {};

    const defaults = {
      autosave: true,
      autoload: true,
      isNew: false,
      autosaveInterval: 10000,
      autosaveCallback() {
        dispatch({
          type: 'DATABASE_AUTOSAVED',
        });

        autoBackup(databaseFilePath);
      },
      autoloadCallback() {
        databaseReady(true);

        dispatch({
          type: 'DATABASE_AUTOLOADED',
        });

        if (!options.isNew) {
          databaseLoaded(true);

          dispatch({
            type: 'DATABASE_LOADED',
          });
        }

        resolve(db);
      },
    };

    options = {
      ...defaults,
      ...opts,
    };

    db = database(databaseFilePath, options);
  });
}

export function getCollection(collectionName) {
  if (!db.ready) {
    throw new Error('Database is not ready');
  }

  return db.getCollection(collectionName);
}

export function getCollectionData(collectionName) {
  if (!db.ready) {
    throw new Error('Database is not ready');
  }

  return db.getCollectionItems(collectionName);
}

export function collectionExists(collectionName) {
  return !!getCollection(collectionName);
}

export function addCollection(collectionName) {
  if (!db.ready) {
    throw new Error('Database is not ready');
  }

  const existingCollection = db.getCollection(collectionName);

  if (existingCollection) {
    return existingCollection;
  }

  const collection = db.addCollection(collectionName);
  collection.insert(db.getDefaultMeta(collectionName));

  return collection;
}

export function removeCollection(collectionName) {
  db.removeCollection(collectionName);
}

export function clearCollection(collectionName) {
  const collection = getCollection(collectionName);

  if (!collection) {
    throw new Error('Database is not ready');
  }

  collection.clear();
}

export function getNonDefaultCollectionNames() {
  return db.getCollectionsNames()
    .filter(n => !defaultCollectionNames.includes(n));
}
