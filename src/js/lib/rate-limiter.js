import Events from 'stupidly-simple-events';

const debug = require('debug')('RateLimiter');

function isPromise(obj) {
  return obj && Object.prototype.toString.call(obj) === '[object Promise]';
}

export default class RateLimiter {
  constructor() {
    this.running = false;
    this.throttle = 0;
    this.intervalId = 0;
    this.queueLength = 0;
    this.queueItemsProcessed = 0;
    this.queue = [];
    this.data = [];
    this.events = Events();
    this.on = this.events.on.bind(this.events);
    this.one = this.events.one.bind(this.events);
    this.off = this.events.off.bind(this.events);
    this.fire = this.events.fire.bind(this.events);
    this.updateProgress = this.updateProgress.bind(this);
    this.next = this.next.bind(this);
  }

  add(items) {
    if (!Array.isArray(items)) {
      return this.queue.push(items);
    }

    this.queue = this.queue.concat(items);

    if (this.running && this.intervalId === 0) {
      this.processQueue();
    }

    return this;
  }

  next() {
    if (this.queue.length === 0) {
      this.fire('queue_done', this.data);
      this.data = [];
      this.stop();
      return;
    }

    const fn = this.queue.shift();

    if (typeof fn !== 'function') {
      debug('Not a function', fn);
      return;
    }

    const res = fn();
    this.data.push(res);

    if (isPromise(res)) {
      res.then(this.updateProgress);
      return;
    }

    this.updateProgress(res);
  }

  updateProgress(data) {
    this.queueItemsProcessed += 1;
    this.fire('data', data);
    this.fire('progress', this.queueLength, this.queueItemsProcessed);
  }

  processQueue(resetQueueLength = true) {
    if (!this.running) {
      this.intervalId = setInterval(this.next, this.throttle);
      this.running = true;

      if (resetQueueLength) {
        this.queueLength = this.queue.length;
      }
    }

    return this.prom;
  }

  stop() {
    this.pause();
    this.running = false;
  }

  pause() {
    clearInterval(this.intervalId);
    this.intervalId = 0;
  }

  resume() {
    this.running = false;
    this.processQueue(false);
  }

  abort() {
    this.stop();
    this.reset();
  }

  reset() {
    this.queueItemsProcessed = 0;
    this.queueLength = 0;
    this.queue = [];
    this.data = [];
  }
}
