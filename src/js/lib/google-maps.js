import _ from 'lodash';

const debug = require('debug')('GoogleMaps');

let injectedGoogleMapsApiScript = false;
let googleMapsInjectPromise;

export function injectGoogleMapsApiScript(apiKey, googleMapsCallbackName = 'googleMapsScriptLoaded') {
  if (!injectedGoogleMapsApiScript) {
    injectedGoogleMapsApiScript = true;

    const baseUrl = 'https://maps.googleapis.com/maps/api/js';
    const src = `${baseUrl}?key=${apiKey}&callback=${googleMapsCallbackName}`;
    const head = document.querySelector('head');
    const script = document.createElement('script');

    // eslint-disable-next-line no-unused-vars
    googleMapsInjectPromise = new Promise((resolve, reject) => {
      window[googleMapsCallbackName] = resolve;

      script.setAttribute('src', src);
      script.setAttribute('async', true);
      script.setAttribute('defer', true);

      head.appendChild(script);
    });
  }

  return googleMapsInjectPromise;
}

export function convertPointCoordsToLatLng(r, swap) {
  if (r.length > 2) { r = r.slice(0, 2); }
  if (swap) { r = r.slice().reverse(); }
  return new window.google.maps.LatLng(r[0], r[1]);
}

export function convertCoordsToLatLng (coordsArray, swap) {
  return coordsArray.map((r) => convertPointCoordsToLatLng(r, swap));
}

export function objToUrl (obj) {
  return Object.keys(obj).map((key) => {
    return `${encodeURIComponent(key)}=${encodeURIComponent(obj[key])}`;
  }).join('&');
}

export function polygonPathsFromBounds (bounds){
  const paths = new window.google.maps.MVCArray();
  const path = new window.google.maps.MVCArray();

  const ne = bounds.getNorthEast();
  const sw = bounds.getSouthWest();
  const se = new window.google.maps.LatLng(sw.lat(), ne.lng());
  const nw = new window.google.maps.LatLng(ne.lat(), sw.lng());

  path.push(ne);
  path.push(se);
  path.push(sw);
  path.push(nw);

  paths.push(path);

  return paths;
}

export default class GoogleMapsWrapper {
  constructor(options) {
    const defaults = {
      mapContainer: null,
      mapTypeId: window.google.maps.MapTypeId.ROAD,
      center: [45, 45],
      zoom: 4,
      streetviewRadius: 50,
      styles: [],
      onDirectionsChange: () => {},
    };

    this.options = {
      ...defaults,
      ...options,
    };

    if (!this.options.mapContainer) {
      throw new Error('Gmapper: invalid mapContainer element in options', options);
    }

    const [cLat, cLng] = this.options.center;

    this.options.center = new window.google.maps.LatLng(cLat, cLng);

    this.map = new window.google.maps.Map(this.options.mapContainer, this.options);
    this.streetViewService = new window.google.maps.StreetViewService();
    this.directionsService = new window.google.maps.DirectionsService();
    this.directionsRenderer = new window.google.maps.DirectionsRenderer({
      draggable: true,
      preserveViewport: true,
      map: this.map,
    });

    this.labels = [];
    this.markers = [];
    this.overlays = [];
    this.polylines = [];
    this.currentDirections = [];
  }

  getStreetViewPanorama (location, radius = 50) {
    return new Promise(((resolve, reject) => {
      this.streetViewService.getPanoramaByLocation(location, radius, (result, status) => {
        if (status === window.google.maps.StreetViewStatus.OK) {
          resolve(result);
        } else {
          reject(status);
        }
      });
    }));
  }

  getRouteStreetViewImages (opts) {
    const out = [];
    const defaults = {};

    _.defaults(opts, defaults);

    opts.route.forEach((location) => {
      out.push(this.getStreetViewPanorama(location, opts.streetviewRadius)
        .then((result) => {
          return this.getStreetViewImage({
            location: result.location.latLng.toUrlValue(),
            size: '640x640',
            heading: 90,
          });
        }));
    });

    return out;
  }

  geocodeReverse (location, callback) {
    const latLng = location.toUrlValue();
    const url = 'https://maps.googleapis.com/maps/api/geocode/json';
    const vars = `?key=${this.options.apiKey}&latlng=${latLng}`;

    fetch(`${url}${vars}`).done(callback);
  }

  objToUrl (obj) {
    return  objToUrl (obj);
  }

  getStreetViewImage (opts) {
    // https://maps.googleapis.com/maps/api/streetview
    // ?size=640x400&location=46.414382,10.013988
    const base = 'https://maps.googleapis.com/maps/api/streetview?';
    const defaults = {};

    _.defaults(opts, defaults);

    const vals = this.objToUrl(opts);

    return base + vals;
  }

  getDirections (options, callback) {
    const defaults = {
      origin: null,
      destination: null,
      travelMode: window.google.maps.DirectionsTravelMode.DRIVING,
    };

    _.defaults(options, defaults);

    if (!options.origin) {
      // eslint-disable-next-line no-console
      console.log('Gmapper.getDirections error: invalid origin', options);
      return;
    }

    if (!options.destination) {
      // eslint-disable-next-line no-console
      console.log('Gmapper.getDirections error: invalid destination', options);
      return;
    }

    const defaultCallback = (response, status) => {
      if (status !== window.google.maps.DirectionsStatus.OK) {
        debug(response, status);
        return;
      }

      this.renderDirections(response);
    };

    this.directionsService.route(options, (result, status) => {
      this.currentDirections = result;
      (callback || defaultCallback)(result, status);

      this.options.onDirectionsChange(result);
    });

    this.directionsRenderer.addListener('directions_changed', () => {
      const directions = this.directionsRenderer.getDirections();
      this.currentDirections = directions;
      this.options.onDirectionsChange(directions);
    });
  }

  setStyles (styles) {
    this.map.set('styles', styles);
  }

  renderDirections (directions) {
    this.directionsRenderer.setDirections(directions);
  }

  dataLayerAddGeoJson(geoJSON) {
    this.map.data.addGeoJson(geoJSON);
  }

  dataLayerClear() {
    this.map.data.forEach(feature => this.map.data.remove(feature));
  }

  dataLayerSetStyle(style) {
    this.map.data.setStyle(style);
  }

  dataLayerZoomAndCenter() {
    const bounds = new window.google.maps.LatLngBounds();

    this.map.data.forEach((feature) => {
      feature.getGeometry().forEachLatLng((latlng) => {
        bounds.extend(latlng);
      });
    });

    this.map.fitBounds(bounds);
  }

  drawPolyline (opts) {
    if( ! opts.path ) {
      throw new Error('Gmapper.drawPoly: missing path in options');
    }

    const defaults = {
      path: [],
      geodesic: true,
      strokeColor: '#FF0000',
      strokeOpacity: 0.5,
      strokeWeight: 8,
    };

    _.defaults(opts, defaults);

    const poly = new window.google.maps.Polyline(opts);

    this.polylines.push(poly);

    poly.setMap(this.map);

    return poly;
  }

  drawPolylines (lines) {
    lines.forEach(this.drawPolyline.bind(this));
  }

  clearPolyLines(_polylines) {
    if (!_polylines) {
      _polylines = this.polylines;
    }

    _polylines.forEach((polyline) => polyline.setMap(null));
  }

  addMarker ( opts = {} ) {
    const { center } = this.options;
    const defaults = {
      position: center,
      draggable: false,
      listeners: [],
      icon: 'http://maps.google.com/mapfiles/kml/paddle/red-circle.png',
    };

    const options = Object.assign(defaults, opts);

    const marker = new window.google.maps.Marker({
      position: options.position,
      draggable: options.draggable,
      map: this.map,
      icon: options.icon,
    });

    this.markers.push(marker);

    if( options.listeners.length ) {
      options.listeners.forEach((event) => {
        window.google.maps.event.addListener(marker, event.name, event.func);
      });
    }

    return marker;
  }

  addMidPoints (route) {
    const points = route.reduce((res, point1, index) => {
      const p1 = new window.google.maps.LatLng(point1[0], point1[1]);
      const point2 = route[index + 1];

      res.push(point1);

      if (point2) {
        const p2 = new window.google.maps.LatLng(point2[0], point2[1]);
        const midpoint = p1.midpointTo(p2);
        res.push([midpoint.lat, midpoint.lon]);
      }

      return res;
    });

    const blob = new Blob(
      [JSON.stringify(points)],
      {type: 'text/plain'}
    );

    window.location.href = URL.createObjectURL(blob);
  }

  addOverlays (overlays = [], opts = {}) {
    overlays.forEach(o => {
      const overlay = GoogleMapsWrapper.convertToGroundOverlay(o, opts);
      this.overlays.push(overlay);
      overlay.setMap(this.map);
    });
  }

  clearOverlays () {
    const o = this.overlays;

    for (; o.length;) {
      o.pop().setMap(null);
    }
  }

  static decodePath (path) {
    return window.google.maps.geometry.encoding.decodePath(path);
  }

  static convertCoordsToLatLng (coordsArray, swap) {
    return convertCoordsToLatLng (coordsArray, swap);
  }

  static convertPointCoordsToLatLng(r, swap) {
    return convertPointCoordsToLatLng(r, swap);
  }

  static convertToWayPoints (coordsArray) {
    const out = [];

    coordsArray.forEach((r, i) => {
      if (i < 10) {
        out.push({
          location: new window.google.maps.LatLng(r[0], r[1]),
          stopover: false,
        });
      }
    });

    return out;
  }

  static transformPointsToSequences (points, route) {
    return points.reduce((res, p1, i) => {
      const p2 = points[i + 1];

      if (p2) {
        const start = p1.point;
        const end = p2.point - 1;
        const coords = route.slice(start, end);

        res.push({
          name: `${p1.name}-${p2.name}`,
          points: convertCoordsToLatLng(coords),
        });
      }

      return res;
    }, []);
  }

  static getGeoPhotosForLocation (photos, location) {
    return photos.filter((p) => {
      const poly = new window.google.maps.Polygon({
        paths: convertCoordsToLatLng(p.coords, true),
      });

      return window.google
        .maps.geometry.poly
        .containsLocation(location, poly);
    });
  }

  static convertToGroundOverlay (overlay, opts = {opacity: 1.0}) {
    return new window.google.maps.GroundOverlay(
      overlay.url,
      overlay.bounds,
      opts
    );
  }

  static getBounds(coords) {
    const bounds = new window.google.maps.LatLngBounds();
    const coordinates = coords.map(
      c => new window.google.maps.LatLng(c[1], c[0])
    );

    coordinates.map(bounds.extend.bind(bounds));

    return bounds;
  }

  static getBoundsFromCoordinates(coords) {
    const bounds = new window.google.maps.LatLngBounds();
    coords.forEach(c => bounds.extend(new window.google.maps.LatLng(c[1], c[0])));
    return bounds;
  }

  static getPolyBounds(coords) {
    const bounds = new window.google.maps.LatLngBounds();
    const coordinates = coords.map(c =>
      new window.google.maps.LatLng(c[1], c[0])
    );
    const poly = new window.google.maps.Polygon({
      paths: coordinates,
    });

    poly.getPaths().forEach(path => {
      path.getArray().forEach((latLng) => {
        bounds.extend(latLng);
      });
    });

    return bounds;
  }

  static distanceBetween (origin, destination) {
    return window.google.maps
      .geometry.spherical
      .computeDistanceBetween(origin, destination);
  }

  static pathLength(latLngCoords) {
    return window.google.maps.geometry.spherical.computeLength(latLngCoords);
  }

  static routeDistance(coords) {
    const len = coords.length;
    let dist = 0;
    let i = 0;

    for (i; i < len - 1; i++) {
      dist += GoogleMapsWrapper.distanceBetween(coords[i], coords[i+1]);
    }

    return dist;
  }
}
