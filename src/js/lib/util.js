import path from 'path';
import slash from 'slash';
import fs from 'fs-extra';
import log from 'electron-log';

const { info: infoLog } = log;

export function platformSlash(filePath) {
  if (!filePath) {
    throw new Error('filePath is empty or undefined');
  }

  const isWindows = (process.platform === 'win32');

  return isWindows ?
    path.win32.normalize(filePath) :
    slash(filePath);
}

export function fileExists(filePath) {
  if (typeof filePath !== 'string' || filePath === '') {
    return false;
  }

  const slashedPath = platformSlash(filePath);

  return fs.existsSync(slashedPath);
}

export function fileExistsAndIsReadable(filePath) {
  if (!filePath) {
    return false;
  }

  return fileExists(filePath) !== false && fs.readFileSync(filePath) !== false;
}

export function getFileContents(filePath, readAs = 'utf8') {
  const slashedPath = platformSlash(filePath);

  if(!fileExistsAndIsReadable(filePath)) {
    return false;
  }

  if (readAs === 'json') {
    try {
      return fs.readJsonSync(slashedPath);
    } catch (err) {
      return false;
    }
  }

  return fs.readFileSync(slashedPath, readAs);
}

export function getJsonFileContents(filePath) {
  return getFileContents(filePath, 'json');
}

export function padLeft(input, padWith, length) {
  let val = input;

  if (typeof input !== 'string' && typeof input.toString === 'function') {
    val = input.toString();
  }

  if (val.length === length) {
    return val;
  }

  while (val.length < length) {
    val = `${padWith}${val}`;
  }

  return val;
}

export function isNumeric(val) {
  return /^\d+$/.test(val);
}

export function getElData(el, name, type = '') {
  const data = el.getAttribute(`data-${name}`);

  if (type === 'int') {
    return parseInt(data, 10);
  }

  if (type === 'float') {
    return parseFloat(data);
  }

  if (type === 'json') {
    return JSON.parse(data);
  }

  return data;
}

const coordinateRegexp = /^-?[\d]{1,3}([.][\d]+)?$/;

export function isValidCoordinate(n) {
  const num = Number(n);

  if (Number.isNaN(num)) {
    return false;
  }

  if (!coordinateRegexp.test(num)) {
    return false;
  }

  return num >= -180 && num <= 180;
}

export function areValidCoordinates(coords) {
  if (!coords) {
    return false;
  }

  let { lng, lat } = coords;

  if (!lng || !lat) {
    return false;
  }

  if (typeof lng === 'function') {
    lng = lng();
    lat = lat();
  }

  if (!isValidCoordinate(lat) || !isValidCoordinate(lng)) {
    infoLog('invalid coordinates', lng, lat);
    return false;
  }

  return true;
}

export function countEmptyStringSegments(str, sep) {
  return str.split(sep).filter(s => s.trim().length === 0).length;
}

export function convertToBool(value) {
  if (typeof value === 'string') {
    const truthy = ['true', '1', 'yes'];
    const falsy = ['false', '0', 'no'];
    const lc = value.toLowerCase();

    if (truthy.includes(lc) || lc.length) {
      return true;
    }

    if (!falsy.includes(lc) && lc.length) {
      return true;
    }

    return false;
  }

  if (typeof value === 'number') {
    return value >= 1.0;
  }

  if (value && value.length) {
    return true;
  }

  return false;
}

export function convertToType(value, type) {
  switch(type) {
    case 'string':
      return String(value);

    case 'int':
      return parseInt(value, 10);

    case 'float':
    case 'lat':
    case 'lng':
      return parseFloat(value);

    case 'boolean':
      return convertToBool(value);

    default:
      return value;
  }
}

export function addClassToAppMain(className) {
  const appMain = document.querySelector('.app-main');
  appMain && appMain.classList.add(className);
}

export function removeClassFromAppMain(className) {
  const appMain = document.querySelector('.app-main');
  appMain && appMain.classList.remove(className);
}

export function  arrayContainsArray (a, b) {
  return b.every((c) => a.includes(c));
}

export function rightFill(str, len, fillSeq) {
  if (str.length >= len) {
    return str;
  }

  let output = str;

  while (output.length < len) {
    output += fillSeq;
  }

  return output;
}

// Number.toFixed without rounding
export function toFixed(num, precision) {
  const [integer, decimal = '0'] = num.toString().split('.');
  const dec = rightFill(decimal, precision, '0').slice(0, precision);

  return `${integer}.${dec}`;
}

export function delayedPromise(timeOut) {
  return new Promise((resolve) => setTimeout(resolve, timeOut));
}

export function objKeysEqual(a, b, key) {
  return a[key] === b[key];
}

export function endSlash(str) {
  if (str.endsWith('/')) {
    return str;
  }

  return `${str}/`;
}
