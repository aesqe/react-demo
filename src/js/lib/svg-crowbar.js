window.URL = (window.URL || window.webkitURL);

function prepareSvg(svgElement, preprocessFn) {
  const doctype = `<?xml version="1.0" standalone="no"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"
  "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">`;

  const prefix = {
    xmlns: 'http://www.w3.org/2000/xmlns/',
    xlink: 'http://www.w3.org/1999/xlink',
    svg: 'http://www.w3.org/2000/svg',
  };

  svgElement = preprocessFn(svgElement);
  svgElement.setAttribute('version', '1.1');
  svgElement.removeAttribute('xmlns');
  svgElement.removeAttribute('xlink');

  if (!svgElement.hasAttributeNS(prefix.xmlns, 'xmlns')) {
    svgElement.setAttributeNS(prefix.xmlns, 'xmlns', prefix.svg);
  }

  if (!svgElement.hasAttributeNS(prefix.xmlns, 'xmlns:xlink')) {
    svgElement.setAttributeNS(prefix.xmlns, 'xmlns:xlink', prefix.xlink);
  }

  const rect = svgElement.getBoundingClientRect();
  const source = (new XMLSerializer()).serializeToString(svgElement);

  return {
    top: rect.top,
    left: rect.left,
    width: rect.width,
    height: rect.height,
    class: svgElement.getAttribute('class'),
    id: svgElement.getAttribute('id'),
    childElementCount: svgElement.childElementCount,
    source: [`${doctype}${source}`],
  };
}

function generateSvgFilename(svgElement) {
  if (svgElement.id) {
    return svgElement.id;
  }

  if (svgElement.class) {
    return svgElement.class;
  }

  if (window.document.title) {
    return window.document.title.replace(/[^a-z0-9]/gi, '-').toLowerCase();
  }

  return 'untitled';
}

function downloadSvg(svgElement, filename) {
  if (filename === 'untitled') {
    filename = generateSvgFilename(svgElement);
  }

  const xmlBlob = new Blob(svgElement.source, { 'type': 'text/xml' });
  const url = window.URL.createObjectURL(xmlBlob);
  const a = document.createElement('a');

  document.body.appendChild(a);

  a.setAttribute('class', 'svg-crowbar');
  a.setAttribute('download', `${filename}.svg`);
  a.setAttribute('href', url);
  a.style['display'] = 'none';
  a.click();

  svgElement.setAttribute('fill', '');

  setTimeout(
    () => window.URL.revokeObjectURL(url),
    500
  );
}

function svgCrowbar(svgElement, filename = 'untitled', preprocessFn = (x) => x) {
  return downloadSvg(
    prepareSvg(svgElement, preprocessFn),
    filename
  );
}

exports.svgCrowbar = svgCrowbar;
