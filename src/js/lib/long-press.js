export default function longPressHandlers(cb = () => {}, ms = 1500) {
  let buttonPressTimer = null;

  function handleButtonPress (event) {
    event.persist();
    buttonPressTimer = setTimeout(() => cb(event), ms);
  }

  function handleButtonRelease () {
    clearTimeout(buttonPressTimer);
  }

  return {
    onTouchStart: handleButtonPress,
    onTouchEnd: handleButtonRelease,
    onMouseDown: handleButtonPress,
    onMouseUp: handleButtonRelease,
    onMouseLeave: handleButtonRelease,
  };
}
