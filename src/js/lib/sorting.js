export function alphaAsc(a, b, prop) {
  return a[prop].localeCompare(b[prop]);
}

export function alphaDesc(a, b, prop) {
  return b[prop].localeCompare(a[prop]);
}

export function numericAsc(a, b, prop) {
  return a[prop] - b[prop];
}

export function numericDesc(a, b, prop) {
  return b[prop] - a[prop];
}
