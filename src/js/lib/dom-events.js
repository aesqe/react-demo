/* global CustomEvent */

export function isNativeEvent(eventname) {
  eventname = eventname.replace(/\..*/, ''); // strip namespace
  return typeof document.body[`on${eventname}`] !== 'undefined';
}

export function addDomEvent(eventNames, selector, callback) {
  const els = document.querySelectorAll(selector);
  const eNames = eventNames.split(/\s+/);

  for( const e of eNames ) {
    for( const el of els ) {
      el.addEventListener(e, callback);
    }
  }
}

export function triggerDomEvent(eventNames, selector, data = null) {
  const els = document.querySelectorAll(selector);
  const eNames = eventNames.split(/\s+/);
  const eData = {detail: {data}};
  const eventList = [];

  for( const ev of eNames ) {
    if( isNativeEvent(ev) ) {
      const eventName = ev.replace(/\..*/, '');
      const event = document.createEvent('HTMLEvents');
      event.initEvent(eventName, true, false);
      eventList.push(event);
    } else {
      eventList.push( new CustomEvent(ev, eData) );
    }
  }

  for( const el of els ) {
    for( const event of eventList ) {
      el.dispatchEvent(event);
    }
  }
}

export function documentReady(textAreaSelectors) {
  const textAreaEvents = 'afterselect change focus paste keyup window_resize';

  this.addDomEvent(textAreaEvents, 'body', ( event ) => {
    if( event.target.matches(textAreaSelectors) ) {
      const el = event.target;
      el.style.height = '0px';
      el.style.height = `${el.scrollHeight + 15}px`;
    }
  });

  window.addEventListener('resize', () => {
    triggerDomEvent('window_resize', textAreaSelectors);
  });
}
