/* worker script */

const path = require('path');
const jimp = require('jimp');
const jetpack = require('fs-jetpack');
const extensions = [
  '.jpg',
  '.jpeg',
  '.png',
  '.bmp',
  // ".gif", // not supported by jimp
];
const IMAGE_WIDTH = 300;
const IMAGE_QUALITY = 75;

let totalImages = 0;
let currentImage = 0;

function sendMessage( data ) {
  process.send({
    currentImage,
    totalImages,
    ...data,
  });
}

function nextImage () {
  currentImage++;

  if( currentImage === totalImages ) {
    sendMessage({
      status: 'done',
    });
  }
}

process.on('message', (message) => {
  const { imagesFolderPath, skipExisting } = message;

  jetpack.listAsync(imagesFolderPath).then((data) => {

    if( data.error ) {
      sendMessage({
        status: 'error',
        error: data,
      });
      return;
    }

    const images = data.filter((fileName) => {
      const extension = path.extname(fileName);
      return extensions.indexOf(extension) > -1;
    });

    totalImages = images.length;
    currentImage = 0;

    images.forEach((fileName) => {
      const fullPath = path.join(imagesFolderPath, fileName);
      const thumbPath = path.join(imagesFolderPath, 'thumbs', fileName);
      const fileExists = jetpack.exists(thumbPath);
      const shouldSkipFile = (skipExisting && fileExists);

      if( shouldSkipFile ) {
        sendMessage({
          status: 'skipped',
          thumbPath,
          fullPath,
        });

        nextImage();
        return;
      }

      jimp.read(fullPath, (err, image) => {
        let error = err;

        sendMessage({status: 'read image', image});

        if( ! error ) {
          try {
            image.resize(IMAGE_WIDTH, jimp.AUTO)
              .quality(IMAGE_QUALITY)
              .write(thumbPath);

            sendMessage({
              status: 'processed',
              thumbPath,
              fullPath,
            });
          } catch (err2) {
            error = err2;
          }
        }

        if( error ) {
          sendMessage({
            status: 'error',
            thumbPath,
            fullPath,
            error,
          });
        }

        nextImage();
      });
    });
  });
});
