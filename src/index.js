import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import nanoid from 'nanoid';

import store from 'reducers';
import NotificationProvider from 'components/notification-provider';
import { AppUpdatesProvider } from 'components/app-updates';

import App from './App';
import { platform } from './const';
import './css/style.scss';

const rootElement = document.getElementById('root');

rootElement.classList.add(`platform-${platform}`);

nanoid();

ReactDOM.render(
  <Provider store={store}>
    <NotificationProvider>
      <AppUpdatesProvider>
        <App />
      </AppUpdatesProvider>
    </NotificationProvider>
  </Provider>,
  rootElement
);
