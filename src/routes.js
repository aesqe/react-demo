import React from 'react';
import { Route, Switch } from 'react-router-dom';

import {
  StreetViewManager,
  StreetViewSequence,
  StreetViewSequencesList,
} from 'components/streetview';
import { TaxonomyManager } from 'components/term-manager';
import { AppUpdates } from 'components/app-updates';
import { CollectionList } from 'components/collection-list';
import Output from 'components/streetview/output';
import Home from 'components/home';
import Heatmap from 'components/heatmap';

export default function Routes () {
  return (
    <Switch>
      <Route exact path="/streetview" component={StreetViewManager} />
      <Route exact path="/streetview-list" component={StreetViewSequencesList} />
      <Route path="/streetview/:id" component={StreetViewSequence} />
      <Route path="/terms" component={TaxonomyManager} />
      <Route path="/collections" component={CollectionList} />
      <Route path="/output" component={Output} />
      <Route path="/updates" component={AppUpdates} />
      <Route path="/heatmap" component={Heatmap} />
      <Route component={Home} />
    </Switch>
  );
}
